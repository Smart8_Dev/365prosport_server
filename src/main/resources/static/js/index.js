window.onload = function () {
    var ajax = new XMLHttpRequest();
    var url = "/admin/api/unievents?pattern=";

    var input = document.getElementById("umatch-id");

    input.addEventListener("keyup", function(e){
        var val = input.value;
        var code = (e.keyCode || e.which);
        if(code === 37 || code === 38 || code === 39 || code === 40 || code === 13) {
            return;
        }
        if ( val.length > 2 ){
            ajax.open("GET", url + val, true);
            ajax.send();
        }
    });

    var awesomplete = new Awesomplete(input);

    ajax.onload = function() {
        var resultData = JSON.parse(ajax.responseText);
        var list = [];
        for( var i in resultData){
            list.push([i + " : " + resultData[i], i]);
        }
        awesomplete.list = list;
    };
};



