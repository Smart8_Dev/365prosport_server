package com.smart8.sportradar.models.news;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class News implements Serializable{

    private String image;
    private String externalCreatedAt;
    private String fullUrl;
    private String unformattedMessage;
    private String description;
    private String posterName;
    private String source;
    private String category;
    private String id;
}
