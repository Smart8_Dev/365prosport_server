package com.smart8.sportradar.models.news;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class Bing {

    private String _type;
    private String readLink;
    private int totalEstimatedMatches;
    private List<Value> value;

    public List<Value> getValue(){
        if(value == null){
            return Collections.emptyList();
        }

        return value;
    }

    @Data
    public static class Value {
        private String name;
        private String url;
        private ImageContainer image;
        private String description;
//        private About about;
        private String datePublished;
        private String category;
        private List<Provider> provider;
    }

    @Data
    public static class ImageContainer {
            private Image thumbnail;
    }

    @Data
    public static class Image {
        private String contentUrl;
        private int width;
        private int height;
    }

    @Data
    public static class Provider {
        private String _type;
        private String name;
    }
}
