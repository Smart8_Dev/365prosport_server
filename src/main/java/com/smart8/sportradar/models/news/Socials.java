package com.smart8.sportradar.models.news;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Socials {

    private Posts posts;

    @JsonProperty("external_created_at")
    private String externalCreatedAt;

    @Data
    public static class Posts {
        private List<NewsItem> items;
    }

    @Data
    public static class Source implements Serializable {
        private long id;
        private String term;
        private String term_type;
        private String source;
        private String options;
    }

    //todo firebase should expect to get camelcase and not underscore
    @Data
    public static class NewsItem implements Serializable{
        private long id;

        @JsonProperty("external_id")
        private String external_id;

        private String image;

        @JsonProperty("poster_image")
        private String poster_image;

        @JsonProperty("unformatted_message")
        private String unformatted_message;

        @JsonProperty("full_url")
        private String full_url;

        @JsonProperty("poster_name")
        private String poster_name;

        @JsonProperty("source")
        private Source source;

        @JsonProperty("external_created_at")
        private String external_created_at;
    }
}
