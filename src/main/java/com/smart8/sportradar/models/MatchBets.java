package com.smart8.sportradar.models;

import com.smart8.sportradar.enums.CriterionType;
import com.smart8.sportradar.firebase.FbBet;
import lombok.ToString;

import java.util.*;
import java.util.stream.Collectors;

@ToString
public class MatchBets {
    private Map<CriterionType, Set<FbBet>> criterionBets = new HashMap<>();

    public boolean addBet(CriterionType type, FbBet fbBet){
        Set<FbBet> bets = criterionBets.computeIfAbsent(type, s -> new HashSet<>());
        return bets.add(fbBet);
    }

    public boolean removeBet(CriterionType type, FbBet fbBet) {
        Set<FbBet> bets = criterionBets.get(type);
        return bets == null || bets.remove(fbBet);
    }

    public void removeCriterionBets(CriterionType type) {
        criterionBets.remove(type);
    }

    public Set<String> getAllUsers(){
        Set<String> users = new HashSet<>();
        for ( Set<FbBet> bets : criterionBets.values() ){
            bets.forEach(bet -> users.add(bet.getUserId()));
        }
        return users;
    }
    public Set<FbBet> getCriterionBets(CriterionType type){
        return criterionBets.getOrDefault(type, Collections.emptySet());
    }

    public Set<String> getUsersByCriterion(CriterionType type){
        return criterionBets.get(type).stream().map(FbBet::getUserId).collect(Collectors.toSet());
    }
}
