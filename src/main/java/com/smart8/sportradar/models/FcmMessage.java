package com.smart8.sportradar.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class FcmMessage {
    public enum MessageType {friend, bet, event}

    private Notification notification;
    private CustomData data;
    private List<String> registration_ids;
    private Boolean content_available;

    @Data
    @Builder
    public static class CustomData {
        private String title;
        private String body;
        private String matchId;
        private MessageType type;
        private String sportType;
        private String eventType;
        private String betId;
        private String payload;
    }

    @Data
    public static class Notification {
        String title;
        String body;
        private String icon;
        private String click_action;
    }
}
