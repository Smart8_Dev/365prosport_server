package com.smart8.sportradar.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MappedLeagues {

    private String name;
    private String srId;
    private long uniId;
    private long parentid;
}
