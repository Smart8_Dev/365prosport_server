package com.smart8.sportradar.models;

import com.smart8.sportradar.fb.Contestant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
public class MiniMatch {
    private String id;
    private List<Contestant> contestants;
    private String dateTime;
    private long dateTimeInt;
    private long tournamentId;
    private String tournamentName;

    public MiniMatch(){}
}