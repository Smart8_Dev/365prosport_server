package com.smart8.sportradar.models.unibet;

import lombok.*;

import java.util.ArrayList;
import java.util.List;


@Data
public class BetOffer {

    private String date;
    private String name;
    private long criterionId;
    private List<Outcome> outcomes = new ArrayList<>();

    @AllArgsConstructor
    @Data
    public static class Outcome {
        private float odds;
        private String type;
    }

    public void addOutcome(Outcome outcome){
        this.outcomes.add(outcome);
    }

}
