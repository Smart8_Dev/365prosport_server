package com.smart8.sportradar.models.unibet;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class UniResult {

    private Range range;
    private List<Event> events;
    private List<BetOffer> betoffers;

    @ToString
    @Getter
    @Setter
    public static class Range {
        private long total;
        private long start;
        private long size;
    }

    @ToString
    @Getter
    @Setter
    public static class Event {
        private String state;
        private String sport;
        private String originalStartTime;
        private String type;
        private long id;
        private long groupId;
        private String start;
        private String awayName;
        private String name;
        private String homeName;
        private String group;

        private List<Participant> participants;
        private List<Path> path;
    }

    @ToString
    @Getter
    @Setter
    public static class Participant {
        private String name;
        private long participantId;
    }

    @ToString
    @Getter
    @Setter
    public static class Path {
        private long id;
        private String englishName;
        private String name;
    }

    @ToString
    @Getter
    @Setter
    public static class BetOffer {
        private long id;
        private long eventId;
        private boolean startingPrice;
        private boolean open;
        private List<Outcome> outcomes;
        private BetCriterion criterion;
        private EachWay eachWay;
        private BetOfferType betOfferType;
        private String closed;
        private boolean suspended;
        private boolean live;
        private boolean main;
    }

    @ToString
    @Getter
    @Setter
    public static class BetCriterion {
        private long id;
        private boolean isDefault;
        private String label;
    }

    @ToString
    @Getter
    @Setter
    public static class BetOfferType {
        private long id;
        private String name;
    }

    @ToString
    @Getter
    @Setter
    public static class Outcome {
        private long id;
        private String changedDate;
        private OutcomeCriterion criterion;
        private boolean homeTeamMember;
        private boolean popular;
        private String oddsFractional;
        private long line;
        private String participant;
        private String label;
        private String oddsAmerican;
        private String type;
        private long odds;
    }

    @ToString
    @Getter
    @Setter
    public static class OutcomeCriterion {
        private String name;
        private int type;
    }

    @ToString
    @Getter
    @Setter
    public static class EachWay {
        private long placeLimit;
        private String terms;
        private long fractionLimit;
    }
}
