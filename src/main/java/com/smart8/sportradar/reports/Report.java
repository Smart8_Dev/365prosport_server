package com.smart8.sportradar.reports;

@FunctionalInterface
public interface Report<T> {

    void writeReport(T data, String file);
}
