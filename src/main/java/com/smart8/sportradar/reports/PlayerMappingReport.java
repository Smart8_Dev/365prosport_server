package com.smart8.sportradar.reports;

import com.smart8.sportradar.firebase.FbPlayer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.smart8.sportradar.Constants.NEW_LINE;

@Slf4j
@Component
public class PlayerMappingReport implements Report<List<FbPlayer>> {

    @Override
    public void writeReport(List<FbPlayer> list, String file) {
        StringBuilder builder = new StringBuilder();

        for(FbPlayer player : list ){
            String missingFields = player.emptyFields();
            if ( !missingFields.isEmpty() ){
                builder.append("player: ")
                    .append(player.getId())
                    .append(", ")
                    .append(player.getFirstName())
                    .append(" ")
                    .append(player.getLastName())
                    .append(NEW_LINE)
                    .append("    missingData: ")
                    .append(missingFields)
                    .append(NEW_LINE);
            }
        }
        try {
            FileUtils.writeStringToFile(new File(file), builder.toString(), "UTF-8", true  );
            log.info("@writeReport > Writing {} missing items to file {}", list.size(), file );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
