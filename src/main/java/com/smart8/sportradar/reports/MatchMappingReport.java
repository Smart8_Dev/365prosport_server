package com.smart8.sportradar.reports;

import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.models.unibet.UniResult;
import com.smart8.sportradar.repositories.MatchRepository;
import com.smart8.sportradar.utils.ConverterHelpers;
import com.smart8.sportradar.utils.MatchComperator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.stream.Collectors;

import static com.smart8.sportradar.Constants.NEW_LINE;

@Component
public class MatchMappingReport implements Report<Map<Long, UniResult.Event>> {

    private MatchRepository matchRepository;

    private Set<Long> found = new HashSet<>();
    private List<MatchComperator.MatchMapper> foundMatches = new ArrayList<>();
    private List<FbSoccerMatch.FbMiniMatch> upcomingMatchesRadar;
    private Map<Long, String> unmappedTournaments = new HashMap<>();
    private Map<String, Map<String, Map<String, FbSoccerMatch.FbMiniMatch>>> upcomingMatches;

    public void setUpcomingMatches(Map<String, Map<String, Map<String, FbSoccerMatch.FbMiniMatch>>> upcomingMatches) {
        this.upcomingMatches = upcomingMatches;
    }

    @Override
    public void writeReport(Map<Long, UniResult.Event> events, String file) {
        List<String> foundMatches = matchesFoundReport(events);
        List<String> unibetMissingGames = unibetMissingGames(events);
        List<String> sRadarMissingGames = radarMissingGames(events);

        StringBuilder builder = new StringBuilder();
        builder.append("---- MATCHES FOUND ----").append(NEW_LINE).append(NEW_LINE);

        for (String foundMatch : foundMatches) {
            builder.append(foundMatch);
            builder.append(NEW_LINE);
        }

        builder.append(NEW_LINE);

        builder.append("---- UNIBET MATCHES THAT ARE NOT MAPPED ----");
        builder.append(NEW_LINE);
        builder.append(NEW_LINE);

        for (String unibetMissingGame : unibetMissingGames) {
            builder.append(unibetMissingGame);
            builder.append(NEW_LINE);
        }

        builder.append(NEW_LINE);


        builder.append("---- SPORT RADAR MATCHES THAT ARE NOT MAPPED ----");
        builder.append(NEW_LINE);
        builder.append(NEW_LINE);

        for (String unibetMissingGame : sRadarMissingGames) {
            builder.append(unibetMissingGame);
            builder.append(NEW_LINE);
        }

        builder.append(NEW_LINE);

        builder.append("---- UNMAPPED  UNIBET TOURNAMENTS THAT RETURNED----");
        builder.append(NEW_LINE);
        builder.append(NEW_LINE);

        for (Map.Entry<Long, String> entry : unmappedTournaments.entrySet()) {
            builder.append(entry.getKey()).append(" - ").append(entry.getValue());
            builder.append(NEW_LINE);
        }

        try {
            FileUtils.writeStringToFile(new File("/home/nodeuser/sportradar/report-" + Instant.now().getEpochSecond() + ".txt"), builder.toString(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> radarMissingGames(Map<Long, UniResult.Event> events) {
        List<String> data = new ArrayList<>();

        Set<String> missingGames = upcomingMatchesRadar.stream().map(FbSoccerMatch.FbMiniMatch::getId).collect(Collectors.toSet());

        for (MatchComperator.MatchMapper foundMatch : foundMatches) {
            missingGames.remove(foundMatch.sRadarId);
        }

        for (Long aLong : found) {
            missingGames.remove(matchRepository.getRadarMatchIdFromUnibetMatchId(aLong));
        }

        for (String missingGame : missingGames) {
            final Optional<FbSoccerMatch.FbMiniMatch> first = upcomingMatchesRadar.stream().filter(match -> match.getId().equals(missingGame)).findFirst();

            first.ifPresent(match -> addGameName(data, match, ""));
        }

        data.sort(String.CASE_INSENSITIVE_ORDER);

        return data;
    }

    private List<String> matchesFoundReport(Map<Long, UniResult.Event> events) {
        List<String> foundMatchesStr = new ArrayList<>();

        for (MatchComperator.MatchMapper foundMatch : foundMatches) {
            final UniResult.Event event = events.get(foundMatch.unibetId);

            addGameName(foundMatchesStr, event, "unibet Game: ");

            for (FbSoccerMatch.FbMiniMatch fbMiniMatch : upcomingMatchesRadar) {
                if(fbMiniMatch.getId().equals(foundMatch.sRadarId)){
                    addGameName(foundMatchesStr, fbMiniMatch, "spRadar Game: ");
                }
            }
        }

        for (Long unibet : found) {
            final UniResult.Event event = events.get(unibet);

            addGameName(foundMatchesStr, event, "unibet Game: ");

            for (FbSoccerMatch.FbMiniMatch fbMiniMatch : upcomingMatchesRadar) {
                if(fbMiniMatch.getId().equals(matchRepository.getRadarMatchIdFromUnibetMatchId(unibet))){
                    addGameName(foundMatchesStr, fbMiniMatch, "spRadar Game: ");
                }
            }
        }

        return foundMatchesStr;
    }

    private void addGameName(List<String> foundMatchesStr, FbSoccerMatch.FbMiniMatch fbMiniMatch, String prefix) {
        String radarFirst = fbMiniMatch.getContestants().get(0).getName();
        String radarSecond = fbMiniMatch.getContestants().get(1).getName();

        radarFirst = StringUtils.isNotEmpty(radarFirst) ? radarFirst : "MISSING TEAM NAME!";
        radarSecond = StringUtils.isNotEmpty(radarSecond) ? radarSecond : "MISSING TEAM NAME!";

        if(radarFirst.compareToIgnoreCase(radarSecond) < 0){
            foundMatchesStr.add(prefix + radarFirst + " - " + radarSecond + ", league: " + fbMiniMatch.getTournamentId() + ", matchId: " + fbMiniMatch.getId());
        }
        else{
            foundMatchesStr.add(prefix + radarSecond + " - " + radarFirst + ", league: " + fbMiniMatch.getTournamentId() + ", matchId: " + fbMiniMatch.getId());
        }
    }

    private void addGameName(List<String> data, UniResult.Event event, String prefix) {
        final List<UniResult.Participant> participants = event.getParticipants();
        final String first = participants.get(0).getName();
        final String second = participants.get(1).getName();

        if(first.compareToIgnoreCase(second) < 0){
            data.add(prefix + first + " - " + second + ", league: " + event.getGroup() + ", matchId: " + event.getId());
        }
        else{
            data.add(prefix + second + " - " + first + ", league: " + event.getGroup() + ", matchId: " + event.getId());
        }
    }

    private List<String> unibetMissingGames(Map<Long, UniResult.Event> events) {
        List<String> data = new ArrayList<>();

        Set<Long> missingEvents = new HashSet<>(events.keySet());

        for (MatchComperator.MatchMapper foundMatch : foundMatches) {
            missingEvents.remove(foundMatch.unibetId);
        }

        for (Long aLong : found) {
            missingEvents.remove(aLong);
        }

        for (Long missingEvent : missingEvents) {
            final UniResult.Event event = events.get(missingEvent);
            final TemporalAccessor accessor = ConverterHelpers.unibetFormat.parse(event.getStart());
            final String format = ConverterHelpers.sdf.format(accessor);

            if(upcomingMatches.containsKey(format)){
                addGameName(data, event, "");
            }
        }

        data.sort(String.CASE_INSENSITIVE_ORDER);

        return data;
    }
}
