package com.smart8.sportradar.reports;

import com.smart8.sportradar.firebase.FbTeam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.smart8.sportradar.Constants.NEW_LINE;

@Slf4j
@Component
public class TeamMappingReport implements Report<List<FbTeam>> {
    @Override
    public void writeReport(List<FbTeam> list, String file) {
        StringBuilder builder = new StringBuilder();

        for(FbTeam team : list ){
            builder.append(team.id)
                .append(", ")
                .append(team.code)
                .append(", ")
                .append(team.name).append(NEW_LINE);
        }
        try {
            FileUtils.writeStringToFile(new File(file), builder.toString(), "UTF-8", true  );
            log.info("@writeReport > Writing {} missing items to file {}", list.size(), file );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
