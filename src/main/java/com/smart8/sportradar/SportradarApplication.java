package com.smart8.sportradar;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootApplication
@EnableScheduling
@EnableCaching
public class SportradarApplication {

    @Autowired
    ObjectMapper mapper;

    @Bean(name = "Statistics")
    public Jaxb2Marshaller getStatisticsBean() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan("com.smart8.sportradar.statisticsmodels");
        Map<String, Object> map = new HashMap<>();
        jaxb2Marshaller.setMarshallerProperties(map);
        return jaxb2Marshaller;
    }

    @Bean(name = "LiveModels")
    public Jaxb2Marshaller getCastorMarshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan("com.smart8.sportradar.livemodels");
        Map<String, Object> map = new HashMap<>();
        jaxb2Marshaller.setMarshallerProperties(map);
        return jaxb2Marshaller;
    }


    public static void main(String[] args) throws InterruptedException { SpringApplication.run(SportradarApplication.class, args);}
}
