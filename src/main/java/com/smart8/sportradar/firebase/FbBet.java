package com.smart8.sportradar.firebase;

import com.smart8.sportradar.enums.SportType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FbBet {
    private String id;
    private String matchId;
    private String userId;
    private Long criterionId;
    private Status status;
    private Outcome on;
    private SportType sport;
    private int stake;
    private Double odds;
    private Long date;

    public enum Status {Active, Win, Lose, NoBet}
    public enum Outcome {OT_ONE, OT_CROSS, OT_TWO}
}
