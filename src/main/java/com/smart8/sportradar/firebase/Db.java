package com.smart8.sportradar.firebase;

public interface Db {

    String TOURNAMENTS = "tournaments";
    String MATCHES = "matches";
    String PLAYERS = "players";
    String TEAMS = "teams";
    String SOCCER = "soccer";
    String DARTS = "darts";
    String FAVORITES = "favorites";
    String STANDINGS = "standings";

    String NEWS = "news";
}
