package com.smart8.sportradar.firebase;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
public class DataToPersist {

    private List<FbTournament> tournaments;
    private List<FbMatch> matches;
    private List<FbMatch> ended;
    private List<FbMatch> upcoming;
    private List<FbMatch> live;

    private DataToPersist(){
        this.tournaments = new ArrayList<>();
        this.matches = new ArrayList<>();
        this.ended = new ArrayList<>();
        this.upcoming = new ArrayList<>();
        this.live = new ArrayList<>();
    }

    public static class Builder {

        private DataToPersist dataToPersist;

        public Builder() {
            dataToPersist = new DataToPersist();
        }

        public Builder addTournament(FbTournament tournament){
            dataToPersist.tournaments.add(tournament);

            return this;
        }

        public Builder addMatch(FbSoccerMatch match){
            dataToPersist.matches.add(match);

            return this;
        }

        public Builder addMatchNotStarted(FbMatch match){
            dataToPersist.upcoming.add(match);

            return this;
        }

        public Builder addMatchEnded(FbMatch match){
            dataToPersist.ended.add(match);

            return this;
        }

        public DataToPersist build(){
            return dataToPersist;
        }

        public Builder addMatchLive(FbMatch fbMatch) {
            dataToPersist.live.add(fbMatch);
            return this;
        }
    }
}
