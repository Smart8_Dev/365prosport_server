package com.smart8.sportradar.firebase;

import lombok.Data;

@Data
public class Team {

    public final String formation;
    public final String code;
    public final String name;
    public final String position;

}
