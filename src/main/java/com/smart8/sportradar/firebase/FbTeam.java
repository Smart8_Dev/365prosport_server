package com.smart8.sportradar.firebase;

import lombok.*;

@Getter
@ToString
@Builder
public class FbTeam {

    public String betradarTeamId;
    public String code;
    public String formation;
    public String id;
    public String name;
    public String position;
    public String score;
    public DressInfo dressInfo;
    public Integer unibetId;


    @Builder
    @ToString
    @Getter
    public static class DressInfo {
        public String gkNumberColor;
        public String gkShirtColor;
        public String gkSleeveColor;
        public String numberColor1;
        public String numberColor2;
        public String shirtColor1;
        public String shirtColor2;
        public String sleeveColor1;
        public String sleeveColor2;

        public static DressInfoBuilder builder() {
            return new DressInfoBuilder();
        }

        public String getGkNumberColor() {
            return this.gkNumberColor;
        }

        public String getGkShirtColor() {
            return this.gkShirtColor;
        }

        public String getGkSleeveColor() {
            return this.gkSleeveColor;
        }

        public String getNumberColor1() {
            return this.numberColor1;
        }

        public String getNumberColor2() {
            return this.numberColor2;
        }

        public String getShirtColor1() {
            return this.shirtColor1;
        }

        public String getShirtColor2() {
            return this.shirtColor2;
        }

        public String getSleeveColor1() {
            return this.sleeveColor1;
        }

        public String getSleeveColor2() {
            return this.sleeveColor2;
        }

        public void setGkNumberColor(String gkNumberColor) {
            this.gkNumberColor = gkNumberColor;
        }

        public void setGkShirtColor(String gkShirtColor) {
            this.gkShirtColor = gkShirtColor;
        }

        public void setGkSleeveColor(String gkSleeveColor) {
            this.gkSleeveColor = gkSleeveColor;
        }

        public void setNumberColor1(String numberColor1) {
            this.numberColor1 = numberColor1;
        }

        public void setNumberColor2(String numberColor2) {
            this.numberColor2 = numberColor2;
        }

        public void setShirtColor1(String shirtColor1) {
            this.shirtColor1 = shirtColor1;
        }

        public void setShirtColor2(String shirtColor2) {
            this.shirtColor2 = shirtColor2;
        }

        public void setSleeveColor1(String sleeveColor1) {
            this.sleeveColor1 = sleeveColor1;
        }

        public void setSleeveColor2(String sleeveColor2) {
            this.sleeveColor2 = sleeveColor2;
        }


        public static class DressInfoBuilder {
            private String gkNumberColor;
            private String gkShirtColor;
            private String gkSleeveColor;
            private String numberColor1;
            private String numberColor2;
            private String shirtColor1;
            private String shirtColor2;
            private String sleeveColor1;
            private String sleeveColor2;

            DressInfoBuilder() {
            }

            public DressInfo.DressInfoBuilder gkNumberColor(String gkNumberColor) {
                this.gkNumberColor = gkNumberColor;
                return this;
            }

            public DressInfo.DressInfoBuilder gkShirtColor(String gkShirtColor) {
                this.gkShirtColor = gkShirtColor;
                return this;
            }

            public DressInfo.DressInfoBuilder gkSleeveColor(String gkSleeveColor) {
                this.gkSleeveColor = gkSleeveColor;
                return this;
            }

            public DressInfo.DressInfoBuilder numberColor1(String numberColor1) {
                this.numberColor1 = numberColor1;
                return this;
            }

            public DressInfo.DressInfoBuilder numberColor2(String numberColor2) {
                this.numberColor2 = numberColor2;
                return this;
            }

            public DressInfo.DressInfoBuilder shirtColor1(String shirtColor1) {
                this.shirtColor1 = shirtColor1;
                return this;
            }

            public DressInfo.DressInfoBuilder shirtColor2(String shirtColor2) {
                this.shirtColor2 = shirtColor2;
                return this;
            }

            public DressInfo.DressInfoBuilder sleeveColor1(String sleeveColor1) {
                this.sleeveColor1 = sleeveColor1;
                return this;
            }

            public DressInfo.DressInfoBuilder sleeveColor2(String sleeveColor2) {
                this.sleeveColor2 = sleeveColor2;
                return this;
            }

        }
    }
}
