package com.smart8.sportradar.firebase;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FbUserProfile {
    private String id;
    private String language;
    //NotificationType string as key because firebase doesn't allow not string keys
    private Map<String, Boolean> notifications;

    public enum NotificationType {
        bets, cards, favorites, goals, lineups, news, penalties, prepare, social;
    }
}
