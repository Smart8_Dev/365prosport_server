package com.smart8.sportradar.firebase;

import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.livemodels.Country;
import com.smart8.sportradar.livemodels.Name;
import org.springframework.util.StringUtils;

import java.util.List;

import static com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament.Match.Venue.*;

public class FbVenue {
    public String city;
    public String cityId;
    public String country;
    public String countryId;
    public String stadium;
    public String stadiumId;

    @java.beans.ConstructorProperties({"city", "cityId", "country", "countryId", "stadium", "stadiumId"})
    FbVenue(String city, String cityId, String country, String countryId, String stadium, String stadiumId) {
        this.city = city;
        this.cityId = cityId;
        this.country = country;
        this.countryId = countryId;
        this.stadium = stadium;
        this.stadiumId = stadiumId;
    }

    public static FbVenueBuilder builder() {
        return new FbVenueBuilder();
    }

    public static class FbVenueBuilder {
        private String city;
        private String cityId;
        private String country;
        private String countryId;
        private String stadium;
        private String stadiumId;

        FbVenueBuilder() {
        }

        public FbVenue.FbVenueBuilder city(City city) {
            if(city != null){
                final List<Name> names = city.getName();

                for (Name name : names) {
                    if(!StringUtils.isEmpty(name.getValue())){
                        this.city = name.getValue();
                    }
                    else if(!StringUtils.isEmpty(name.getValue2())){
                        this.city = name.getValue2();
                    }
                }

                if(city.getId() != null){
                    this.cityId = String.valueOf(city.getId().longValue());
                }
            }

            return this;
        }


        public FbVenue.FbVenueBuilder country(Country country) {
            if(country != null){
                if (country.getId() != null) {
                    this.countryId = String.valueOf(country.getId().longValue());
                }

                if(country.getName() != null){
                    for (Name name : country.getName()) {
                        if(!StringUtils.isEmpty(name.getValue())){
                            this.country = name.getValue();
                        }
                        else if(!StringUtils.isEmpty(name.getValue2())){
                            this.country = name.getValue2();
                        }
                    }
                }
            }
            return this;
        }

        public FbVenue.FbVenueBuilder stadium(Stadium stadium) {
            if(stadium != null){
                if (stadium.getId() != null) {
                    this.stadiumId = String.valueOf(stadium.getId().longValue());
                }

                if(stadium.getName() != null){
                    for (Name name : stadium.getName()) {
                        if(!StringUtils.isEmpty(name.getValue())){
                            this.stadium = name.getValue();
                        }
                        else if(!StringUtils.isEmpty(name.getValue2())){
                            this.stadium = name.getValue2();
                        }
                    }
                }
            }
            return this;
        }

        public FbVenue.FbVenueBuilder stadiumId(String stadiumId) {
            this.stadiumId = stadiumId;
            return this;
        }

        public FbVenue build() {
            return new FbVenue(city, cityId, country, countryId, stadium, stadiumId);
        }

        public String toString() {
            return "com.smart8.sportradar.firebase.FbVenue.FbVenueBuilder(city=" + this.city + ", cityId=" + this.cityId + ", country=" + this.country + ", countryId=" + this.countryId + ", stadium=" + this.stadium + ", stadiumId=" + this.stadiumId + ")";
        }
    }
}
