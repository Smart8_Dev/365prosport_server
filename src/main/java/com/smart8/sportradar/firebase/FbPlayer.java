package com.smart8.sportradar.firebase;

import lombok.Builder;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class FbPlayer {

    public long id;
    public String firstName;
    public String fullName;
    public String lastName;
    public String matchName;
    public String playerId;
    public String position;
    public String number;
    public String substitute;
    public String redCards;
    public String yellowCards;

    public String birth;
    public String foot;
    public int weight;
    public int height;
    public String country;
    public String team;
    public long teamId;


    public String emptyFields(){
        List<String> missingFields = new ArrayList<>();

        if ( firstName == null ){
            missingFields.add("First name");
        }
        if ( lastName == null){
            missingFields.add("Last name");
        }
        if ( position == null ){
            missingFields.add("Position");
        }
        if ( number == null ){
            missingFields.add("Number");
        }
        if ( birth == null ){
            missingFields.add("Date of birth");
        }
        if ( foot == null ){
            missingFields.add("Preferred foot");
        }
        if ( weight == 0 ){
            missingFields.add("Weight");
        }
        if ( height == 0 ){
            missingFields.add("Height");
        }
        if ( country == null ){
            missingFields.add("Country");
        }
        if ( team == null ){
            missingFields.add("Team");
        }

        return StringUtils.collectionToCommaDelimitedString(missingFields);
    }

}
