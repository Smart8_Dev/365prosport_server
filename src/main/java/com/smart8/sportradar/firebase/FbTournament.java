package com.smart8.sportradar.firebase;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class FbTournament {
    private String id;
    private String country;
    private String countryId;
    private String name;
    private int priority;
    private String season;
    private String seasonId;
    private String seasonName;
}
