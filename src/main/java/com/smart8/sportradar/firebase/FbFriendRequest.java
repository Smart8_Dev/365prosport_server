package com.smart8.sportradar.firebase;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FbFriendRequest {
    private String id;
    private String inviter;
    private String inviterId;
    private String invited;
    private String invitedId;
    private InviteStatus status;

    public enum InviteStatus {
        REQUESTED, REJECTED, ACCEPTED;
    }
}
