package com.smart8.sportradar.firebase;

import com.smart8.sportradar.fb.Contestant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
public class FbSoccerMatch extends FbMatch{

    private SoccerLiveData liveData;

    @Override
    public void setLiveData(LiveData liveData) {
        this.liveData = (SoccerLiveData) liveData;
    }

    @Override
    public Map<String, Integer> getScore() {
        return this.getLiveData().getScore();
    }

    public SoccerLiveData getLiveData() {
        return liveData;
    }

    @Data
    @Builder
    public static class SoccerLiveData implements LiveData{
        private int minutes;

        /** key: teamId, value: teams goals*/
        private Map<String, Integer> score;

        /** key: teamId, value: teams goals*/
        private Map<String, Integer> aggScore;

        /** List of scores by period <teamId -> goals */
        private List<Map<String, Integer>> sets;
        private List<Goal> goals;
        private List<Card> cards;
        private List<Substitute> subs;
        private List<Statistic> stats;
        private List<Lineup> lineup;
        private List<ShootOut> shootouts;
    }

    @Data
    @Builder
    public static class Substitute {
        private long contestantId;
        private int periodId;
        private long playerOnId;
        private String playerOn;
        private long playerOffId;
        private String playerOff;
        private int timeMin;
    }


    @Data
    @Builder
    public static class Card {
        private long contestantId;
        private int periodId;
        private Long playerId;
        private String player;
        private int timeMin;
        private String type;
    }

    @Data
    @Builder
    public static class Goal {
        private long contestantId;
        private int periodId;
        private Long scorerId;
        private String scorer;
        private int timeMin;
        private String type;
        private Long assistId;
        private String assist;
        private boolean shootout;
    }

    @Data
    @Builder
    public static class ShootOut {
        private long contestantId;
        private int sequense;
        private Long playerId;
        private String player;
        private boolean score;
    }


    @Data
    @Builder
    public static class Lineup {
        private long contestantId;
        private String formation;
        private List<FbTeamOfficial> official;
        private List<LineupPlayer> players;

    }
    
    @Data
    @Builder
    public static class Statistic {
        private long contestantId;
        private Integer fouls;
        private Integer offsides;
        private Integer cornerKicks;
        private Integer freeKicks;
        private Integer shots;
        private Integer shotsTarget;
        private Integer ballPossession;
    }

    @Data
    @Builder
    public static class LineupPlayer {
        private long id;
        private String firstName;
        private String lastName;
        private int number;
        private int position;
    }

    @Data
    @Builder
    public static class FbMiniMatch {
        private String id;
        private List<Contestant> contestants;
        private String dateTime;
        private long dateTimeInt;
        private String tournamentId;
        private String tournamentName;

    }
}
