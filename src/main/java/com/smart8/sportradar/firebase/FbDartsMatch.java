package com.smart8.sportradar.firebase;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
public class FbDartsMatch extends FbMatch{

    private DartsLiveData liveData;

    public FbDartsMatch(){}

    @Override
    public void setLiveData(LiveData liveData) {
        this.liveData = (DartsLiveData) liveData;
    }

    @Override
    public Map<String, Integer> getScore() {
        return liveData.getScore();
    }

    public DartsLiveData getLiveData() {
        return liveData;
    }

    @Data
    @Builder
    @AllArgsConstructor
    public static class DartsLiveData implements LiveData{
        private int minutes;

        /** key: teamId, value: teams goals*/
        private Map<String, Integer> score;

        /** List of scores by period <teamId -> goals */
        private List<Map<String, Integer>> sets;

    }




}
