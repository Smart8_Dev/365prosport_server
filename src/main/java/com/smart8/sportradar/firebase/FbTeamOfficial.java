package com.smart8.sportradar.firebase;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class FbTeamOfficial {
    public String lastName;
    public String type;
    public String firstName;
    public String fullName;

    public FbTeamOfficial(String type, String fullName) {
        this.type = type;
        this.fullName = fullName;

        if(fullName != null){
            final String[] split = fullName.split(" ");
            lastName = split[0];
            firstName = split.length > 1 ? split[1] : null;
        }
    }

    public FbTeamOfficial() {
    }
}
