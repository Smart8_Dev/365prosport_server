package com.smart8.sportradar.firebase;

import lombok.Data;

@Data
public class FbUserFriend {
    private int coins;
    private int rank;
    private String userId;
    private String userName;
}
