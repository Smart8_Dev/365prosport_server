package com.smart8.sportradar.firebase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.fb.Referee;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public abstract class FbMatch {

    private String id;
    private boolean canceled;
    private List<Contestant> contestants;
    private String dateTime;
    private Long dateTimeInt;
    private Long gameEndedInt;
    private Boolean postponed;
    private List<Referee> referee;
    private Long tournamentId;
    private String tournamentName;
    private String subTournamentId;
    private Long venueId;
    private String venueName;
    private int statusCode;
    private String status;
    private List<FbBet> bets;

    public abstract void setLiveData(LiveData liveData);

    @JsonIgnore
    public abstract Map<String, Integer> getScore();
}
