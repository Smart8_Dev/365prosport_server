package com.smart8.sportradar.fb;

import com.smart8.sportradar.enums.RefereeType;
import lombok.Data;

@Data
public class Referee {
    private String name;
    private RefereeType type;

    public Referee() {}

    public Referee(String name, RefereeType type) {
        this.name = name;
        this.type = type;
    }
}