
package com.smart8.sportradar.fb;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class Contestant {
    private long id;
    private String code;
    private String name;
    private String position;
    private String lastFive;
    private List<Long> lastFiveId;

    public Contestant(){}
}
