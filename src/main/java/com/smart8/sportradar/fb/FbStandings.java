package com.smart8.sportradar.fb;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class FbStandings {

    public enum StandingColumn {
        matchesTotal, pointsTotal, goalsForTotal, goalsAgainstTotal, changeTotal, winTotal, drawTotal, lossTotal, positionTotal
    }

    private String groupName;
    private Map<String, FbStandingRow> standings;

    @Data
    @Builder
    public static class FbStandingRow {
        private String teamId;
        private String teamName;
        private int points;
        private int goalsFor;
        private int goalsAgainst;
        private int change;
        private int games;
        private int win;
        private int draw;
        private int lose;
    }
}
