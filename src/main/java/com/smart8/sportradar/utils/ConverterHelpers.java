package com.smart8.sportradar.utils;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.firebase.FbTeam;
import com.smart8.sportradar.firebase.FbTournament;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.statisticsmodels.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.*;

@Slf4j
public class ConverterHelpers {

    public static DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.of("UTC"));
    public static DateTimeFormatter shortDateFormat = DateTimeFormatter.ofPattern("yy-MM-dd");
    public static DateTimeFormatter matchDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneId.of("UTC"));
    public static final DateTimeFormatter unibetFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm'Z'").withZone(ZoneId.of("UTC"));
    public static DateTimeFormatter newsFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneId.of("UTC"));
    public static DateTimeFormatter matchDateCETReader = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss z");

    public static String parseXmlGCDateToStrUTC(XMLGregorianCalendar gregorianCalendar){
        final GregorianCalendar calendar = gregorianCalendar.toGregorianCalendar();
        return matchDateFormat.format(calendar.toZonedDateTime());
    }

    /**
     * Parse Xml Gregorian Calendar date to long
     */
    public static long parseXmlGCDateToLong(XMLGregorianCalendar gregorianCalendar){
        final GregorianCalendar calendar = gregorianCalendar.toGregorianCalendar();
        return calendar.toZonedDateTime().toInstant().toEpochMilli();
    }

    public static String parseMatchDateToYYYY(String matchDate){
        final TemporalAccessor parse = matchDateFormat.parse(matchDate);
        return sdf.format(parse);
    }

    public static long dateUtcToLong(String dateUtc){
        final TemporalAccessor parse = matchDateFormat.parse(dateUtc);

        return Instant.from(parse).toEpochMilli();
    }

    /**
     * Parse dateTime of match in ISO-8601 format.
     * @param matchDate pattern 'yyyy-MM-dd'T'HH:mm:ss z'
     * @return epoch time in millis
     */
    public static long parseMatchDateTimeToLong(String matchDate){
        final TemporalAccessor time = matchDateCETReader.parse(matchDate);
        return Instant.from(time).toEpochMilli();
    }

    /**
     * Parse dateTime of match in ISO-8601 format.
     * @param date  pattern 'yyyy-MM-dd'T'HH:mm:ss z'
     * @return return UTC time. Pattern yyyy-MM-dd'T'HH:mm:ss'Z'
     */
    public static String parseDateOfMatchString(String date){
        try {
            final TemporalAccessor converted = matchDateCETReader.parse(date);
            return matchDateFormat.format(converted);
        } catch (Exception e) {
            log.error("@parseDateOfMatchString > error converting date: " + date, e);
            return date;
        }
    }

    /**
     * Split player full name to first name and second name
     * @param fullName  Assume that If full name spited with coma than last name goes first. If no coma than last name is second
     * @return array [lastName, firstName]
     */
    public static String[] splitPlayerFullName(String fullName){
        String firstName = "";
        String lastName;
        final String[] split;

        if (fullName.contains(",")){
            split = fullName.split(",");
            lastName = split[0].trim();
            if (split.length > 1 ){
                firstName = split[1].trim();
            }
        }else{
            split = fullName.split(" ", 2);

            if ( split.length == 1){
                lastName = split[0];
            }else{
                firstName = split[0];
                lastName = split[1];
            }
        }

        return new String[]{lastName, firstName};
    }


    @Deprecated
    public static List<FbTeam> parsePreMatchTeams(Match match, FirebaseService firebaseService) {
        final List<Team> list = match.getTeams().getTeam();

        final List<FbTeam> contestants = new ArrayList<>();
        for (Team team : list) {
            final FbTeam team1 = firebaseService.getTeam(String.valueOf(team.getId()));
            if(team1 != null){
                final FbTeam build = FbTeam.builder()
                        .code(team1.getCode())
                        .name(team1.getName())
                        .formation(team.getFormation() != null ? team.getFormation().replace("-", "") : null)
                        .dressInfo(team1.dressInfo)
                        .position(team.getType() == 1 ? "home" : "away").build();

                contestants.add(build);
            }
            else{
                contestants.add(buildTeam(team));
            }
        }

        return contestants;
    }

    private static FbTeam buildTeam(Team team) {
        return (FbTeam.builder()
                .id(String.valueOf(team.getId()))
                .code(team.getName())
                .position(team.getType() == 1 ? Constants.HOME : Constants.AWAY)
                .name(team.getName()).build());
    }

    public static void fillTournamentDetails(@NotNull Category category, @NotNull FbTournament.FbTournamentBuilder builder){
        final int id = category.getId();
        final String name = category.getName();

        if(!name.equalsIgnoreCase(Constants.INTERNATIONAL)){
            builder.country(name);
        }

        builder.countryId(String.valueOf(id));
        builder.country(name);

        final Tournament tournament = category.getTournament().get(0);


        builder.id(tournament.getUniqueTournamentId().toString());
        builder.name(tournament.getName());

        final List<Season> season = tournament.getSeason();
        if(MyCollections.isNotEmpty(season)){
            final Season season1 = season.get(0);
            builder.seasonName(season1.getName());
            builder.seasonId(String.valueOf(season1.getId()));
        }
    }

    public static long getTournamentId(Tournament tournament){
        if(tournament.getUniqueTournamentId() != null){
            return tournament.getUniqueTournamentId();
        }
        else {
            return tournament.getId();
        }
    }


    public static long safeBigLong(BigInteger integer){
        if(integer == null){
            return Constants.NO_VALUE;
        }

        return integer.longValue();
    }

    public static int safeBigInteger(BigInteger integer){
        if(integer == null){
            return Constants.NO_VALUE;
        }

        return integer.intValue();
    }



    /**
     * Remove null values from map and flatten to one level map.
     * This is for firebase so it won't rewrite values.
     * @param map map to flatten
     * @param parentKey key that will be added before current key with slash. Parent key / current Key
     * @return flatten map
     */
    public static Map<String, Object> removeNullsAndFlattenMap(Map map, String parentKey){
        Map<String, Object> flattenMap = new HashMap<>();

        for ( Map.Entry<String, Object> entry : ((Map<String, Object>)map).entrySet()){
            if( entry.getValue() instanceof Map){
                Map<String, Object> internalMap = removeNullsAndFlattenMap((Map) entry.getValue(), parentKey + entry.getKey() + "/");
                flattenMap.putAll(internalMap);
            }else if ( entry.getValue() instanceof List) {
                Map<String, Object> internalMap = new HashMap<>();
                int count = 0;
                for( Object val : (List)entry.getValue()) {
                    if ( val instanceof Map ){
                        internalMap.putAll(removeNullsAndFlattenMap((Map) val, parentKey + entry.getKey() + "/" + count + "/"));
                    }else{
                        internalMap.put(parentKey + entry.getKey() + "/" + count, val);
                    }

                    count++;
                }
                flattenMap.putAll(internalMap);
            }else if ( entry.getValue() != null){
                flattenMap.put(parentKey + entry.getKey(), entry.getValue());
            }
        }
        return flattenMap;
    }


    public static int getMinutesDiff(long startTime, long endTime) {
        long diff = endTime - startTime;
        return (int)( diff/(60 * 1000));
    }

    /**
     * Convert Team full name to 3 letter Code. Use [.,-] for split
     * @param fullName team name
     * @return team code
     */
    public static String createTeamCode(String fullName) {
        if ( StringUtils.isEmpty(fullName)) {
            return null;
        }

        String[] parts = org.apache.commons.lang3.StringUtils.split(fullName.replaceAll("[.,-]", " "));
        String code = "";

        if (parts.length > 2) {
            code = parts[0].substring(0, 1) + parts[1].substring(0, 1) + parts[2].substring(0, 1);
        } else if (parts.length == 2) {
            if (parts[0].length() > 1) {
                code = parts[0].substring(0, 2) + parts[1].substring(0, 1);
            } else if (parts[1].length() > 1 ) {
                code = parts[0].substring(0, 1) + parts[1].substring(0, 2);
            }else{
                code = parts[0].substring(0, 1) + parts[1].substring(0, 1);
            }
        } else if (parts.length == 1) {
            if ( parts[0].length() > 3){
                code = parts[0].substring(0, 3);
            }else{
                code = parts[0];
            }
        }
        return code.toUpperCase();
    }
}
