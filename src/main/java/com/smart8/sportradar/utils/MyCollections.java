package com.smart8.sportradar.utils;

import java.util.Collection;

public class MyCollections {

    public static  <T> boolean isEmpty(Collection<T> collection){
        return collection == null || collection.isEmpty();
    }

    public static <T> boolean isNotEmpty(Collection<T> collection){
        return collection != null && !collection.isEmpty();
    }
}
