package com.smart8.sportradar.utils;

import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.models.MiniMatch;
import com.smart8.sportradar.models.unibet.UniResult;
import com.smart8.sportradar.repositories.MatchRepository;
import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDetailedDistance;
import org.apache.commons.text.similarity.LevenshteinResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class MatchComperator {

    public static final int NO_VALUE = -1;

    //3 hours apart
    public static final long SAME_DATE_THREASHOLD = 60 * 60 * 3;

    @Autowired
    MatchRepository repository;

    private LevenshteinDetailedDistance distance = new LevenshteinDetailedDistance(3);

    @AllArgsConstructor
    @ToString
    public class MatchMapper {
        public final long unibetId;
        public final String sRadarId;
        public final TeamMapper teamA;
        public final TeamMapper teamB;
    }

    @ToString
    @AllArgsConstructor
    public class TeamMapper {
        public final String sRadarTeam;
        public final long unibetTeam;
        public final boolean hasMapping;
    }

    public Optional<MatchMapper> matchesComperator(UniResult.Event event, MiniMatch match){
        final String start = event.getStart();
        final TemporalAccessor uniMatchDate = ConverterHelpers.unibetFormat.parse(start);
        final Instant matchDate = Instant.from(ConverterHelpers.matchDateFormat.parse(match.getDateTime()));
        final Instant uniDate = Instant.from(uniMatchDate);

        final List<UniResult.Participant> participants = event.getParticipants();
        final UniResult.Participant participantA = participants.get(0);
        final UniResult.Participant participantB = participants.get(1);

        final List<Contestant> contestants = match.getContestants();
        final Contestant fbTeamA = contestants.get(0);
        final Contestant fbTeamB = contestants.get(1);

        if(Math.abs(matchDate.getEpochSecond() - uniDate.getEpochSecond()) > SAME_DATE_THREASHOLD){
            return Optional.empty();
        }

        final Optional<TeamMapper> teamMapper = teamsComperator(participantA, fbTeamA);

        if(teamMapper.isPresent()){
            final Optional<TeamMapper> teamMapper1 = teamsComperator(participantB, fbTeamB);

            return teamMapper1.map(mapper -> new MatchMapper(event.getId(), match.getId(), teamMapper.get(), mapper));
        }
        else{
            final Optional<TeamMapper> teamMapper2 = teamsComperator(participantA, fbTeamB);

            if(!teamMapper2.isPresent()){
                log.info("Can't find sportradar team equivalent for {}", participantA.getName());
                return Optional.empty();
            }

            //found, now just compare other
            final Optional<TeamMapper> teamMapper3 = teamsComperator(participantB, fbTeamA);

            return teamMapper3.map(mapper -> new MatchMapper(event.getId(), match.getId(), teamMapper2.get(), mapper));
        }
    }

    public Optional<TeamMapper> teamsComperator(UniResult.Participant participant, Contestant fbTeam){
        if(fbTeam == null){
            return Optional.empty();
        }
        else if (fbTeam.getId() == 0){
            log.error("@teamsComperator > fbTeamId is null! " + fbTeam);
        }
        final String radarId = repository.getRadarMatchIdFromUnibetMatchId(participant.getParticipantId());

        if(fbTeam.getId() == 0 && String.valueOf(fbTeam.getId()).equalsIgnoreCase(radarId)){
            return Optional.of(new TeamMapper(radarId, participant.getParticipantId(), true));
        }
        else{
            if(StringUtils.isEmpty(fbTeam.getName())){
                return Optional.empty();
            }

            final String participantName = participant.getName().trim().toLowerCase();
            final String teamName = fbTeam.getName().trim().toLowerCase();

            final LevenshteinResults results = distance.apply(participantName, teamName);

            final Integer distance = results.getDistance();

            if(distance == null || distance == NO_VALUE || distance > 3){
                if(StringUtils.contains(participantName, teamName) || StringUtils.contains(teamName, participantName)){
                    return Optional.of(new TeamMapper(String.valueOf(fbTeam.getId()), participant.getParticipantId(), false));
                }
                return Optional.empty();
            }

            final Optional<TeamMapper> teamMapper = Optional.of(new TeamMapper(String.valueOf(fbTeam.getId()), participant.getParticipantId(), false));

            if(distance != 0) {
                log.info("@teamsComperator > similar teams: {}, {} distance is: {}", participantName, teamName, distance);
            }

            return teamMapper;
        }
    }
}
