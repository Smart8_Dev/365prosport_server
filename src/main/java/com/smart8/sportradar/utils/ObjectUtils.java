package com.smart8.sportradar.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings("unused")
public class ObjectUtils {

    /** Returns true only if object is boolean true
     */
    public static Boolean safeIsTrue(Boolean obj ){
        return obj == null ? false : obj;
    }

    /**
     * Display object in json style
     */
    @SuppressWarnings("unused")
    public static <E> void displayObj(E obj){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        String s = "";
        try {
            s = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println(s);
    }
}
