//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.28 at 05:11:02 PM IDT 
//


package com.smart8.sportradar.statisticsmodels;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for season complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="season">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlexport.scoreradar.com/V1}translatable">
 *       &lt;sequence>
 *         &lt;element name="LocalizedDates" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LocalizedDate" type="{http://xmlexport.scoreradar.com/V1}localizedDate" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RefereeStatistics" type="{http://xmlexport.scoreradar.com/V1}refereeStatisticsEntries" minOccurs="0"/>
 *         &lt;element name="SeasonStatistics" type="{http://xmlexport.scoreradar.com/V1}teamSeasonStatistics" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="end" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="externalId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="start" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="year" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "season", propOrder = {
    "localizedDates",
    "refereeStatistics",
    "seasonStatistics"
})
@XmlSeeAlso({
    TennisSeason.class
})
public class Season
    extends Translatable
{

    @XmlElement(name = "LocalizedDates")
    protected LocalizedDates localizedDates;
    @XmlElement(name = "RefereeStatistics")
    protected RefereeStatisticsEntries refereeStatistics;
    @XmlElement(name = "SeasonStatistics")
    protected TeamSeasonStatistics seasonStatistics;
    @XmlAttribute(name = "end")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar end;
    @XmlAttribute(name = "externalId")
    protected String externalId;
    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "start")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar start;
    @XmlAttribute(name = "year")
    protected String year;

    /**
     * Gets the value of the localizedDates property.
     * 
     * @return
     *     possible object is
     *     {@link LocalizedDates }
     *     
     */
    public LocalizedDates getLocalizedDates() {
        return localizedDates;
    }

    /**
     * Sets the value of the localizedDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalizedDates }
     *     
     */
    public void setLocalizedDates(LocalizedDates value) {
        this.localizedDates = value;
    }

    /**
     * Gets the value of the refereeStatistics property.
     * 
     * @return
     *     possible object is
     *     {@link RefereeStatisticsEntries }
     *     
     */
    public RefereeStatisticsEntries getRefereeStatistics() {
        return refereeStatistics;
    }

    /**
     * Sets the value of the refereeStatistics property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefereeStatisticsEntries }
     *     
     */
    public void setRefereeStatistics(RefereeStatisticsEntries value) {
        this.refereeStatistics = value;
    }

    /**
     * Gets the value of the seasonStatistics property.
     * 
     * @return
     *     possible object is
     *     {@link TeamSeasonStatistics }
     *     
     */
    public TeamSeasonStatistics getSeasonStatistics() {
        return seasonStatistics;
    }

    /**
     * Sets the value of the seasonStatistics property.
     * 
     * @param value
     *     allowed object is
     *     {@link TeamSeasonStatistics }
     *     
     */
    public void setSeasonStatistics(TeamSeasonStatistics value) {
        this.seasonStatistics = value;
    }

    /**
     * Gets the value of the end property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnd() {
        return end;
    }

    /**
     * Sets the value of the end property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnd(XMLGregorianCalendar value) {
        this.end = value;
    }

    /**
     * Gets the value of the externalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalId() {
        return externalId;
    }

    /**
     * Sets the value of the externalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalId(String value) {
        this.externalId = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the start property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStart() {
        return start;
    }

    /**
     * Sets the value of the start property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStart(XMLGregorianCalendar value) {
        this.start = value;
    }

    /**
     * Gets the value of the year property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the value of the year property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYear(String value) {
        this.year = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LocalizedDate" type="{http://xmlexport.scoreradar.com/V1}localizedDate" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "localizedDate"
    })
    public static class LocalizedDates {

        @XmlElement(name = "LocalizedDate")
        protected List<LocalizedDate> localizedDate;

        /**
         * Gets the value of the localizedDate property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the localizedDate property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLocalizedDate().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LocalizedDate }
         * 
         * 
         */
        public List<LocalizedDate> getLocalizedDate() {
            if (localizedDate == null) {
                localizedDate = new ArrayList<LocalizedDate>();
            }
            return this.localizedDate;
        }

    }

}
