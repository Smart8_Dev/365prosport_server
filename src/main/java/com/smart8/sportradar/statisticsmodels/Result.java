//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.28 at 05:11:02 PM IDT 
//


package com.smart8.sportradar.statisticsmodels;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="result">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlexport.scoreradar.com/V1}translatable">
 *       &lt;sequence>
 *         &lt;element name="Comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResultComment" type="{http://xmlexport.scoreradar.com/V1}resultComment" minOccurs="0"/>
 *         &lt;element name="Score" type="{http://xmlexport.scoreradar.com/V1}score" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="canceled" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="decisionbyFA" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="postponedNewMatchid" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="postponed" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "result", propOrder = {
    "comment",
    "resultComment",
    "score"
})
public class Result
    extends Translatable
{

    @XmlElement(name = "Comment")
    protected String comment;
    @XmlElement(name = "ResultComment")
    protected ResultComment resultComment;
    @XmlElement(name = "Score")
    protected List<Score> score;
    @XmlAttribute(name = "canceled")
    protected Boolean canceled;
    @XmlAttribute(name = "decisionbyFA")
    protected Boolean decisionbyFA;
    @XmlAttribute(name = "postponedNewMatchid")
    protected Integer postponedNewMatchid;
    @XmlAttribute(name = "postponed")
    protected Boolean postponed;

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the resultComment property.
     * 
     * @return
     *     possible object is
     *     {@link ResultComment }
     *     
     */
    public ResultComment getResultComment() {
        return resultComment;
    }

    /**
     * Sets the value of the resultComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultComment }
     *     
     */
    public void setResultComment(ResultComment value) {
        this.resultComment = value;
    }

    /**
     * Gets the value of the score property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the score property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Score }
     * 
     * 
     */
    public List<Score> getScore() {
        if (score == null) {
            score = new ArrayList<Score>();
        }
        return this.score;
    }

    /**
     * Gets the value of the canceled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCanceled() {
        return canceled;
    }

    /**
     * Sets the value of the canceled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanceled(Boolean value) {
        this.canceled = value;
    }

    /**
     * Gets the value of the decisionbyFA property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDecisionbyFA() {
        return decisionbyFA;
    }

    /**
     * Sets the value of the decisionbyFA property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDecisionbyFA(Boolean value) {
        this.decisionbyFA = value;
    }

    /**
     * Gets the value of the postponedNewMatchid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPostponedNewMatchid() {
        return postponedNewMatchid;
    }

    /**
     * Sets the value of the postponedNewMatchid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPostponedNewMatchid(Integer value) {
        this.postponedNewMatchid = value;
    }

    /**
     * Gets the value of the postponed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPostponed() {
        return postponed;
    }

    /**
     * Sets the value of the postponed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPostponed(Boolean value) {
        this.postponed = value;
    }

}
