//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.28 at 05:11:02 PM IDT 
//


package com.smart8.sportradar.statisticsmodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for teamSeasonStatistics complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="teamSeasonStatistics">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlexport.scoreradar.com/V1}translatable">
 *       &lt;sequence>
 *         &lt;element name="Overall" type="{http://xmlexport.scoreradar.com/V1}teamSeasonStatisticsEntries" minOccurs="0"/>
 *         &lt;element name="Home" type="{http://xmlexport.scoreradar.com/V1}teamSeasonStatisticsEntries" minOccurs="0"/>
 *         &lt;element name="Away" type="{http://xmlexport.scoreradar.com/V1}teamSeasonStatisticsEntries" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="teamsInSeason" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "teamSeasonStatistics", propOrder = {
    "overall",
    "home",
    "away"
})
public class TeamSeasonStatistics
    extends Translatable
{

    @XmlElement(name = "Overall")
    protected TeamSeasonStatisticsEntries overall;
    @XmlElement(name = "Home")
    protected TeamSeasonStatisticsEntries home;
    @XmlElement(name = "Away")
    protected TeamSeasonStatisticsEntries away;
    @XmlAttribute(name = "teamsInSeason")
    protected Integer teamsInSeason;

    /**
     * Gets the value of the overall property.
     * 
     * @return
     *     possible object is
     *     {@link TeamSeasonStatisticsEntries }
     *     
     */
    public TeamSeasonStatisticsEntries getOverall() {
        return overall;
    }

    /**
     * Sets the value of the overall property.
     * 
     * @param value
     *     allowed object is
     *     {@link TeamSeasonStatisticsEntries }
     *     
     */
    public void setOverall(TeamSeasonStatisticsEntries value) {
        this.overall = value;
    }

    /**
     * Gets the value of the home property.
     * 
     * @return
     *     possible object is
     *     {@link TeamSeasonStatisticsEntries }
     *     
     */
    public TeamSeasonStatisticsEntries getHome() {
        return home;
    }

    /**
     * Sets the value of the home property.
     * 
     * @param value
     *     allowed object is
     *     {@link TeamSeasonStatisticsEntries }
     *     
     */
    public void setHome(TeamSeasonStatisticsEntries value) {
        this.home = value;
    }

    /**
     * Gets the value of the away property.
     * 
     * @return
     *     possible object is
     *     {@link TeamSeasonStatisticsEntries }
     *     
     */
    public TeamSeasonStatisticsEntries getAway() {
        return away;
    }

    /**
     * Sets the value of the away property.
     * 
     * @param value
     *     allowed object is
     *     {@link TeamSeasonStatisticsEntries }
     *     
     */
    public void setAway(TeamSeasonStatisticsEntries value) {
        this.away = value;
    }

    /**
     * Gets the value of the teamsInSeason property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTeamsInSeason() {
        return teamsInSeason;
    }

    /**
     * Sets the value of the teamsInSeason property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTeamsInSeason(Integer value) {
        this.teamsInSeason = value;
    }

}
