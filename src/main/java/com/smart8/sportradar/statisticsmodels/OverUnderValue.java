//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.28 at 05:11:02 PM IDT 
//


package com.smart8.sportradar.statisticsmodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for overUnderValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="overUnderValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="goals" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="over" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="under" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "overUnderValue")
public class OverUnderValue {

    @XmlAttribute(name = "goals")
    protected Double goals;
    @XmlAttribute(name = "over")
    protected Integer over;
    @XmlAttribute(name = "under")
    protected Integer under;

    /**
     * Gets the value of the goals property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getGoals() {
        return goals;
    }

    /**
     * Sets the value of the goals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setGoals(Double value) {
        this.goals = value;
    }

    /**
     * Gets the value of the over property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOver() {
        return over;
    }

    /**
     * Sets the value of the over property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOver(Integer value) {
        this.over = value;
    }

    /**
     * Gets the value of the under property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnder() {
        return under;
    }

    /**
     * Sets the value of the under property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnder(Integer value) {
        this.under = value;
    }

}
