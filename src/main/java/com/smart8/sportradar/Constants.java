package com.smart8.sportradar;

public class Constants {

    public static final String HOME = "home";
    public static final String AWAY = "away";
    public static final String MANAGER = "manager";

    public static final String RED = "Red";
    public static final String YELLOW = "Yellow";
    public static final String Color = "color";

    public static final String INTERNATIONAL = "international";
    public static final String ZERO = "0";
    public static final int NO_VALUE = -1;
    public static final int DARTS_ID = 22;
    public static final String DELTA_TYPE = "delta";
    public static final int MILLIS_IN_MINUTE = 60000;
    public static final int MATCH_NORMAL_TIME = 90;
    public static final int MATCH_HALF_MINUTES = 45;
    public static final int MATCH_EXTRA_MINUTES = 30;
    public static final int MATCH_EXTRA_TIME = 120;
    public static final int EXTRA_TIME_HALF_MIN = 15;
    public static final int MATCH_TIME_AFTER_FIRST_EXTRA = 105;
    public static final String NEW_LINE = "\n";
    public static final String ODDS_DELIM = "/";

    public static final int SPORTRADAR_MATCH_MAX_EXTRA_THRESHOLD = 10;
    public static final int SPORTRADAR_MATCH_EXTRA_THRESHOLD = 5;

    public static final int LAST_X_GAMES = 5;

    public static final int REDIS_MAX_VIEW_ITEMS = 10;
}
