package com.smart8.sportradar.converters;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.converters.live.LiveMatchConverter;
import com.smart8.sportradar.converters.live.DartsLiveMatchConverter;
import com.smart8.sportradar.converters.live.SoccerLiveMatchConverter;
import com.smart8.sportradar.firebase.DataToPersist;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbTournament;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament.Match;
import com.smart8.sportradar.services.BetResolveService;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.handlers.MatchEventsHandler;
import com.smart8.sportradar.services.StatusCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

import static com.smart8.sportradar.Constants.DELTA_TYPE;
import static com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport;

@Slf4j
@Component
public class DocumentToFbDataConverter implements Converter<BetradarLivescoreData, DataToPersist> {

    private SoccerLiveMatchConverter soccerConverter;
    private DartsLiveMatchConverter dartsConverter;
    private FirebaseService firebaseService;
    private MatchEventsHandler matchEventsService;
    private BetResolveService betResolveService;


    @Autowired
    DocumentToFbDataConverter(FirebaseService fbs,
                              MatchEventsHandler mService,
                              BetResolveService betService,
                              SoccerLiveMatchConverter sConverter,
                              DartsLiveMatchConverter dConverter){
        this.firebaseService = fbs;
        this.matchEventsService = mService;
        this.betResolveService = betService;
        this.soccerConverter = sConverter;
        this.dartsConverter = dConverter;
    }

    /**
     * Convert and save matches
     */
    @Nonnull
    @Override
    public DataToPersist convert(BetradarLivescoreData source) {

        DataToPersist.Builder builder = new DataToPersist.Builder();

        LiveMatchConverter matchConverter;

        for (Sport sport : source.getSport()) {

            List<FbMatch> fbMatchList= new ArrayList<>();

            final int sportId = sport.getBetradarSportId().intValue();
            SportType sportType;

            try{
                sportType = SportType.findById(sportId);
            }catch (UnsupportedOperationException ex){
                log.error("Sport id '{}' is not allowed", sportId );
                return builder.build();
            }

            if ( sportType == SportType.Darts){
                matchConverter = dartsConverter;
            }else{
                matchConverter = soccerConverter;
            }

            final List<BetradarLivescoreData.Sport.Category> categories = sport.getCategory();

            for (Sport.Category category : categories) {

                final List<BetradarLivescoreData.Sport.Category.Tournament> tournaments = category.getTournament();
                for (Sport.Category.Tournament tournament : tournaments) {

                    if ( sportType == SportType.Soccer && !firebaseService.isAllowed(tournament)) {
                        log.info("Tournament id: {} is not allowed: ", tournament.getUniqueTournamentId());
                        continue;
                    }
                    FbTournament fbTournament = firebaseService.getTournament(tournament.getUniqueTournamentId().toString());

                    if (sportType == SportType.Soccer && fbTournament == null){
                        log.warn("Missing tournament {}", tournament.getUniqueTournamentId().toString());
                        continue;
                    }

                    final List<Match> matchList = tournament.getMatch();

                    for (Match match : matchList) {

                        final FbMatch fbMatch = matchConverter.convert(tournament,  match);
                        log.debug("Converted match {}", fbMatch);
                        fbMatchList.add(fbMatch);
                        handleNotStarted(builder, fbMatch);
                    }
                }
            }

            firebaseService.saveMatches(fbMatchList, sportType.name().toLowerCase());

            fbMatchList.forEach(fbMatch -> {
                matchEventsService.handleMatchEvents(fbMatch);
                if (!source.getType().equals(DELTA_TYPE) ){
                    betResolveService.resolve(fbMatch);
                }
            });
        }
        return builder.build();
    }


    private void handleNotStarted(DataToPersist.Builder builder, FbMatch fbMatch) {
        StatusCode statusObj = StatusCode.getByCode(fbMatch.getStatusCode());

        if(StatusCode.notStarted(statusObj)){
            builder.addMatchNotStarted(fbMatch);
        }
        else if(StatusCode.isEnded(statusObj)){
            builder.addMatchEnded(fbMatch);
        }
        else{
            builder.addMatchLive(fbMatch);
        }
    }


}
