package com.smart8.sportradar.converters.stats;

import com.smart8.sportradar.firebase.FbPlayer;
import com.smart8.sportradar.statisticsmodels.Player;
import com.smart8.sportradar.statisticsmodels.PlayerInfoEntry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PlayerToFbPlayerConverter {

    public FbPlayer convertRadarPlayerToFbPlayer(Player radarPlayer){
        FbPlayer.FbPlayerBuilder fbPlayerBuilder = FbPlayer.builder().id(radarPlayer.getPlayerID());

        for ( PlayerInfoEntry playerInfo : radarPlayer.getPlayerInfo().getPlayerInfoEntry()){
            switch ( playerInfo.getName() ){
                case "First name":
                    fbPlayerBuilder.firstName(playerInfo.getValue());
                    break;
                case "Last name":
                    fbPlayerBuilder.lastName(playerInfo.getValue());
                    break;
                case "Position":
                    fbPlayerBuilder.position(playerInfo.getValue());
                    break;
                case "Shirt number":
                    fbPlayerBuilder.number(playerInfo.getValue());
                    break;
                case "Weight":
                    fbPlayerBuilder.weight(Integer.parseInt(playerInfo.getValue()));
                    break;
                case "Height" :
                    fbPlayerBuilder.height(Integer.parseInt(playerInfo.getValue()));
                    break;
                case "Date of birth" :
                    fbPlayerBuilder.birth(playerInfo.getValue());
                    break;
                case "Preferred foot" :
                    fbPlayerBuilder.foot(playerInfo.getValue().toLowerCase());
                    break;
                case "Nationality" :
                    fbPlayerBuilder.country(playerInfo.getValue());
                    break;
                default:
                    break;
            }
        }

        return fbPlayerBuilder.build();
    }

}


