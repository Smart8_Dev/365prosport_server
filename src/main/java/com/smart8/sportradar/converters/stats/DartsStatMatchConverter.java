package com.smart8.sportradar.converters.stats;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.FbDartsMatch;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.firebase.LiveData;
import com.smart8.sportradar.handlers.CancelledMatchHandler;
import com.smart8.sportradar.statisticsmodels.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DartsStatMatchConverter extends StatMatchConverter {

    @Autowired
    private CancelledMatchHandler cancelledMatchHandler;

    @Override
    protected FbMatch createMatch() {
        return new FbDartsMatch();
    }

    @Override
    protected LiveData createLiveData(Match match) {
        final FbSoccerMatch.SoccerLiveData.SoccerLiveDataBuilder builder = FbSoccerMatch.SoccerLiveData.builder();
        builder.score(super.createScore(match));

        return builder.build();
    }

    @Override
    void sendMatchCancelledMessage(String matchId) {
        cancelledMatchHandler.sendMatchCancelledMessage(SportType.Darts, matchId);
    }

    /**@inheritDoc*/
    @Override
    protected String getTeamPositionByIndex(int index) {
        return String.valueOf(index + 1);
    }

}
