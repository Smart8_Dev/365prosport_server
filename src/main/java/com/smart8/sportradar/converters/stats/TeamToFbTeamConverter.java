package com.smart8.sportradar.converters.stats;

import com.smart8.sportradar.firebase.FbTeam;
import com.smart8.sportradar.statisticsmodels.Team;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Slf4j
@Component
public class TeamToFbTeamConverter {

    /**
     * Create new FbTeam from sportradarTeam
     * @param radarTeam sportradar team
     * @return created FbTeam
     */
    public FbTeam createFbTeam(Team radarTeam){
        return FbTeam.builder()
            .id(radarTeam.getId().toString())
            .code(ConverterHelpers.createTeamCode(radarTeam.getName()))
            .name(radarTeam.getName())
            .build();
    }

    /**
     * Update FbTeam with sportradar team data. Updates fbTeam ID.
     * @return false - if no updates were made
     */
    boolean updateFbTeam(FbTeam fbTeam, Team radarTeam){

        final String code = ConverterHelpers.createTeamCode(radarTeam.getName());

        if (  !Objects.equals(code, fbTeam.getCode()) || !Objects.equals(radarTeam.getName(), fbTeam.getName() )) {
            fbTeam.id = radarTeam.getId().toString();
            fbTeam.code = code;
            fbTeam.name = radarTeam.getName();
            return true;
        } else {
            return false;
        }
    }

}
