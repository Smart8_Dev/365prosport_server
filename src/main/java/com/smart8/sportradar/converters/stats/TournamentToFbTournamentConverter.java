package com.smart8.sportradar.converters.stats;

import com.fasterxml.jackson.core.type.TypeReference;
import com.smart8.sportradar.firebase.FbTournament;
import com.smart8.sportradar.mappers.ResourceMapper;
import com.smart8.sportradar.statisticsmodels.Category;
import com.smart8.sportradar.statisticsmodels.Season;
import com.smart8.sportradar.statisticsmodels.Tournament;
import com.smart8.sportradar.statisticsmodels.UniqueTournament;
import com.smart8.sportradar.utils.MyCollections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TournamentToFbTournamentConverter {

    private static final String TOURNAMENTS_PRIORITY_JSON = "appdata/tournaments-priority.json";

    private Map<String, Integer> tournamentsPriority;

    @Autowired
    TournamentToFbTournamentConverter(ResourceMapper resourceMapper){
        this.tournamentsPriority = resourceMapper.readDataToMap(TOURNAMENTS_PRIORITY_JSON, new TypeReference<Map<String, Integer>>(){});
    }

    public FbTournament convertTournamentToFbTournament(Category category, Tournament tournament) {
        String id, name;
        if (MyCollections.isNotEmpty(tournament.getUniqueTournament()) ){
            UniqueTournament uniqueTournament = tournament.getUniqueTournament().get(0);
            id = uniqueTournament.getId().toString();
            name = uniqueTournament.getName();
        }else{
            id = tournament.getId().toString();
            name = tournament.getName();
        }

        FbTournament.FbTournamentBuilder fbTournamentBuilder =  FbTournament.builder()
            .id(id)
            .name(name)
            .country(category.getName())
            .countryId(String.valueOf(category.getId()))
            .priority(tournamentsPriority.getOrDefault(id, 0));

        if ( MyCollections.isNotEmpty(tournament.getSeason())){
            Season season = tournament.getSeason().get(0);
            fbTournamentBuilder.season(season.getYear())
                .seasonId(String.valueOf(season.getId()))
                .seasonName(season.getName());
        }

        return fbTournamentBuilder.build();
    }
}
