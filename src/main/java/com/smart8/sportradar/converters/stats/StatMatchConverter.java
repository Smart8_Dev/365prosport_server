package com.smart8.sportradar.converters.stats;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.enums.RefereeType;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.fb.Referee;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.LiveData;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.statisticsmodels.Match;
import com.smart8.sportradar.statisticsmodels.Score;
import com.smart8.sportradar.statisticsmodels.Team;
import com.smart8.sportradar.statisticsmodels.Tournament;
import com.smart8.sportradar.utils.ConverterHelpers;
import com.smart8.sportradar.utils.ObjectUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public abstract class StatMatchConverter {

    abstract FbMatch createMatch();

    abstract LiveData createLiveData(Match match);

    abstract void sendMatchCancelledMessage(String matchId);

    public FbMatch convert(Tournament tournament, Match match) {

        final FbMatch fbMatch = createMatch();
        fbMatch.setId(match.getId().toString());
        fbMatch.setContestants(createContestants(tournament, match));
        fbMatch.setDateTime(ConverterHelpers.parseXmlGCDateToStrUTC(match.getDateOfMatch()));
        fbMatch.setDateTimeInt(ConverterHelpers.parseXmlGCDateToLong(match.getDateOfMatch()));
        fbMatch.setTournamentId(ConverterHelpers.getTournamentId(tournament));
        fbMatch.setTournamentName(tournament.getName());

        if (match.getReferee() != null) {
            Referee referee = new Referee(match.getReferee().getName(), RefereeType.MAIN);
            fbMatch.setReferee(Collections.singletonList(referee));
        }

        if (match.getStadium() != null) {
            fbMatch.setVenueId(match.getStadium().getId().longValue());
            fbMatch.setVenueName(match.getStadium().getName());
        }

        if (match.getResult() != null) {
            boolean isCancelled = match.getResult().isCanceled();
            boolean isPostponed = ObjectUtils.safeIsTrue(match.getResult().isPostponed());

            if (!isCancelled && !isPostponed) {
                fbMatch.setStatusCode(StatusCode.Ended.statusCode);
                fbMatch.setStatus(StatusCode.Ended.toString());
                fbMatch.setCanceled(false);
                fbMatch.setPostponed(false);
                fbMatch.setLiveData(createLiveData(match));
            } else {
                fbMatch.setCanceled(isCancelled);
                fbMatch.setPostponed(isPostponed);

                if (isPostponed) {
                    fbMatch.setStatusCode(StatusCode.Postponed.statusCode);
                    fbMatch.setStatus(StatusCode.Postponed.name());
                } else {
                    fbMatch.setStatusCode(StatusCode.Cancelled.statusCode);
                    fbMatch.setStatus(StatusCode.Cancelled.name());
                }
                if (isCancelled) {
                    sendMatchCancelledMessage( match.getId().toString());
                }
            }
        } else {
            fbMatch.setStatusCode(StatusCode.NotStarted.statusCode);
            fbMatch.setStatus(StatusCode.NotStarted.toString());
            fbMatch.setCanceled(false);
            fbMatch.setPostponed(false);
        }

        return fbMatch;
    }

    private List<Contestant> createContestants(Tournament tournament, Match match) {
        List<Contestant> contestantList = new ArrayList<>();
        int index = 0;
        for (Team team : match.getTeams().getTeam()) {
            Contestant.ContestantBuilder contestantBuilder = Contestant.builder();

            contestantBuilder.id(team.getId())
                .name(team.getName())
                .code(ConverterHelpers.createTeamCode(team.getName()))
                .position(getTeamPositionByIndex(index));

            //This mean that this is a prematch feed so add match history
            if (tournament.getTeams() != null && tournament.getTeams().getTeam().get(index).getLastMatches() != null) {
                bindLastMatches(tournament.getTeams().getTeam().get(index), contestantBuilder);
            }
            index++;
            contestantList.add(contestantBuilder.build());
        }
        return contestantList;
    }

    /**
     * Get team match positions by its index in feed that starts from 0
     * @return home/away string when its possible otherwise override method with number position
     */
    protected String getTeamPositionByIndex(int index) {
        return index == 0 ? Constants.HOME : Constants.AWAY;
    }


    /**
     * Creates String representation (WWDLW) of last LAST_X_GAMES matches for team in ALL tournaments
     * and List of last 5 matches id. The last one is the latest
     *
     * @param contestantBuilder builder to update with new data
     */
    private void bindLastMatches(Team team, Contestant.ContestantBuilder contestantBuilder) {
        List<Long> lastMatchesId = new ArrayList<>();
        StringBuilder lastResultsBuilder = new StringBuilder();
        int count = 0;
        for (Tournament tournament : team.getLastMatches().getTournament()) {
            Match match = tournament.getMatches().getMatch().get(0);
            if (match.getResult() != null) {
                count++;

                lastMatchesId.add(match.getId().longValue());
                lastResultsBuilder.append(getMatchResultStr(match, team.getId()));

                if (count >= Constants.LAST_X_GAMES) {
                    break;
                }
            }
        }
        Collections.reverse(lastMatchesId);
        contestantBuilder.lastFiveId(lastMatchesId);
        contestantBuilder.lastFive(lastResultsBuilder.reverse().toString());
    }

    private String getMatchResultStr(Match match, Integer teamId) {
        boolean isTeamHome = false;
        for (Team team : match.getTeams().getTeam()) {
            if (team.getId().equals(teamId)) {
                isTeamHome = team.getType() == 1;
                break;
            }
        }
        for (Score score : match.getResult().getScore()) {
            if (score.getType().equalsIgnoreCase("FT")) {
                String[] sc = score.getValue().split(":");
                Integer homeSc = Integer.valueOf(sc[0]);
                Integer awaySc = Integer.valueOf(sc[1]);
                if (homeSc > awaySc) {
                    return isTeamHome ? "W" : "L";
                } else if (homeSc < awaySc) {
                    return isTeamHome ? "L" : "W";
                } else {
                    return "D";
                }
            }
        }
        return "";
    }

    Map<String, Integer> createScore(Match match){
        final String team1Id = match.getTeams().getTeam().get(0).getId().toString();
        final String team2Id = match.getTeams().getTeam().get(1).getId().toString();

        String scoreResult = "0:0";
        for (Score score : match.getResult().getScore()) {
            if (score.getType().equalsIgnoreCase("OT")) {
                scoreResult = score.getValue();
                break;
            } else if (score.getType().equalsIgnoreCase("FT")) {
                scoreResult = score.getValue();
            }
        }

        String[] result = scoreResult.split(":");
        Map<String, Integer> scoreMap = new HashMap<>();
        scoreMap.put(team1Id, Integer.parseInt(result[0]));
        scoreMap.put(team2Id, Integer.parseInt(result[1]));
        return scoreMap;
    }






}
