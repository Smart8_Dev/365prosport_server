package com.smart8.sportradar.converters.stats;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.converters.SoccerConverter;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.firebase.LiveData;
import com.smart8.sportradar.handlers.CancelledMatchHandler;
import com.smart8.sportradar.statisticsmodels.*;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class SoccerStatMatchConverter extends StatMatchConverter {

    @Autowired
    private CancelledMatchHandler cancelledMatchHandler;

    @Override
    protected FbMatch createMatch() {
        return new FbSoccerMatch();
    }

    @Override
    protected LiveData createLiveData(Match match) {

        final FbSoccerMatch.SoccerLiveData.SoccerLiveDataBuilder builder = FbSoccerMatch.SoccerLiveData.builder();
        builder.minutes(Constants.MATCH_NORMAL_TIME);
        builder.score(super.createScore(match));

        Map<String, Long> contestants = createContestantMap(match.getTeams().getTeam());

        if (match.getGoals() != null && match.getGoals().getGoal().size() > 0) {
            final List<FbSoccerMatch.Goal> goals = createGoals(match.getGoals(), contestants);
            builder.goals(goals);
        }

        if (match.getCards() != null && match.getCards().getCard().size() > 0) {
            final List<FbSoccerMatch.Card> cards = createCards(match.getCards(), contestants);
            builder.cards(cards);
        }

        if (match.getSubstitutions() != null && match.getSubstitutions().getSubstitution().size() > 0) {
            final List<FbSoccerMatch.Substitute> subs = createSubs(match.getSubstitutions(), contestants);
            builder.subs(subs);
        }

        final List<FbSoccerMatch.Lineup> lineups = createLineups(match.getTeams().getTeam());
        builder.lineup(lineups);

        final List<FbSoccerMatch.Statistic> statistics = createStatistics(match.getTeams().getTeam());
        builder.stats(statistics);

        return builder.build();
    }

    @Override
    void sendMatchCancelledMessage(String matchId) {
        cancelledMatchHandler.sendMatchCancelledMessage(SportType.Soccer, matchId);
    }

    private Map<String, Long> createContestantMap(List<Team> teams) {
        Map<String, Long> contestants = new HashMap<>();
        for (Team team : teams) {
            contestants.put(team.getType().toString(), team.getId().longValue());
        }
        return contestants;
    }

    private List<FbSoccerMatch.Goal> createGoals(Match.Goals goals, Map<String, Long> contestatns) {
        List<FbSoccerMatch.Goal> goalList = new ArrayList<>();
        try {
            for (LiveScoreEventTopGoal goal : goals.getGoal()) {
                FbSoccerMatch.Goal.GoalBuilder goalBuilder = FbSoccerMatch.Goal.builder();
                goalBuilder.contestantId(contestatns.get(goal.getTeam()))
                    .periodId(SoccerConverter.getPeriodIdByTime(goal.getTime()))
                    .shootout(goal.isShootout());

                goalBuilder.timeMin(SoccerConverter.getTimeRelevantByPeriod(goal.getTime()) + getInjuryTime(goal.getInjuryTime()));

                if (goal.getPlayer() != null) {
                    goalBuilder.scorerId(goal.getPlayer().getPlayerID().longValue())
                        .scorer(goal.getPlayer().getPlayerName());
                }

                if (goal.getFlag() != null) {
                    goalBuilder.type(SoccerConverter.getGoalTypeCode(goal.getFlag().getType()));
                }

                if (goal.getAssists() != null) {
                    Assist assist = goal.getAssists().getAssist().get(0);
                    goalBuilder.assist(assist.getPlayer().getPlayerName())
                        .assistId(assist.getPlayer().getPlayerID().longValue());

                }
                goalList.add(goalBuilder.build());
            }
        }catch (Exception ex){
            log.error("Weird goal info {}", goals);
        }
        return goalList;
    }

    private List<FbSoccerMatch.Card> createCards(Match.Cards cards, Map<String, Long> contestatns) {
        List<FbSoccerMatch.Card> cardList = new ArrayList<>();

        for (LiveScoreEventTopCard card : cards.getCard()) {
            FbSoccerMatch.Card.CardBuilder cardBuilder = FbSoccerMatch.Card.builder();
            cardBuilder.contestantId(contestatns.get(card.getTeam()))
                .periodId(SoccerConverter.getPeriodIdByTime(card.getTime()))
                .timeMin(SoccerConverter.getTimeRelevantByPeriod(card.getTime()) + getInjuryTime(card.getInjuryTime()));

            if (card.getPlayer() != null) {
                cardBuilder.playerId(card.getPlayer().getPlayerID().longValue())
                    .player(card.getPlayer().getPlayerName());
            }

            cardBuilder.type(SoccerConverter.getCardCode(card.getColor()));

            cardList.add(cardBuilder.build());
        }
        return cardList;

    }

    private List<FbSoccerMatch.Substitute> createSubs(Match.Substitutions subs, Map<String, Long> contestatns) {
        List<FbSoccerMatch.Substitute> subList = new ArrayList<>();

        for (LiveScoreEventTopSubstitution substitution : subs.getSubstitution()) {
            FbSoccerMatch.Substitute.SubstituteBuilder subBuilder = FbSoccerMatch.Substitute.builder();
            subBuilder.contestantId(contestatns.get(substitution.getTeam()))
                .periodId(SoccerConverter.getPeriodIdByTime(substitution.getTime()))
                .timeMin(SoccerConverter.getTimeRelevantByPeriod(substitution.getTime()) + getInjuryTime(substitution.getInjuryTime()));

            if (substitution.getIncomingPlayer() != null) {
                subBuilder.playerOn(substitution.getIncomingPlayer().getPlayer().get(0).getPlayerName())
                    .playerOnId(substitution.getIncomingPlayer().getPlayer().get(0).getPlayerID());
            }
            if (substitution.getOutgoingPlayer() != null) {
                subBuilder.playerOff(substitution.getOutgoingPlayer().getPlayer().get(0).getPlayerName())
                    .playerOffId(substitution.getOutgoingPlayer().getPlayer().get(0).getPlayerID());
            }

            subList.add(subBuilder.build());
        }
        return subList;

    }

    private List<FbSoccerMatch.Lineup> createLineups(List<Team> teams) {
        List<FbSoccerMatch.Lineup> lineupList = new ArrayList<>();
        for (Team team : teams) {
            FbSoccerMatch.Lineup.LineupBuilder lineupBuilder = FbSoccerMatch.Lineup.builder();
            lineupBuilder.contestantId(team.getId());
            if (team.getFormation() != null) {
                lineupBuilder.formation(team.getFormation().replaceAll("-", ""));
            }

            if (team.getLineups() != null) {
                List<FbSoccerMatch.LineupPlayer> playerList = new ArrayList<>();
                for (LiveScoreEventTopLineup topLineup : team.getLineups().getLineup()) {

                    boolean isSubstitute = topLineup.isSubstitutes();
                    for (Player player : topLineup.getPlayers().getPlayer()) {
                        playerList.add(createLineupPlayer(player, isSubstitute));
                    }
                }
                if (!playerList.isEmpty()) {
                    lineupBuilder.players(playerList);
                }
            }
            lineupList.add(lineupBuilder.build());
        }
        return lineupList.isEmpty() ? null : lineupList;
    }


    private FbSoccerMatch.LineupPlayer createLineupPlayer(Player radarPlayer, boolean isSubstitute) {
        final FbSoccerMatch.LineupPlayer.LineupPlayerBuilder playerBuilder = FbSoccerMatch.LineupPlayer.builder();
        final String[] nameParts = ConverterHelpers.splitPlayerFullName(radarPlayer.getPlayerName());

        playerBuilder.id(radarPlayer.getPlayerID().longValue())
            .firstName(nameParts[1])
            .lastName(nameParts[0]);

        if (isSubstitute) {
            playerBuilder.position(Constants.NO_VALUE);
        } else if (radarPlayer.getMatchPosistion() != null) {
            playerBuilder.position(Integer.parseInt(radarPlayer.getMatchPosistion()));
        }

        if (radarPlayer.getPlayerInfo() != null) {
            for (PlayerInfoEntry info : radarPlayer.getPlayerInfo().getPlayerInfoEntry()) {
                if (info.getName().equalsIgnoreCase("Shirt number")) {
                    playerBuilder.number(Integer.valueOf(info.getValue()));
                }
            }
        }

        return playerBuilder.build();
    }

    private List<FbSoccerMatch.Statistic> createStatistics(List<Team> teams) {
        final List<FbSoccerMatch.Statistic> statisticsList = new ArrayList<>();

        for (Team team : teams) {
            MatchStatistics stats = team.getMatchstatistics();
            if (stats == null) {
                continue;
            }
            FbSoccerMatch.Statistic.StatisticBuilder statisticBuilder = FbSoccerMatch.Statistic.builder();

            statisticBuilder.contestantId(team.getId())
                .fouls(stats.getFouls())
                .offsides(stats.getOffsides())
                .cornerKicks(stats.getCorners())
                .freeKicks(stats.getFreeKicks())
                .shots(stats.getShotsOffGoal())
                .shotsTarget(stats.getShotsOnGoal())
                .ballPossession(stats.getBallpossession());
            statisticsList.add(statisticBuilder.build());
        }
        return statisticsList.isEmpty() ? null : statisticsList;
    }

    private int getInjuryTime(Integer injuryTime) {
        return injuryTime == null ? 0 : injuryTime;
    }
}
