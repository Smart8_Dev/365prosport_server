package com.smart8.sportradar.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.enums.MatchEventCounter;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.firebase.*;
import com.smart8.sportradar.models.FcmMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.smart8.sportradar.converters.SoccerConverter.OWN_GOAL;
import static com.smart8.sportradar.converters.SoccerConverter.PENALTY_GOAL;

@Component
@Slf4j
public class NotificationCreator {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ObjectMapper mapper;

    public FcmMessage buildMatchNotification(MatchEventCounter type, FbMatch fbMatch, Locale locale) {

        List<Contestant> teamList = SoccerConverter.createHomeAwayList(fbMatch);

        FcmMessage.Notification notification = createNotification(type, fbMatch, teamList, locale);

        //Duplicate title and body to custom data for frontEnd
        FcmMessage.CustomData data = FcmMessage.CustomData.builder()
            .title(notification.getTitle())
            .body(notification.getTitle())
            .matchId(fbMatch.getId())
            .type(FcmMessage.MessageType.event)
            .eventType(type.name())
            .build();

        if (fbMatch instanceof FbSoccerMatch){
            String payload = createPayload(type, (FbSoccerMatch) fbMatch);
            data.setPayload(payload);
            data.setSportType(SportType.Soccer.name().toLowerCase());
        }else if ( fbMatch instanceof FbDartsMatch){
            data.setSportType(SportType.Darts.name().toLowerCase());
        }

        log.debug("Custom data: {}", data);

        return FcmMessage.builder()
            .data(data)
            .content_available(true)
            .notification(notification)
            .build();
    }

    public FcmMessage buildBetNotification(String userToken, FbBet bet, FbMatch fbMatch, Locale locale) {
        List<Contestant> teamList = SoccerConverter.createHomeAwayList(fbMatch);


        FcmMessage.Notification notification = new FcmMessage.Notification();
        notification.setTitle(teamList.get(0).getName() + getScore(fbMatch, teamList) +  teamList.get(1).getName());

        if ( bet.getStatus() == FbBet.Status.Win ){
            notification.setBody(messageSource.getMessage("bets.notify.won", new Object[]{(int)(bet.getStake()*bet.getOdds())}, locale));
        }else if (bet.getStatus() == FbBet.Status.Lose){
            notification.setBody(messageSource.getMessage("bets.notify.lost", null, locale));
        }

        String payload = null;
        try {
            payload = mapper.writeValueAsString(bet);
        } catch (JsonProcessingException e) {
            log.error("Error {} while parsing bet payload for match {}. Bet {}", e.getMessage(), bet);
        }

        FcmMessage.CustomData data = FcmMessage.CustomData.builder()
            .title(notification.getTitle())
            .body(notification.getTitle())
            .matchId(fbMatch.getId())
            .betId(bet.getId())
            .type(FcmMessage.MessageType.bet)
            .payload(payload)
            .build();
        log.debug("Custom notification: {}", data);

        return FcmMessage.builder()
            .registration_ids(Collections.singletonList(userToken))
            .data(data)
            .content_available(true)
            .notification(notification)
            .build();
    }

    public FcmMessage buildFriendRequestNotification(String userToken, FbFriendRequest request, Locale locale) {

        String payload = null;
        try {
            payload = mapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            log.error("Error {} while parsing friend request payload {}.", e.getMessage(), request);
        }

        FcmMessage.Notification notification = new FcmMessage.Notification();
        if ( request.getStatus() == FbFriendRequest.InviteStatus.REQUESTED ){
            notification.setTitle(messageSource.getMessage("users.notify.friend.request", null, locale));
            notification.setBody(request.getInviter());
        }else if (request.getStatus() == FbFriendRequest.InviteStatus.ACCEPTED){
            notification.setTitle(messageSource.getMessage("users.notify.friend.accepted", null, locale));
            notification.setBody(request.getInvited());
        }
        log.debug("Notification: {}", notification);

        //Duplicate title and body for frontEnd needs
        FcmMessage.CustomData data = FcmMessage.CustomData.builder()
            .title(notification.getTitle())
            .body(notification.getTitle())
            .type(FcmMessage.MessageType.friend)
            .payload(payload)
            .build();

        return FcmMessage.builder()
            .registration_ids(Collections.singletonList(userToken))
            .content_available(true)
            .data(data)
            .notification(notification)
            .build();
    }

    private FcmMessage.Notification createNotification(MatchEventCounter type, FbMatch fbMatch, List<Contestant> teamList, Locale locale) {

        FcmMessage.Notification notification = new FcmMessage.Notification();
        String title;
        int time;
        FbSoccerMatch fbSoccerMatch;
        FbDartsMatch fbDartsMatch;

        switch (type){
            case SoonStart :
                notification.setTitle(teamList.get(0).getName() + " : " + teamList.get(1).getName());
                notification.setBody(messageSource.getMessage("match.notify.soon_start",new Object[]{15}, locale));
                break;
            case Lineup:
                notification.setTitle(teamList.get(0).getName() + " : " + teamList.get(1).getName());
                notification.setBody(messageSource.getMessage("match.notify.lineup_available", null, locale));
                break;
            case StartFirst:
                notification.setTitle(teamList.get(0).getName() + " : " + teamList.get(1).getName());
                notification.setBody(messageSource.getMessage("match.notify.starts", null, locale));
                break;
            case StartSecond:
                notification.setTitle(teamList.get(0).getName() + " : " + teamList.get(1).getName());
                notification.setBody(messageSource.getMessage("match.notify.start.second", null, locale));
                break;
            case Goal:
                fbSoccerMatch = (FbSoccerMatch)fbMatch;
                FbSoccerMatch.Goal goal = fbSoccerMatch.getLiveData().getGoals().get(fbSoccerMatch.getLiveData().getGoals().size()-1);
                title = teamList.get(0).getName() + getScore(fbSoccerMatch, teamList) + teamList.get(1).getName();
                notification.setTitle(title);
                time = SoccerConverter.convertPeriodTimeToMatchTimeString(goal.getPeriodId(), goal.getTimeMin());
                switch (goal.getType()) {
                    case PENALTY_GOAL:
                        notification.setBody(messageSource.getMessage("match.notify.goal.penalty", new Object[]{time, goal.getScorer()}, locale));
                        break;
                    case OWN_GOAL:
                        notification.setBody(messageSource.getMessage("match.notify.goal.own", new Object[]{time, goal.getScorer()}, locale));
                        break;
                    default:
                        notification.setBody(messageSource.getMessage("match.notify.goal", new Object[]{time, goal.getScorer()}, locale));
                        break;
                }
                break;
            case Sets :
                fbDartsMatch = (FbDartsMatch)fbMatch;
                title = teamList.get(0).getName() + getSetScore(fbDartsMatch.getLiveData().getScore(), teamList) + teamList.get(1).getName();
                notification.setTitle(title);
                notification.setBody(messageSource.getMessage("match.notify.setwin", null, locale));
                break;
            case Legs:
                fbDartsMatch = (FbDartsMatch)fbMatch;
                Map<String, Integer> legScore = fbDartsMatch.getLiveData().getSets().get(fbDartsMatch.getLiveData().getSets().size()-1);
                title = teamList.get(0).getName() + getSetScore(legScore, teamList) + teamList.get(1).getName();
                notification.setTitle(title);
                notification.setBody(messageSource.getMessage("match.notify.legwin", null, locale));
                break;
            case Card:
                fbSoccerMatch = (FbSoccerMatch)fbMatch;
                FbSoccerMatch.Card card = fbSoccerMatch.getLiveData().getCards().get(fbSoccerMatch.getLiveData().getCards().size()-1);
                title = teamList.get(0).getName() + getScore(fbMatch, teamList) + teamList.get(1).getName();
                notification.setTitle(title);
                time = SoccerConverter.convertPeriodTimeToMatchTimeString(card.getPeriodId(), card.getTimeMin());
                notification.setBody(messageSource.getMessage("match.notify.card." +  card.getType(), new Object[]{time, card.getPlayer()}, locale));
                break;
            case Ht:
                notification.setTitle(teamList.get(0).getName() + " : " + teamList.get(1).getName());
                notification.setBody(messageSource.getMessage("match.notify.half_time", null, locale));
                break;
            case Finished:
                notification.setTitle(teamList.get(0).getName() + getScore(fbMatch, teamList) + teamList.get(1).getName());
                notification.setBody(messageSource.getMessage("match.notify.end", null, locale));
                break;
            case Started:
                notification.setTitle(teamList.get(0).getName() + " : " + teamList.get(1).getName());
                notification.setBody(messageSource.getMessage("match.notify.starts", null, locale));
                break;
            default:
                log.info("Type {} unprocessed", type);
                break;
        }

        notification.setIcon(type.name().toLowerCase());
        notification.setClick_action("FCM_PLUGIN_ACTIVITY");

        log.debug("Native notification: {}", notification);
        return notification;
    }

    /**
     * Returns score with spaces " 0:0 "
     */
    private String getScore(FbMatch fbMatch, List<Contestant> teamList){
        return " " +
            fbMatch.getScore().get(String.valueOf(teamList.get(0).getId())) +
            ":" +
            fbMatch.getScore().get(String.valueOf(teamList.get(1).getId())) +
            " ";
    }

    private String getSetScore(Map<String, Integer> setScore, List<Contestant> teamList){
        return " " +
            setScore.get(String.valueOf(teamList.get(0).getId())) +
            ":" +
            setScore.get(String.valueOf(teamList.get(1).getId())) +
            " ";

    }

    private String createPayload(MatchEventCounter type, FbSoccerMatch fbMatch) {
        try {
            switch( type ){
                case Goal:
                    FbSoccerMatch.Goal goal = fbMatch.getLiveData().getGoals().get(fbMatch.getLiveData().getGoals().size() - 1);
                    return mapper.writeValueAsString(goal);
                case Card:
                    FbSoccerMatch.Card card = fbMatch.getLiveData().getCards().get(fbMatch.getLiveData().getCards().size() - 1);
                    return mapper.writeValueAsString(card);
                case Substitute:
                    FbSoccerMatch.Substitute substitute = fbMatch.getLiveData().getSubs().get(fbMatch.getLiveData().getSubs().size() - 1);
                    return mapper.writeValueAsString(substitute);
                default:
                    return null;
            }
        } catch (JsonProcessingException e) {
            log.error("Error {} while parsing notification payload for match {}. Notify type {}", e.getMessage(), fbMatch.getId(), type);
            return null;
        }
    }



}
