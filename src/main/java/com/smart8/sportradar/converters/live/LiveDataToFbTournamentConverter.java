package com.smart8.sportradar.converters.live;

import com.smart8.sportradar.firebase.FbTournament;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class LiveDataToFbTournamentConverter implements Converter<BetradarLivescoreData.Sport.Category.Tournament, FbTournament> {

    @Nullable
    @Override
    public FbTournament convert(BetradarLivescoreData.Sport.Category.Tournament source) {

        return null;
    }
}
