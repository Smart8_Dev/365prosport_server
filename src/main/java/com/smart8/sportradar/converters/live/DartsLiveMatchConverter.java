package com.smart8.sportradar.converters.live;

import com.smart8.sportradar.enums.SoccerPeriodType;
import com.smart8.sportradar.firebase.FbDartsMatch;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.LiveData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class DartsLiveMatchConverter extends LiveMatchConverter {

    @Override
    protected FbMatch createMatch() {
        return new FbDartsMatch();
    }

    @Override
    protected LiveData createLiveData(Tournament.Match match) {
        final StatusCode statusCode = StatusCode.getByCode(match.getStatus().getCode().intValue());
        final FbDartsMatch.DartsLiveData.DartsLiveDataBuilder builder = FbDartsMatch.DartsLiveData.builder();

        if (statusCode.equals(StatusCode.Started) && match.getCurrentPeriodStart() != null) {
            long periodStart = ConverterHelpers.parseMatchDateTimeToLong(match.getCurrentPeriodStart());
            builder.minutes(ConverterHelpers.getMinutesDiff(periodStart, System.currentTimeMillis()));
        }
        if (match.getStatus().getCode().intValue() > 0) {
            builder.score(createScores(match));
            builder.sets(createDartsSets(match));
        }
        return builder.build();

    }

    /**@inheritDoc*/
    @Override
    protected String getTeamPositionByIndex(int index) {
        return String.valueOf(index + 1);
    }


    private Map<String, Integer> createScores(Tournament.Match match) {
        if (match.getScores() != null) {
            String team1Id = match.getTeam1().getUniqueTeamId().toString();
            String team2Id = match.getTeam2().getUniqueTeamId().toString();

            for (Tournament.Match.Scores.Score betScore : match.getScores().getScore()) {
                if (betScore.getType().equalsIgnoreCase(SoccerPeriodType.current.toString())) {
                    return createScore(team1Id, team2Id, betScore);
                }
            }
        }
        return null;
    }

    private List<Map<String, Integer>> createDartsSets(Tournament.Match match) {
        if (match.getScores() == null) {
            return null;
        }
        List<Map<String, Integer>> legs = new ArrayList<>();
        String firstPlayerId = match.getTeam1().getUniqueTeamId().toString();
        String secondPlayerId = match.getTeam2().getUniqueTeamId().toString();

        for (Tournament.Match.Scores.Score betScore : match.getScores().getScore()) {
            if (betScore.getType().contains("Period")) {
                Map<String, Integer> legScore = new HashMap<>();
                legScore.put(firstPlayerId, betScore.getTeam1().getValue().intValue());
                legScore.put(secondPlayerId, betScore.getTeam2().getValue().intValue());
                legs.add(legScore);
            }
        }
        return legs;
    }



}
