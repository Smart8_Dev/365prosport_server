package com.smart8.sportradar.converters.live;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.converters.SoccerConverter;
import com.smart8.sportradar.enums.SoccerPeriodType;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.firebase.FbTeamOfficial;
import com.smart8.sportradar.firebase.LiveData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament;
import com.smart8.sportradar.livemodels.TAssist;
import com.smart8.sportradar.livemodels.TPlayer;
import com.smart8.sportradar.livemodels.TStatistics;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.smart8.sportradar.utils.ConverterHelpers.safeBigInteger;

@Slf4j
@Component
public class SoccerLiveMatchConverter extends LiveMatchConverter {

    @Override
    protected FbMatch createMatch() {
        return new FbSoccerMatch();
    }

    @Override
    protected LiveData createLiveData(Tournament.Match match) {
        final StatusCode byCode = StatusCode.getByCode(match.getStatus().getCode().intValue());

        final FbSoccerMatch.SoccerLiveData.SoccerLiveDataBuilder builder = FbSoccerMatch.SoccerLiveData.builder();

        String periodStart = null;
        if (match.getCurrentPeriodStart() != null) {
            periodStart = ConverterHelpers.parseDateOfMatchString(match.getCurrentPeriodStart());
        }

        builder.minutes(SoccerConverter.getMatchMinutes(byCode, periodStart, null, match));
        builder.lineup(createLineups(match));

        if (match.getStatus().getCode().intValue() > 0) {
            createScores(match, builder);
            builder.goals(createGoals(match))
                .cards(createCards(match))
                .subs(createSubstitutions(match))
                .stats(createStats(match));
        }

        if (match.getPenaltyShootout() != null) {
            builder.shootouts(createPenaltyShootOuts(match));
        }

        return builder.build();
    }

    private List<FbSoccerMatch.Lineup> createLineups(Tournament.Match match) {

        FbSoccerMatch.Lineup.LineupBuilder homeBuilder = FbSoccerMatch.Lineup.builder()
            .contestantId(match.getTeam1().getUniqueTeamId().longValue())
            .formation(SoccerConverter.getFormation(match.getTeam1().getFormation()));

        FbSoccerMatch.Lineup.LineupBuilder awayBuilder = FbSoccerMatch.Lineup.builder()
            .contestantId(match.getTeam2().getUniqueTeamId().longValue())
            .formation(SoccerConverter.getFormation(match.getTeam2().getFormation()));

        final Tournament.Match.Managers managers = match.getManagers();
        if (managers != null) {
            homeBuilder.official(Collections.singletonList(new FbTeamOfficial(Constants.MANAGER, managers.getTeam1())));
            awayBuilder.official(Collections.singletonList(new FbTeamOfficial(Constants.MANAGER, managers.getTeam2())));
        }

        final Tournament.Match.Lineups lineups = match.getLineups();
        if (lineups != null) {
            createLineupPlayers(match.getLineups(), homeBuilder, awayBuilder);
        }

        return Arrays.asList(homeBuilder.build(), awayBuilder.build());
    }

    /**
     * Create players in the provided match
     */
    private void createLineupPlayers(Tournament.Match.Lineups lineups, FbSoccerMatch.Lineup.LineupBuilder homeLineupBuilder, FbSoccerMatch.Lineup.LineupBuilder awayLineupBuilder) {
        List<Object> teamPlayerOrTeamOfficial = lineups.getTeamPlayerOrTeamOfficial();

        List<FbSoccerMatch.LineupPlayer> homePlayers = new ArrayList<>();
        List<FbSoccerMatch.LineupPlayer> awayPlayers = new ArrayList<>();

        for (Object obj : teamPlayerOrTeamOfficial) {

            if (obj instanceof Tournament.Match.Lineups.TeamPlayer) {
                Tournament.Match.Lineups.TeamPlayer player = (Tournament.Match.Lineups.TeamPlayer) obj;
                FbSoccerMatch.LineupPlayer lineupPlayer = createLineupPlayer(player);

                if (player.getPlayerTeam().intValue() == 1) {
                    homePlayers.add(lineupPlayer);
                } else {
                    awayPlayers.add(lineupPlayer);
                }
            } else {
                log.warn("Missed linuep member of class {} ", obj.getClass().getName());
            }
        }

        homeLineupBuilder.players(homePlayers);
        awayLineupBuilder.players(awayPlayers);
    }

    private FbSoccerMatch.LineupPlayer createLineupPlayer(Tournament.Match.Lineups.TeamPlayer radarPlayer) {

        final FbSoccerMatch.LineupPlayer.LineupPlayerBuilder playerBuilder = FbSoccerMatch.LineupPlayer.builder();
        final String fullName = parsePlayerName(radarPlayer.getPlayer());
        final String[] nameParts = ConverterHelpers.splitPlayerFullName(fullName);

        playerBuilder.id(radarPlayer.getPlayer().getId().longValue())
            .firstName(nameParts[1])
            .lastName(nameParts[0])
            .number(safeBigInteger(radarPlayer.getShirtNumber()));
        String position = radarPlayer.getPos();

        if (position == null) {
            playerBuilder.position(Constants.NO_VALUE);
        } else {
            playerBuilder.position(Integer.parseInt(position));
        }


        return playerBuilder.build();
    }

    private String parsePlayerName(TPlayer player) {
        if (player.getName() != null) {
            return player.getName();
        } else {
            for (Object p : player.getContent()) {
                if (p != null) {
                    return p.toString();
                }
            }
            log.warn("Player name not FOUND. Player id: {}", player.getId());
            return null;
        }
    }

    private List<FbSoccerMatch.Goal> createGoals(Tournament.Match match) {

        final Tournament.Match.Goals liveGoals = match.getGoals();
        if (liveGoals == null) {
            return null;
        }
        final List<Tournament.Match.Goals.Goal> goalList = liveGoals.getGoal();
        List<FbSoccerMatch.Goal> goals = new ArrayList<>(goalList.size());

        for (Tournament.Match.Goals.Goal goal : goalList) {

            final int scoringTeam = goal.getScoringTeam().intValue();
            long teamId = getTeamIdFromMatch(match, scoringTeam);
            final int goalTime = goal.getTime().intValue();

            final FbSoccerMatch.Goal.GoalBuilder goalBuilder = FbSoccerMatch.Goal.builder()
                .contestantId(teamId)
                .periodId(SoccerConverter.getPeriodIdByTime(goalTime))
                .type(SoccerConverter.getGoalTypeCode(goal.getFrom()))
                .timeMin(SoccerConverter.getTimeRelevantByPeriod(goalTime) + getAddedTime(goal.getAddedTime()));

            bindScorer(goal, goalBuilder);
            bindAssist(goal, goalBuilder);
            goals.add(goalBuilder.build());
        }

        return goals;
    }

    private void bindScorer(Tournament.Match.Goals.Goal goal, FbSoccerMatch.Goal.GoalBuilder builder) {
        final TPlayer goalScorer = goal.getPlayer();
        if (goalScorer != null) {
            builder.scorerId(goalScorer.getId().longValue())
                .scorer(parsePlayerName(goalScorer));
        }
    }

    private void bindAssist(Tournament.Match.Goals.Goal goal, FbSoccerMatch.Goal.GoalBuilder builder) {
        final TAssist goalAssist = goal.getAssist1();
        if (goalAssist != null) {
            builder.assist(goalAssist.getName())
                .assistId(Long.parseLong(goalAssist.getId()));
        } else if (goal.getAssist2() != null) {
            builder.assist(goal.getAssist2().getId())
                .assistId(Long.parseLong(goal.getAssist2().getId()));
        }
    }

    private List<FbSoccerMatch.Card> createCards(Tournament.Match source) {
        final Tournament.Match.Cards objectCards = source.getCards();
        if (objectCards == null) {
            return null;
        }

        final List<Tournament.Match.Cards.Card> list = objectCards.getCard();
        List<FbSoccerMatch.Card> toReturn = new ArrayList<>(list.size());

        for (Tournament.Match.Cards.Card card : list) {

            final TPlayer player = card.getPlayer();
            final FbSoccerMatch.Card cardObj = FbSoccerMatch.Card.builder()
                .contestantId(getTeamIdFromMatch(source, card.getPlayerTeam().longValue()))
                .periodId(SoccerConverter.getPeriodIdByTime(card.getTime().intValue()))
                .playerId(player.getId().longValue())
                .player(parsePlayerName(player))
                .timeMin(SoccerConverter.getTimeRelevantByPeriod(card.getTime().intValue()) + getAddedTime(card.getAddedTime()))
                .type(SoccerConverter.getCardCode(card.getType()))
                .build();

            toReturn.add(cardObj);
        }

        return toReturn;
    }

    private List<FbSoccerMatch.Substitute> createSubstitutions(Tournament.Match source) {
        final Tournament.Match.Substitutions substitutionsObj = source.getSubstitutions();

        if (substitutionsObj == null) {
            return null;
        }

        final List<Tournament.Match.Substitutions.Substitution> list = substitutionsObj.getSubstitution();
        List<FbSoccerMatch.Substitute> subList = new ArrayList<>();
        for (Tournament.Match.Substitutions.Substitution sub : list) {

            final FbSoccerMatch.Substitute substitute = FbSoccerMatch.Substitute.builder()
                .contestantId(getTeamIdFromMatch(source, sub.getPlayerTeam().longValue()))
                .periodId(SoccerConverter.getPeriodIdByTime(sub.getTime().intValue()))
                .playerOnId(sub.getPlayerIn().getId().longValue())
                .playerOn(parsePlayerName(sub.getPlayerIn()))
                .playerOffId(sub.getPlayerOut().getId().longValue())
                .playerOff(parsePlayerName(sub.getPlayerOut()))
                .timeMin(SoccerConverter.getTimeRelevantByPeriod(sub.getTime().intValue()) + getAddedTime(sub.getAddedTime()))
                .build();

            subList.add(substitute);
        }
        return subList;
    }


    private List<FbSoccerMatch.Statistic> createStats(Tournament.Match match) {

        final Tournament.Match.Statistics statObject = match.getStatistics();
        if (statObject == null) {
            return null;
        }
        final FbSoccerMatch.Statistic.StatisticBuilder statBuilder1 = FbSoccerMatch.Statistic.builder();
        final FbSoccerMatch.Statistic.StatisticBuilder statBuilder2 = FbSoccerMatch.Statistic.builder();

        final List<JAXBElement<?>> statList = statObject.getBallPossessionOrShotsOnGoalOrShotsOffGoal();

        statBuilder1.contestantId(match.getTeam1().getUniqueTeamId().longValue());
        statBuilder2.contestantId(match.getTeam2().getUniqueTeamId().longValue());

        for (JAXBElement<?> entity : statList) {
            if (entity.getDeclaredType() == TStatistics.class) {

                TStatistics tStatistics = (TStatistics) entity.getValue();
                String statType = entity.getName().getLocalPart().toLowerCase();
                switch (statType) {
                    case "fouls":
                        statBuilder1.fouls(Integer.parseInt(tStatistics.getTeam1().getValue()));
                        statBuilder2.fouls(Integer.parseInt(tStatistics.getTeam2().getValue()));
                        break;
                    case "offsides":
                        statBuilder1.offsides(Integer.parseInt(tStatistics.getTeam1().getValue()));
                        statBuilder2.offsides(Integer.parseInt(tStatistics.getTeam2().getValue()));
                        break;
                    case "cornerkicks":
                        statBuilder1.cornerKicks(Integer.parseInt(tStatistics.getTeam1().getValue()));
                        statBuilder2.cornerKicks(Integer.parseInt(tStatistics.getTeam2().getValue()));
                        break;
                    case "freekicks":
                        statBuilder1.freeKicks(Integer.parseInt(tStatistics.getTeam1().getValue()));
                        statBuilder2.freeKicks(Integer.parseInt(tStatistics.getTeam2().getValue()));
                        break;
                    case "shotsoffgoal":
                        statBuilder1.shots(Integer.parseInt(tStatistics.getTeam1().getValue()));
                        statBuilder2.shots(Integer.parseInt(tStatistics.getTeam2().getValue()));
                        break;
                    case "shotsongoal":
                        statBuilder1.shotsTarget(Integer.parseInt(tStatistics.getTeam1().getValue()));
                        statBuilder2.shotsTarget(Integer.parseInt(tStatistics.getTeam2().getValue()));
                        break;
                    case "ballpossession":
                        statBuilder1.ballPossession(Integer.parseInt(tStatistics.getTeam1().getValue()));
                        statBuilder2.ballPossession(Integer.parseInt(tStatistics.getTeam2().getValue()));
                        break;
                    default:
                        break;
                }
            }
        }

        return Arrays.asList(statBuilder1.build(), statBuilder2.build());
    }


    private List<FbSoccerMatch.ShootOut> createPenaltyShootOuts(Tournament.Match match) {
        final Tournament.Match.PenaltyShootout shootoutList = match.getPenaltyShootout();

        final List<Tournament.Match.PenaltyShootout.Shot> shotList = shootoutList.getShot();
        List<FbSoccerMatch.ShootOut> shots = new ArrayList<>(shotList.size());

        for (Tournament.Match.PenaltyShootout.Shot shot : shotList) {
            final int scoringTeam = shot.getTeam().intValue();
            long teamId = getTeamIdFromMatch(match, scoringTeam);
            final FbSoccerMatch.ShootOut.ShootOutBuilder shootBuilder = FbSoccerMatch.ShootOut.builder()
                .contestantId(teamId)
                .sequense(shot.getSequence().intValue())
                .score(shot.getScore().intValue() == 1);
            bindShotPlayer(shot, shootBuilder);
            shots.add(shootBuilder.build());
        }
        return shots;
    }

    private void bindShotPlayer(Tournament.Match.PenaltyShootout.Shot shot, FbSoccerMatch.ShootOut.ShootOutBuilder builder) {
        final TPlayer goalScorer = shot.getPlayer();
        if (goalScorer != null) {
            builder.playerId(goalScorer.getId().longValue())
                .player(parsePlayerName(goalScorer));
        }
    }

    /**
     * Create Score and aggregated Score
     */
    private void createScores(Tournament.Match match, FbSoccerMatch.SoccerLiveData.SoccerLiveDataBuilder scoreBuilder) {
        if (match.getScores() == null) {
            return;
        }
        String team1Id = match.getTeam1().getUniqueTeamId().toString();
        String team2Id = match.getTeam2().getUniqueTeamId().toString();

        for (Tournament.Match.Scores.Score betScore : match.getScores().getScore()) {
            if (betScore.getType().equalsIgnoreCase(SoccerPeriodType.current.toString())) {
                scoreBuilder.score(createScore(team1Id, team2Id, betScore));
            } else if (betScore.getType().equalsIgnoreCase(SoccerPeriodType.aggregated.toString())) {
                scoreBuilder.aggScore(createScore(team1Id, team2Id, betScore));
            }
        }
    }

}
