package com.smart8.sportradar.converters.live;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.enums.RefereeType;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.fb.Referee;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbTeam;
import com.smart8.sportradar.firebase.LiveData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament;
import com.smart8.sportradar.livemodels.Name;
import com.smart8.sportradar.livemodels.TTeamInMatch;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.util.*;

import static com.smart8.sportradar.utils.ConverterHelpers.parseDateOfMatchString;
import static com.smart8.sportradar.utils.ConverterHelpers.parseMatchDateTimeToLong;


/**
 * Convert Betradarlivescore feed match to Firebase Match
 */
@Slf4j
public abstract class LiveMatchConverter {

    @Autowired
    private FirebaseService fbService;

    abstract FbMatch createMatch();

    abstract LiveData createLiveData(Tournament.Match match);

    public FbMatch convert(Tournament tournament, Tournament.Match match) {
        log.info("Converting match with id: {} to FbMatch. {}", match.getId(), this.getClass().getName());

        checkMissingTeam(match);

        final FbMatch fbMatch = createMatch();
        fbMatch.setId(match.getId().toString());
        fbMatch.setContestants(createContestants(Arrays.asList(match.getTeam1(), match.getTeam2())));
        fbMatch.setDateTime(parseDateOfMatchString(match.getMatchDate()));
        fbMatch.setDateTimeInt(parseMatchDateTimeToLong(match.getMatchDate()));
        fbMatch.setTournamentId(tournament.getUniqueTournamentId().longValue());
        fbMatch.setTournamentName(BetradarLivescoreData.getFromName(tournament.getName()));
        fbMatch.setStatusCode(match.getStatus().getCode().intValue());
        fbMatch.setStatus(StatusCode.getByCode(match.getStatus().getCode().intValue()).name());
        fbMatch.setCanceled(match.getStatus().getCode().intValue() == StatusCode.Cancelled.statusCode);
        fbMatch.setPostponed(match.getStatus().getCode().intValue() == StatusCode.Postponed.statusCode);

        if (match.getVenue() != null) {
            fbMatch.setVenueId(match.getVenue().getStadium().getId().longValue());
            fbMatch.setVenueName(match.getVenue().getStadium().getName().get(0).getValue());
        }

        if (match.getReferee() != null) {
            fbMatch.setReferee(createRefereeList(match));
        }

        //Put endTime only if match is ended
        if (StatusCode.isEnded(StatusCode.getByCode(match.getStatus().getCode().intValue()))) {
            long time = parseMatchDateTimeToLong(match.getCurrentPeriodStart());
            fbMatch.setGameEndedInt(time);
        }
        fbMatch.setLiveData(createLiveData(match));

        return fbMatch;
    }


    private List<Referee> createRefereeList(Tournament.Match match) {
        if (!StringUtils.isEmpty(match.getReferee().getName2())) {
            Referee referee = new Referee(match.getReferee().getName2(), RefereeType.MAIN);
            return  Collections.singletonList(referee);
        }
        for (Name refereeName : match.getReferee().getName()) {
            if (!refereeName.getValue().isEmpty()) {
                return Collections.singletonList(new Referee(refereeName.getValue(), RefereeType.MAIN));
            } else if (refereeName.getValue2().isEmpty()) {
                return Collections.singletonList(new Referee(refereeName.getValue2(), RefereeType.MAIN));
            } else {
                log.warn("Referee name for match {} not exists", match.getId());
            }
        }
        return Collections.emptyList();
    }

    /**
     * Check if team from feed is missing in our DB
     */
    private void checkMissingTeam(Tournament.Match match) {
        final TTeamInMatch team1 = match.getTeam1();
        final TTeamInMatch team2 = match.getTeam2();

        FbTeam teamHome = fbService.getTeam(String.valueOf(match.getTeam1().getUniqueTeamId()));
        FbTeam teamAway = fbService.getTeam(String.valueOf(match.getTeam2().getUniqueTeamId()));
        if (teamHome == null) {
            log.warn("@convert > team {} is missing!", team1.getUniqueTeamId());
            teamHome = createFbTeam(team1);
            fbService.putTeam(teamHome);
        }
        if (teamAway == null) {
            log.warn("@convert > team {} is missing!", team2.getUniqueTeamId());
            teamAway = createFbTeam(team2);
            fbService.putTeam(teamAway);
        }
    }

    private List<Contestant> createContestants(List<TTeamInMatch> teams) {
        List<Contestant> contestantList = new ArrayList<>();
        int index = 0;
        for (TTeamInMatch team : teams) {
            Contestant.ContestantBuilder contestantBuilder = Contestant.builder();
            String teamName = parseTeamName(team);
            contestantBuilder.code(ConverterHelpers.createTeamCode(teamName))
                .id(team.getUniqueTeamId().longValue())
                .name(teamName)
                .position(getTeamPositionByIndex(index));
            contestantList.add(contestantBuilder.build());
            index++;
        }
        return contestantList;
    }

    /**
     * Get team match positions by its index in feed that starts from 0
     * @return home/away string when its possible otherwise override method with number position
     */
    protected String getTeamPositionByIndex(int index) {
        return index == 0 ? Constants.HOME : Constants.AWAY;
    }


    Map<String, Integer> createScore(String team1Id, String team2Id, Tournament.Match.Scores.Score betRadarScore) {
        Map<String, Integer> score = new HashMap<>();
        score.put(team1Id, betRadarScore.getTeam1().getValue().intValue());
        score.put(team2Id, betRadarScore.getTeam2().getValue().intValue());
        return score;
    }

    private String parseTeamName(TTeamInMatch team) {
        if (team.getName() != null) {
            return team.getName();
        } else {
            for (Object obj : team.getContent()) {
                if (obj instanceof Name) {
                    Name name = (Name) obj;
                    if (!StringUtils.isEmpty(name.getValue())) {
                        return name.getValue();
                    } else if (!StringUtils.isEmpty(name.getValue2())) {
                        return name.getValue2();
                    }
                }
            }
            log.warn("Team name not FOUND. Team id: {}", team.getId());
            return null;
        }
    }


    /**
     * Get betradar team id from live feed  by its position in match feed (team1 or team2)
     */
    long getTeamIdFromMatch(Tournament.Match source, long teamId) {
        return teamId == 1 ? source.getTeam1().getUniqueTeamId().longValue() : source.getTeam2().getUniqueTeamId().longValue();
    }

    private FbTeam createFbTeam(TTeamInMatch betTeam) {
        String teamName = parseTeamName(betTeam);
        FbTeam fbTeam = FbTeam.builder()
            .id(betTeam.getUniqueTeamId().toString())
            .code(ConverterHelpers.createTeamCode(teamName))
            .name(teamName)
            .build();
        log.info("@createFbTeam > created new team: " + fbTeam);
        return fbTeam;
    }

    int getAddedTime(BigInteger addedTime) {
        return addedTime == null ? 0 : addedTime.intValue();
    }

}
