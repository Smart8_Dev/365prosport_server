package com.smart8.sportradar.converters;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.enums.SoccerPeriodType;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament.Match;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.utils.ConverterHelpers;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;


@Slf4j
public class SoccerConverter {

    public static final String Y_CARD = "YC";
    public static final String R_CARD = "RC";
    public static final String Y2R_CARD = "Y2R";

    private static final String GOAL = "G";
    public static final String PENALTY_GOAL = "PG";
    public  static final String OWN_GOAL = "OG";
    private static final String OWN_GOAL_FULL = "owngoal";
    private static final String PENALTY_FULL = "penalty";
    private static final String PLAYER_POS_GK = "G";
    private static final String PLAYER_POS_DEF = "D";
    private static final String PLAYER_POS_MID = "M";
    private static final String PLAYER_POS_FOR = "F";



    public static int getTimeRelevantByPeriod(int time) {
        if (time <= Constants.MATCH_HALF_MINUTES) {
            return time;
        } else if (time <= Constants.MATCH_NORMAL_TIME) {
            return time - Constants.MATCH_HALF_MINUTES;
        } else if (time <= Constants.MATCH_TIME_AFTER_FIRST_EXTRA) {
            return time - Constants.MATCH_NORMAL_TIME;
        }
        return time - Constants.MATCH_TIME_AFTER_FIRST_EXTRA;
    }

    public static int getPeriodIdByTime(int time) {
        if (time <= Constants.MATCH_HALF_MINUTES) {
            return 1;
        } else if (time <= Constants.MATCH_NORMAL_TIME) {
            return 2;
        } else if (time <= Constants.MATCH_TIME_AFTER_FIRST_EXTRA) {
            return 3;
        }
        return 4;
    }

    public static int convertPeriodTimeToMatchTimeString(int period, int time) {
        if ( period == 2){
            return Constants.MATCH_HALF_MINUTES + time;
        }else if ( period == 3 ){
            return Constants.MATCH_NORMAL_TIME + time;
        }else if ( period == 4 ){
            return Constants.MATCH_TIME_AFTER_FIRST_EXTRA + time;
        }else{
            return time;
        }

    }

    public static String getCardCode(String cardColor){
        String cardType;
        switch (cardColor) {
            case Constants.YELLOW:
                cardType = Y_CARD;
                break;
            case Constants.RED:
                cardType = R_CARD;
                break;
            default:
                cardType = Y2R_CARD;
                break;
        }
        return cardType;
    }

    public static String getGoalTypeCode(@Nullable  String type ){
        if (PENALTY_FULL.equalsIgnoreCase(type)) {
            return PENALTY_GOAL;
        } else if ( OWN_GOAL_FULL.equals(type)) {
            return OWN_GOAL;
        } else {
            return GOAL;
        }
    }

    public static String getFormation(String formation ) {
        if (formation != null) {
            return formation.replace("-", "");
        }
        return "442";
    }


    /**
     * Convert Betradar TeamPlayer position number to relevant string
     * @param posStr betradar TeamPlayer position
     * @return G, D, M, F or null
     */
    @Nullable
    public static String convertPlayerNumberPosition(String posStr){
        if (StringUtils.isEmpty(posStr) ){
            return null;
        }
        int pos = Integer.parseInt(posStr);
        if ( pos == 1 ){
            return PLAYER_POS_GK;
        }else if ( pos > 1 && pos < 6){
            return PLAYER_POS_DEF;
        }else if (pos >=6 && pos < 10 ){
            return PLAYER_POS_MID;
        }else if ( pos >= 10){
            return PLAYER_POS_FOR;
        }else{
            return null;
        }
    }



    /**
     * Calculate match current playing minute.
     * Can be a problem if getMinutes or other method will run before invoke. Need some testing
     */
    public static int getMatchMinutes(StatusCode statusCode, String periodStart, String matchEndedTime, Match match){
        MatchTimeCalc matchTimeCalc = new MatchTimeCalc(statusCode, periodStart, matchEndedTime, match);
        return matchTimeCalc.getMinutes();
    }

    /**
     * return 1  when home win, -1 away win, or 0
     */
    public static int whoWonPeriod(FbSoccerMatch match, int period){

        long homeId, awayId;
        if ( match.getContestants().get(0).getPosition().equalsIgnoreCase(Constants.HOME)){
            homeId = match.getContestants().get(0).getId();
            awayId = match.getContestants().get(1).getId();
        }else{
            homeId = match.getContestants().get(1).getId();
            awayId = match.getContestants().get(0).getId();
        }

        Integer countHome = 0;
        Integer countAway = 0;

        if ( period == 0 ){
            if (match.getLiveData().getScore() == null || match.getLiveData().getScore().size() == 0) {
                return 0;
            }
            countHome = match.getLiveData().getScore().get(String.valueOf(homeId));
            countAway = match.getLiveData().getScore().get(String.valueOf(awayId));
        }else{
            if (MyCollections.isEmpty(match.getLiveData().getGoals())) {
                return 0;
            }
            for( FbSoccerMatch.Goal goal : match.getLiveData().getGoals()){
                if ( goal.getPeriodId() == period ){
                    if (goal.getContestantId() == homeId ){
                        countHome++;
                    }else{
                        countAway++;
                    }
                }
            }
        }

        return countHome.compareTo(countAway);
    }


    public static List<Contestant> createHomeAwayList(FbMatch fbMatch){
        LinkedList<Contestant> teamList = new LinkedList<>();

        if ( fbMatch.getContestants().get(0).getPosition().equalsIgnoreCase(Constants.HOME)){
            teamList.add(fbMatch.getContestants().get(0));
            teamList.add(fbMatch.getContestants().get(1));
        }else if ( fbMatch.getContestants().get(0).getPosition().equalsIgnoreCase(Constants.AWAY)){
            teamList.add(fbMatch.getContestants().get(1));
            teamList.add(fbMatch.getContestants().get(0));
        }else {
            for( Contestant contestant : fbMatch.getContestants()){
                if ( contestant.getPosition().equals("1")){
                    teamList.addFirst(contestant);
                }else{
                    teamList.add(contestant);
                }
            }
        }
        return teamList;
    }



    private static class MatchTimeCalc {
        private StatusCode statusCode;
        private String periodStart;
        private String matchEndedTime;
        private BetradarLivescoreData.Sport.Category.Tournament.Match match;
        private int minutes;
        private int extra;

        private MatchTimeCalc(StatusCode statusCode, String periodStart, String matchEndedTime, Match match) {
            this.statusCode = statusCode;
            this.periodStart = periodStart;
            this.matchEndedTime = matchEndedTime;
            this.match = match;
        }

        private int getMinutes() {
            invoke();
            return minutes;
        }

        private String getTimeString() {
            if (extra != 0) {
                return minutes + " +" + extra;
            } else {
                return String.valueOf(minutes);
            }
        }

        private MatchTimeCalc invoke() {
            long matchEndedMilli = 0;
            long periodStartedMilli = 0;

            if (periodStart != null) {
                periodStartedMilli = ConverterHelpers.dateUtcToLong(periodStart);
            }

            if (matchEndedTime != null) {
                matchEndedMilli = ConverterHelpers.dateUtcToLong(matchEndedTime);
            }

            minutes = 0;
            extra = 0;

            final BetradarLivescoreData.Sport.Category.Tournament.Match.Scores scores = match.getScores();


            SoccerPeriodType lastKnownPeriodType = extractLastKnownPeriodThatEnded(scores);

            if (StatusCode.isEnded(statusCode)) {
                calcMatchEnded(matchEndedMilli, periodStartedMilli, lastKnownPeriodType);
            } else {
                switch (statusCode) {
                    case NotStarted:
                        minutes = 0;
                        break;
                    case Delayed:
                        minutes = 0;
                        break;
                    case Postponed:
                        minutes = 0;
                        break;
                    case Cancelled:
                        minutes = 0;
                        break;
                    case Interrupted:
                        calcMatchEnded(matchEndedMilli, periodStartedMilli, lastKnownPeriodType);
                        break;
                    case Suspended:
                        calcMatchEnded(matchEndedMilli, periodStartedMilli, lastKnownPeriodType);
                        break;
                    case Abandoned:
                        break;
                    case Started:
                    case FirstHalf:
                    case Period1:
                    case ExtraTimeHalftime:
                        firstHalfTime(periodStartedMilli);
                        break;
                    case Period2:
                    case SecondHalf:
                        secondHalfTime(periodStartedMilli);
                        break;
                    case Period3:
                        break;
                    case Period4:
                        break;
                    case Break:
                        minutes = Constants.MATCH_HALF_MINUTES;
                        break;
                    case Overtime:
                        break;
                    case Ended:
                        calcMatchEnded(matchEndedMilli, periodStartedMilli, lastKnownPeriodType);
                        break;
                    case Penalties1:
                    case Aet:
                    case Ap:
                        calcMatchEnded(matchEndedMilli, periodStartedMilli, lastKnownPeriodType);
                        break;
                    case Halftime:
                        minutes = Constants.MATCH_HALF_MINUTES;
                        break;
                    case FirstExtra:
                        firstExtraTime(periodStartedMilli);
                        break;
                    case SecondExtra:
                        secondExtraTime(periodStartedMilli);
                        break;
                    case AwaitExtraTime:
                        minutes = Constants.MATCH_NORMAL_TIME;
                        break;
                    case AwaitPenalties:
                        minutes = Constants.MATCH_EXTRA_TIME;
                        break;
                    case Penalties:
                        minutes = Constants.MATCH_EXTRA_TIME;
                        break;
                }
            }
            return this;
        }

        private void secondExtraTime(long periodStartedMilli) {
            if (periodStart == null) {
                minutes = Constants.MATCH_TIME_AFTER_FIRST_EXTRA;
            } else {
                final long currentTimeMiilli = getCurrentTimeMiilli();
                int diff = (int) ((currentTimeMiilli - periodStartedMilli) / Constants.MILLIS_IN_MINUTE);

                if (diff < Constants.EXTRA_TIME_HALF_MIN) {
                    minutes = Constants.MATCH_TIME_AFTER_FIRST_EXTRA + diff;
                } else if (diff < Constants.EXTRA_TIME_HALF_MIN + Constants.SPORTRADAR_MATCH_EXTRA_THRESHOLD) {
                    minutes = Constants.MATCH_EXTRA_TIME;
                    extra = diff - Constants.EXTRA_TIME_HALF_MIN;
                } else {
                    minutes = Constants.MATCH_EXTRA_TIME;
                    extra = Constants.SPORTRADAR_MATCH_EXTRA_THRESHOLD;
                }
            }
        }

        private void firstExtraTime(long periodStartedMilli) {
            if (periodStart == null) {
                minutes = Constants.MATCH_NORMAL_TIME;
            } else {
                final long currentTimeMiilli = getCurrentTimeMiilli();
                int diff = (int) ((currentTimeMiilli - periodStartedMilli) / Constants.MILLIS_IN_MINUTE);

                if (diff < Constants.EXTRA_TIME_HALF_MIN) {
                    minutes = Constants.MATCH_NORMAL_TIME + diff;
                } else if (diff < Constants.EXTRA_TIME_HALF_MIN + Constants.SPORTRADAR_MATCH_EXTRA_THRESHOLD) {
                    minutes = Constants.MATCH_TIME_AFTER_FIRST_EXTRA;
                    extra = diff - Constants.EXTRA_TIME_HALF_MIN;
                } else {
                    minutes = Constants.MATCH_TIME_AFTER_FIRST_EXTRA;
                    extra = Constants.SPORTRADAR_MATCH_EXTRA_THRESHOLD;
                }
            }
        }

        private void secondHalfTime(long periodStartedMilli) {
            if (periodStart == null) {
                minutes = Constants.MATCH_HALF_MINUTES;
            } else {
                final long currentTimeMiilli = getCurrentTimeMiilli();
                int diff = (int) ((currentTimeMiilli - periodStartedMilli) / Constants.MILLIS_IN_MINUTE);

                if (diff < 0) {
                    log.info("@invoke > diff between current and period start is smaller than 0! {}, {}", currentTimeMiilli, periodStartedMilli);
                    minutes = Constants.MATCH_HALF_MINUTES;
                } else if (diff < Constants.MATCH_HALF_MINUTES) {
                    minutes = diff + Constants.MATCH_HALF_MINUTES;
                    //Second half usually has more extra time added so its bigger than first half for 5 minutes
                } else if (diff < Constants.MATCH_HALF_MINUTES + Constants.SPORTRADAR_MATCH_MAX_EXTRA_THRESHOLD + 5) {
                    minutes = Constants.MATCH_NORMAL_TIME;
                    extra = diff - Constants.MATCH_HALF_MINUTES;
                } else {
                    log.error("@invoke > invalid diff: {}, between: {}, {}", diff, currentTimeMiilli, periodStartedMilli);
                    minutes = Constants.MATCH_NORMAL_TIME;
                    extra = Constants.SPORTRADAR_MATCH_MAX_EXTRA_THRESHOLD;
                }
            }
        }

        private void firstHalfTime(long periodStartedMilli) {
            if (periodStart == null) {
                minutes = 0;
            } else {
                final long currentTimeMiilli = getCurrentTimeMiilli();
                int diff = (int) ((currentTimeMiilli - periodStartedMilli) / Constants.MILLIS_IN_MINUTE);

                if (diff < 0) {
                    log.info("@invoke > diff between current and period start is smaller than 0! {}, {}", currentTimeMiilli, periodStartedMilli);
                    minutes = 0;
                } else if (diff < Constants.MATCH_HALF_MINUTES) {
                    minutes = diff;
                } else if (diff < Constants.MATCH_HALF_MINUTES + Constants.SPORTRADAR_MATCH_MAX_EXTRA_THRESHOLD) {
                    minutes = Constants.MATCH_HALF_MINUTES;
                    extra = diff - Constants.MATCH_HALF_MINUTES;
                } else {
                    log.error("@invoke > invalid diff: {}, between: {}, {}", diff, currentTimeMiilli, periodStartedMilli);
                    minutes = Constants.MATCH_HALF_MINUTES;
                    extra = Constants.SPORTRADAR_MATCH_EXTRA_THRESHOLD;
                }

            }
        }

        private long getCurrentTimeMiilli() {
            ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
            return utc.toEpochSecond() * 1000;
        }

        private void calcMatchEnded(long matchEndedMilli, long periodStartedMilli, SoccerPeriodType lastKnownPeriodType) {
            if (lastKnownPeriodType == SoccerPeriodType.normaltime) {
                if (matchEndedTime != null && periodStart != null) {
                    final int minutesDiff = calcMinutesDiff(matchEndedMilli, periodStartedMilli);

                    if (minutesDiff > Constants.MATCH_HALF_MINUTES) {
                        minutes = Constants.MATCH_NORMAL_TIME;
                        extra = minutesDiff - Constants.MATCH_HALF_MINUTES;
                        extra = extra > Constants.EXTRA_TIME_HALF_MIN ? Constants.EXTRA_TIME_HALF_MIN  : extra;
                    } else {
                        minutes = minutesDiff > 40 ? minutesDiff + Constants.MATCH_HALF_MINUTES : 88;
                    }
                } else {
                    minutes = Constants.MATCH_NORMAL_TIME;
                }
            } else if (lastKnownPeriodType == SoccerPeriodType.overtime || lastKnownPeriodType == SoccerPeriodType.penalties) {
                if (matchEndedTime != null && periodStart != null) {
                    final int minutesDiff = calcMinutesDiff(matchEndedMilli, periodStartedMilli);

                    if (minutesDiff > Constants.MATCH_EXTRA_MINUTES) {
                        minutes = Constants.MATCH_EXTRA_TIME;
                        extra = minutesDiff - Constants.MATCH_EXTRA_MINUTES;
                        extra = extra > Constants.SPORTRADAR_MATCH_EXTRA_THRESHOLD ? Constants.SPORTRADAR_MATCH_EXTRA_THRESHOLD : extra;
                    } else {
                        minutes = Constants.MATCH_EXTRA_TIME;
                    }
                } else {
                    minutes = Constants.MATCH_EXTRA_TIME;
                }
            }
        }

        private SoccerPeriodType extractLastKnownPeriodThatEnded(BetradarLivescoreData.Sport.Category.Tournament.Match.Scores scores) {
            SoccerPeriodType lastKnownPeriodType = null;
            if (scores != null) {
                final List<Match.Scores.Score> scoreList = scores.getScore();
                for (Match.Scores.Score score : scoreList) {
                    String type = score.getType();
                    if (type != null) {
                        type = type.toLowerCase();
                        final SoccerPeriodType periodType = SoccerPeriodType.valueOf(type);
                        if (periodType == SoccerPeriodType.current || periodType == SoccerPeriodType.aggregated ) {
                            continue;
                        }

                        if (lastKnownPeriodType == null) {
                            lastKnownPeriodType = periodType;
                        } else {
                            if (periodType.priority > lastKnownPeriodType.priority) {
                                lastKnownPeriodType = periodType;
                            }
                        }
                    }
                }
            }
            return lastKnownPeriodType;
        }

        private int calcMinutesDiff(long matchEndedMilli, long periodStartedMilli) {
            final long diff = matchEndedMilli - periodStartedMilli;
            return (int) (diff / Constants.MILLIS_IN_MINUTE);
        }
    }
}
