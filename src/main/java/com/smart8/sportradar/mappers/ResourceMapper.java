package com.smart8.sportradar.mappers;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@Slf4j
@Component
public class ResourceMapper {

    @Autowired
    ResourceLoader resourceLoader;

    @Autowired
    ObjectMapper mapper;

    public  <T> Map<String, T> readDataToMap(String filename, TypeReference<Map<String, T>> typeReference){
        log.debug("Loading data from {}", filename);
        Resource resource = resourceLoader.getResource("classpath:" + filename);
        if ( !resource.exists()){
            log.error("Could not find {} file", filename );
            return Collections.emptyMap();
        }

        Map<String, T> map;
        try {
            map = mapper.readValue(resource.getInputStream(), typeReference);
        } catch (IOException e) {
            log.error("Could not read file {}. {}", filename, e.getMessage());
            map = Collections.emptyMap();
        }
        return map;
    }

    public <T> T readDataMap(String filename, TypeReference<T> typeReference){
        log.debug("Loading data from {}", filename);
        Resource resource = resourceLoader.getResource("classpath:" + filename);

        if ( !resource.exists()){
            log.error("Could not find {} file", filename );
            return null;
        }

        try {
            return mapper.readValue(resource.getInputStream(), typeReference);
        } catch (IOException e) {
            log.error("@readDataToMap > could not read file", e);
            return null;
        }
    }
}
