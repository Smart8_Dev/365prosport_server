package com.smart8.sportradar.enums;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum ServiceType {
    prematch, livetable, leaguetable, matchdetails, schedulesandresult, tournament, postmatch, squad, team;

    public static ServiceType findByName(String serviceName){
        try {
            return ServiceType.valueOf(serviceName.toLowerCase());
        } catch (IllegalArgumentException e) {
            log.warn("@findByName > missing service: " + serviceName);
        }

        return null;
    }
}
