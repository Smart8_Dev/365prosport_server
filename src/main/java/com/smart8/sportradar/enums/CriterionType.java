package com.smart8.sportradar.enums;

import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
public enum CriterionType {

    FULL_TIME(1001159858),

    HALF_TIME(1000316018),

    SECOND_HALF(1001159826),

    /**Who wins first half. If draw bet return */
    DRAW_NO_BET_1H(1001159884),

    /**Who wins second half. If draw bet return */
    DRAW_NO_BET_2H(1001421321),

    /**Who wins game. If draw bet return */
    DRAW_NO_BET(1001159666),

    /**Who scores last goal. If no goals return */
    LAST_GOAL_NG_NB(1001582443),

    /**Who scores first goal. If no goals return */
    FIRST_GOAL_NG_NB(1000105110),

    MATCH_OUTCOME(1001159904);

    public long id;

    CriterionType(long id) {
        this.id = id;
    }

    public static Optional<CriterionType> findById(long id){
        for(CriterionType type : CriterionType.values()){
            if ( type.id == id){
                return Optional.of(type);
            }
        }
        log.info("Soccer CriterionId {} not allowed.", id);
        return Optional.empty();
    }
}
