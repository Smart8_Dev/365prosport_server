package com.smart8.sportradar.enums;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum SportType {
    Soccer(1), Darts(22), All(-1);

    private Integer sportId;

    SportType(int id){
        this.sportId = id;
    }

    public static SportType findById(int id){
        for ( SportType type : SportType.values()) {
            if(type.sportId == id) {
                return type;
            }
        }
        throw new UnsupportedOperationException("Invalid Sport with Id: " + id);
    }

    public static SportType findByName(String sport, SportType defaultType){
        for ( SportType type : SportType.values()) {
            if(type.name().equalsIgnoreCase(sport)) {
                return type;
            }
        }
        return defaultType;
    }

}
