package com.smart8.sportradar.enums;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public enum DartsPeriodType {
    period1(1), period2(2), current(99);

    public int priority;

    DartsPeriodType(int priority) {
        this.priority = priority;
    }

    public static DartsPeriodType getPeriodTypeFromString(String str){
        try {
            return DartsPeriodType.valueOf(str.toLowerCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
