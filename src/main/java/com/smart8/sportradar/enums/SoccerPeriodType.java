package com.smart8.sportradar.enums;

public enum SoccerPeriodType {
    period1(1), period2(2), overtime(5), extra1(3), extra2(4),penalties(6), normaltime(2), current(99), aggregated(-1);

    public int priority;

    SoccerPeriodType(int priority) {

        this.priority = priority;
    }

    public static SoccerPeriodType getPeriodTypeFromString(String str){
        try {
            return SoccerPeriodType.valueOf(str.toLowerCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
