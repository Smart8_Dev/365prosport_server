package com.smart8.sportradar.enums;

public enum RefereeType {
    MAIN, LINESMAN, FOURTH;
}
