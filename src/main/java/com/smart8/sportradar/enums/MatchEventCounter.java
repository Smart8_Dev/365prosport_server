package com.smart8.sportradar.enums;

public enum MatchEventCounter {
    Lineup(1),
    SoonStart(1),
    Started(1),//Started for darts
    StartFirst(1),
    StartSecond(1),
    Legs(99),
    Sets(99),
    Penalty(99),
    Ht(1),
    Finished(1),
    Goal(99),
    Card(99),
    Substitute(99);


    //Max number of notification of this type.
    public int limit;

    MatchEventCounter(int limit) {
        this.limit = limit;
    }
}