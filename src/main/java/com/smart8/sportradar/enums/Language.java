package com.smart8.sportradar.enums;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum Language {
    EN("en"), NL("nl");

    private String lang;

    Language(String lang){
        this.lang = lang;
    }

    public static Language findByString(String lang){
        for ( Language type : Language.values()) {
            if(type.lang.equalsIgnoreCase(lang)) {
                return type;
            }
        }
        return EN;
    }

}