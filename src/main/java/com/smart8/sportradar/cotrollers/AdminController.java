package com.smart8.sportradar.cotrollers;


import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.models.MiniMatch;
import com.smart8.sportradar.repositories.NotificationRepository;
import com.smart8.sportradar.services.FbListenerService;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.services.MappingService;
import com.smart8.sportradar.services.NotificationServiceFcm;
import com.smart8.sportradar.tasks.UnibetTask;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private MappingService mappingService;

    @Value("${app.version}")
    private String appVersion;

    @Autowired
    private FirebaseService firebaseService;

    @Autowired
    NotificationServiceFcm notificationService;

    @Autowired
    FbListenerService fbListenerService;

    @Autowired
    NotificationRepository repository;

    @Autowired
    UnibetTask unibetTask;

    @GetMapping
    public String getAdmin(Model model){
        log.debug("Loading admin page...");
        List<MiniMatch> map = mappingService.getUnmappedMatches();
        model.addAttribute("unmappedMatches", map);

        return "admin/index";
    }

    @GetMapping({"teams-mapping", "teams-mapping/{id}"})
    public String getTeamMapping(@Nullable @PathVariable("id") String teamId, @Nullable @RequestParam("team") String teamName,  Model model){
        if ( StringUtils.isNumeric(teamId)){
            model.addAttribute("radarTeamId", teamId);
            model.addAttribute("radarTeamName", teamName);
            log.debug("Mapping for team {}:'{}'", teamId, teamName);
        }else{
            log.debug("Creating new team  mapping");
        }
        return "admin/mapping/teams";
    }

    @GetMapping({"matches-mapping", "matches-mapping/{id}"})
    public String getMatchMapping(@Nullable @PathVariable("id") String matchId, Model model){

        if ( StringUtils.isNumeric(matchId)){
            FbSoccerMatch.FbMiniMatch match = mappingService.getUnmappedMatch(matchId);
            if ( match != null ){
                log.debug("Founded match for id :{}", match.getId());
                model.addAttribute("match", match);
                if ( mappingService.getMappedTeam(match.getContestants().get(0).getId()) < 0 ){
                    model.addAttribute("missingHomeTeam", match.getContestants().get(0).getId());
                    log.debug("No mapping for Home team: {}.", match.getContestants().get(0).getName());
                }

                if ( mappingService.getMappedTeam(match.getContestants().get(1).getId()) < 0 ){
                    model.addAttribute("missingAwayTeam", match.getContestants().get(1).getId());
                    log.debug("No mapping for Away team: {} .", match.getContestants().get(1).getId());
                }
            }
        }else{
            log.debug("Creating new match  mapping");
        }
        return "admin/mapping/matches";
    }

    @PostMapping("/teams-mapping")
    public String setTeamMapping(Model model, @RequestParam("rteamid") String radarTeamId, @RequestParam("uteamid") String unibetTeamId){
        log.debug("Saving team mapping. Sportradar team Id: {}, Unibet Team Id: {}", radarTeamId, unibetTeamId);
        if (StringUtils.isNumeric(radarTeamId) && StringUtils.isNumeric(unibetTeamId)){
            mappingService.storeTeamMapping( unibetTeamId, radarTeamId );
            model.addAttribute("message", "Saved");
            log.info("New team mapping saved.");
        }else{
            model.addAttribute("message", "Error on saving");
            log.error("Team mapping  NOT saved.");
        }
        return "admin/mapping/teams";
    }

    @PostMapping("/matches-mapping")
    public String setMatchMapping(Model model, @RequestParam("rmatchid") String radarMatchId, @RequestParam("umatchid") String unibetMatchId){
        log.debug("Saving match mapping. Sportradar match Id: {}, Unibet match Id: {}", radarMatchId, unibetMatchId);
        if (StringUtils.isNumeric(radarMatchId) && StringUtils.isNumeric(unibetMatchId)){
            mappingService.storeMatchMapping(unibetMatchId, radarMatchId);
            model.addAttribute("message", "Saved");
            log.info("New match mapping saved.");
        }else{
            model.addAttribute("message", "Error on saving");
            log.error("Match mapping  NOT saved.");
        }

        return "admin/mapping/matches";
    }


    @GetMapping("logger")
    public String getLogger(Model model){
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

        List<Logger> list = loggerContext.getLoggerList().stream()
            .filter(item -> item.getLevel() != null)
            .collect(Collectors.toList());

        model.addAttribute("loggerList", list);
        return "admin/settings/logger";
    }

    @PostMapping("logger")
    public String setLogger(@RequestParam("package") String pckg, @Nullable @RequestParam("level") String level){
        log.debug("Changing log level of '{}' to Level: {}", pckg, level);
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.getLogger(pckg).setLevel(Level.toLevel(level));

        return "redirect:/admin/logger";
    }



    //TODO AD: merge with invoke
    @GetMapping("update-bets")
    public String test(){
        unibetTask.runPreMatch();
        return "redirect:/admin/index";
    }

    @GetMapping("update-live")
    public String updateLive(){
        unibetTask.getLiveBets();

        return "redirect:/admin/index";
    }

    /** Only for testing*/
    @GetMapping("invoke")
    public String test(@RequestParam("update") String update, @Nullable @RequestParam("id") String id){
        switch (update){
            case "unibet":
                unibetTask.runPreMatch();
                break;
            case "user-bets":
                fbListenerService.init();
                repository.removeMatchBetCounter(id);
                break;
            case "update-coins":
                fbListenerService.updateUserCoins("AoaMQv7rCQOAC8Sc59zlhuHrlOs1", 100, true);
        }
        return "redirect:/admin/index";
    }


    @GetMapping("find-match")
    public String findMatch(Model model, @RequestParam("path") String path, @RequestParam("team") String team){
        Mono<String> mono =
            Mono.create(sink -> {
                firebaseService.findMatch(path, team, ( str) ->  sink.success(str));
            });

        model.addAttribute("match", mono.block());
        return "admin/findmatch";
    }

    @ModelAttribute("appVersion")
    public String getAppVersion() {
        return appVersion;
    }



}
