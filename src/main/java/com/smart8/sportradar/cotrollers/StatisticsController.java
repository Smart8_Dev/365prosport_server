package com.smart8.sportradar.cotrollers;

import com.smart8.sportradar.enums.ServiceType;
import com.smart8.sportradar.services.*;
import com.smart8.sportradar.statisticsmodels.SportradarData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.bind.annotation.*;
import org.springframework.xml.transform.StringSource;
import reactor.core.publisher.Mono;

import javax.xml.bind.JAXBElement;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

@RestController
@Slf4j
@RequestMapping("statistics")
public class StatisticsController {

    @Value("${app.path.sportStatData}")
    private String statDir = "/home/nodeuser/smart8Stats";

    @Qualifier("Statistics")
    @Autowired
    Jaxb2Marshaller marshaller;

    @Autowired
    PreMatchService preMatchService;

    @Autowired
    LeagueTableService leagueTableService;

    @Autowired
    SquadService squadService;

    @Autowired
    TeamService teamService;

    @Autowired
    MatchDetailService detailService;

    @Autowired
    TournamentService tournamentService;

    @PostMapping(consumes = MediaType.APPLICATION_XML_VALUE)
    public Mono<Boolean> getStatistics(@RequestBody String data) {
        return Mono.fromCallable(() -> {
            final JAXBElement<?> unmarshal = (JAXBElement<?>) marshaller.unmarshal(new StringSource(data));

            if (unmarshal.getValue() instanceof SportradarData) {
                SportradarData sportradarData = (SportradarData) unmarshal.getValue();
                writeToFile(data, sportradarData);
                parseInput(sportradarData);
            }
            return true;
        });
    }

    private void parseInput(SportradarData sportradarData) {

        final String serviceName = sportradarData.getServiceName();

        ServiceType type = ServiceType.findByName(serviceName.toLowerCase());
        if (type == null) {
            return;
        }

        log.info("Service Type: {}", type);
        switch (type) {
            case prematch:
            case schedulesandresult:
                preMatchService.handlePreMatchGame(sportradarData);
                break;
            case livetable:
            case leaguetable:
                leagueTableService.processTables(sportradarData);
                break;
            case squad:
                squadService.processSquad(sportradarData);
            case team:
                teamService.processTeams(sportradarData);
                break;
            case matchdetails:
                detailService.handleMatchDetails(sportradarData);
                break;
            case tournament:
                tournamentService.processTournaments(sportradarData);
                break;
            case postmatch:
            default:
                log.info("Service type: {} no found", type);
                break;
        }
    }

    //@GetMapping(value = "squad")
    public Mono<Boolean> readPreMatch() {
        return Mono.fromCallable(() -> {
            File file = new File(statDir + "/squad");
            final File[] files = file.listFiles();

            if (files == null) {
                return true;
            }
            for (File file1 : files) {
                final String string = IOUtils.toString(new FileInputStream(file1), Charset.forName("UTF-8"));
                final JAXBElement<?> unmarshal = (JAXBElement<?>) marshaller.unmarshal(new StringSource(string));

                if (unmarshal.getValue() instanceof SportradarData) {
                    SportradarData sportradarData = (SportradarData) unmarshal.getValue();
                    parseInput(sportradarData);
                }
            }
            return true;
        });
    }

    private void writeToFile(String data, SportradarData betradarLivescoreData) {
        File dir = new File(statDir, betradarLivescoreData.getServiceName());

        if (!dir.exists()) {
            dir.mkdirs();
        }

        final String fileName = betradarLivescoreData.getFileName() + ".xml";


        File fileToSave = new File(dir, fileName);

        log.info("@writeToFile > saving file: " + fileToSave);
        try {
            FileUtils.write(fileToSave, data, Charset.forName("UTF-8"));
        } catch (IOException e) {
            log.error("@writeToFile > error saving file " + fileToSave, e);
        }

    }
}
