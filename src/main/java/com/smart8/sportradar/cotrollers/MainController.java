package com.smart8.sportradar.cotrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {

    @GetMapping
    public String main(){
        return "redirect:admin";
    }

    @GetMapping("login")
    public String login(){
        return "admin/login";
    }
}
