package com.smart8.sportradar.api;


import com.smart8.sportradar.services.MappingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("admin/api")
@Slf4j
public class ApiController {

    @Autowired
    MappingService mappingService;

    @GetMapping("/unievents")
    public ResponseEntity getMissingUniEvents(@RequestParam("pattern") String pattern ) {

        Map<String, String> result = mappingService.findUnmappedEvents(pattern);
        log.debug("Sending response data: {}", result);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
