package com.smart8.sportradar.api;

import com.smart8.sportradar.enums.Language;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.models.news.News;
import com.smart8.sportradar.models.news.Socials;
import com.smart8.sportradar.repositories.UserRepository;
import com.smart8.sportradar.services.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("api")
@Slf4j
public class NewsApiController {

    @Autowired
    NewsService newsService;

    @Autowired
    UserRepository userRepository;

    private final String ACCESS_HEADER = "Access-Control-Allow-Origin";

    @GetMapping("news")
    public ResponseEntity<Map<String, News>> getNews(@RequestParam("userId") String userId,
                                                     @RequestParam("token") String token,
                                                     @Nullable @RequestParam("lang") String lang,
                                                     @Nullable @RequestParam("sport") String sport){

        log.debug( "Get news for user {} with token {}. Lang: {}, Sport: {}", userId, token, lang, sport);
        if ( !userRepository.getUserToken(userId).equals(token)){
            log.info( "User  token {} does not exists. Request Rejected", token);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).header(ACCESS_HEADER, "*").build();
        }
        SportType sportType = SportType.findByName(sport, SportType.All);

        Map<String, News> newsMap = newsService.getNews( Language.findByString(lang), sportType);
        return ResponseEntity.ok().header(ACCESS_HEADER, "*").body(newsMap);
    }

    @GetMapping("socials")
    public ResponseEntity<Map<String, Socials.NewsItem>>  getSocial(@RequestParam("userId") String userId,
                                @RequestParam("token") String token,
                                @Nullable @RequestParam("lang") String lang,
                                @Nullable @RequestParam("sport") String sport){
        log.debug( "Get juicer-social news  for user {} with token {}. Lang: {}, Sport: {}", userId, token, lang, sport);
        if (!userRepository.getUserToken(userId).equals(token)){
            log.info( "User token {} does not exists. Request Rejected", token);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).header(ACCESS_HEADER, "*").build();
        }
        SportType sportType = SportType.findByName(sport, SportType.All);
        Pair<Boolean, Map<String, Socials.NewsItem>> newsMap = newsService.getSocialNews(Language.findByString(lang), sportType);

        return ResponseEntity.ok().header(ACCESS_HEADER, "*").body(newsMap.getRight());
    }
}
