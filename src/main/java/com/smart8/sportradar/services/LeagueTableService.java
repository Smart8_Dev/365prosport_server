package com.smart8.sportradar.services;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.fb.FbStandings;
import com.smart8.sportradar.statisticsmodels.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class LeagueTableService {

    private FirebaseService fbService;

    @Autowired
    LeagueTableService(FirebaseService fbService){
        this.fbService = fbService;
    }

    public void processTables(SportradarData data){
        final Sport sport = data.getSport().get(0);

        handleSoccer(sport);
    }

    private void handleSoccer(Sport sport) {

        SportType sportType;
        try{
            sportType = SportType.findById(sport.getId());
        }catch (UnsupportedOperationException ex){
            log.error("Sport ID:{} not allowed", sport.getId());
            return;
        }

        final Category category = sport.getCategory().get(0);
        final Tournament tournament = category.getTournament().get(0);
        final LeagueTable leagueTable = tournament.getLeagueTable().get(0);

        final String subTournamentId = String.valueOf(leagueTable.getId());

        final FbStandings.FbStandingsBuilder builder = FbStandings.builder();

        if ( !leagueTable.getName().equalsIgnoreCase(tournament.getName())){
            builder.groupName(leagueTable.getName());
        }

        final List<LeagueTableRow> leagueTableRow = leagueTable.getLeagueTableRows().getLeagueTableRow();
        Map<String, FbStandings.FbStandingRow> standings = new HashMap<>();
        log.debug("Parsing {} table rows", leagueTableRow.size());

        for (LeagueTableRow tableRow : leagueTableRow) {
            final List<LeagueTableColumn> leagueTableColumn = tableRow.getLeagueTableColumn();

            final FbStandings.FbStandingRow.FbStandingRowBuilder rowBuilder = FbStandings.FbStandingRow.builder()
                .teamId(String.valueOf(tableRow.getTeam().getId()))
                .teamName(tableRow.getTeam().getName());

            int position = getTableValuesFromColumns(rowBuilder, leagueTableColumn);
            standings.put(String.valueOf(position-1), rowBuilder.build());
        }

        builder.standings(standings);

        log.debug("Saving {} standings", tournament.getName());
        fbService.saveStandings(tournament.getUniqueTournamentId().toString(), subTournamentId, builder.build(), sportType.name());
    }

    private int getTableValuesFromColumns(FbStandings.FbStandingRow.FbStandingRowBuilder rowBuilder, List<LeagueTableColumn> columns){
        int position = 0;
        for (LeagueTableColumn leagueTableColumn : columns) {
            String key = leagueTableColumn.getKey().toLowerCase();
            if ( key.equalsIgnoreCase(FbStandings.StandingColumn.pointsTotal.name())){
                rowBuilder.points( Integer.parseInt(leagueTableColumn.getValue()));
            }else if (key.equalsIgnoreCase(FbStandings.StandingColumn.goalsForTotal.name())){
                rowBuilder.goalsFor( Integer.parseInt(leagueTableColumn.getValue()));
            }else if (key.equalsIgnoreCase(FbStandings.StandingColumn.goalsAgainstTotal.name())){
                rowBuilder.goalsAgainst( Integer.parseInt(leagueTableColumn.getValue()));
            }else if (key.equalsIgnoreCase(FbStandings.StandingColumn.changeTotal.name())){
                rowBuilder.change( Integer.parseInt(leagueTableColumn.getValue()));
            }else if (key.equalsIgnoreCase(FbStandings.StandingColumn.matchesTotal.name())){
                rowBuilder.games( Integer.parseInt(leagueTableColumn.getValue()));
            }else if (key.equalsIgnoreCase(FbStandings.StandingColumn.winTotal.name())){
                rowBuilder.win( Integer.parseInt(leagueTableColumn.getValue()));
            }else if (key.equalsIgnoreCase(FbStandings.StandingColumn.drawTotal.name())){
                rowBuilder.draw( Integer.parseInt(leagueTableColumn.getValue()));
            }else if (key.equalsIgnoreCase(FbStandings.StandingColumn.lossTotal.name())){
                rowBuilder.lose( Integer.parseInt(leagueTableColumn.getValue()));
            }else if ( key.equalsIgnoreCase(FbStandings.StandingColumn.positionTotal.name())){
                position = Integer.parseInt(leagueTableColumn.getValue());
            }
        }
        return position;
    }
}
