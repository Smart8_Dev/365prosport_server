package com.smart8.sportradar.services;

import com.smart8.sportradar.firebase.DataToPersist;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.converters.DocumentToFbDataConverter;
import com.smart8.sportradar.repositories.MatchRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.xml.transform.StringSource;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Service
public class LiveDataService {

    @Value("${app.path.sportLiveData}")
    private String dataDir;

    @Qualifier("LiveModels")
    @Autowired
    private Jaxb2Marshaller marshaller;

    @Autowired
    UnibetService unibetService;

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private DocumentToFbDataConverter converter;

    private SimpleDateFormat dirFormat = new SimpleDateFormat("yy-MM-dd");

    @PostConstruct
    public void init() {
        Path path = Paths.get(dataDir);

        if(!Files.exists(path)){
            try {
                log.info("data dir not exist, creating");
                Files.createDirectory(path);
            } catch (IOException e) {
                log.error("Failed to create data dir", e);

                throw new RuntimeException(e);
            }
        }
    }

    public Mono<Boolean> persistDocument(String data) {

        return Mono.fromCallable(() -> {
            BetradarLivescoreData livescoreData = (BetradarLivescoreData) marshaller.unmarshal(new StringSource(data));
            writeToFile(data, livescoreData);

            final DataToPersist dataToPersist = converter.convert(livescoreData);

            if ( !dataToPersist.getLive().isEmpty()){
                matchRepository.addLiveMatches(dataToPersist.getLive());
            }

            matchRepository.removeLiveMatches(dataToPersist.getEnded());

            return true;
        }).subscribeOn(Schedulers.elastic());

    }

    private void writeToFile(String betradarLivescoreData, BetradarLivescoreData livescoreData) {
        final Date date = new Date();
        File dir = new File(dataDir, dirFormat.format(date));

        if (!dir.exists()) {
            dir.mkdirs();
        }

        final String filename = (livescoreData.getType() != null ? livescoreData.getType() : "sport") + "_" +
                date.getTime() + ".xml";

        File fileToSave = new File(dir, filename);
        try {
            FileUtils.write(fileToSave, betradarLivescoreData, Charset.forName("UTF-8"));
            log.info("@@writeToFile > saved file: " + fileToSave);
        } catch (IOException e) {
            log.error("@writeToFile > error saving file " + fileToSave, e);
        }
    }
}
