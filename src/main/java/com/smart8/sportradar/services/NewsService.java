package com.smart8.sportradar.services;

import com.smart8.sportradar.enums.Language;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.models.news.Bing;
import com.smart8.sportradar.models.news.News;
import com.smart8.sportradar.models.news.Socials;
import com.smart8.sportradar.utils.ConverterHelpers;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class NewsService {

    @Value("${bing.url}")
    private String BING_URL;

    @Value("${bing.key}")
    private String BING_KEY;

    @Value("${jucier.url}")
    private String JUICER_URL;

    private static final String NEWS_BING_KEY = "news-bing";
    private static final String NEWS_JUICER_KEY = "news-juicer";
    private static final String KEY_SEPARATOR = "-";
    private static final long CLEAR_CACHE_INTERVAL = 1000 * 60 * 20L;

    public enum SocialFeed {
        SOCCER_EN("en_us"),
        SOCCER_NL("nl_nl"),
        DARTS_EN("darts"),
        DARTS_NL("darts_nl");

        private final String uri;
        SocialFeed(String uri) {
            this.uri = uri;
        }
    }
    private static final String[] BING_DARTS_QUERIES = {"pdc AND darts", "uk AND open AND darts"};

    private static final String[] BING_SOCCER_QUERIES = {
        "UEFA AND Champions AND league",
        "Eredivisie",
        "Primera AND Division",
        "Premier AND League"
    };

    private Map<Language, String> bingLanguages = new HashMap<>();
    private HttpHeaders headers;



    @PostConstruct
    public void init() {
        headers = new HttpHeaders();
        headers.add("Ocp-Apim-Subscription-Key", BING_KEY);

        bingLanguages.put(Language.EN, "en-gb");
        bingLanguages.put(Language.NL, "nl-NL");
    }

    @Cacheable(NEWS_BING_KEY)
    public Map<String, News> getNews(Language lang, SportType sportType) {

        log.info("Get bing news. Lang: {}. Sport: {}", lang, sportType);
        String bingLanguage = bingLanguages.getOrDefault(lang, "en-gb");

        RestTemplate restTemplate = new RestTemplate();
        String uri = BING_URL + "search?";

        Map<String, News> newsMap = new HashMap<>();

        if (SportType.Soccer == sportType || SportType.All == sportType) {
            for (String soccerQuery : BING_SOCCER_QUERIES) {
                queryBingNews(restTemplate, uri, bingLanguage, soccerQuery).ifPresent(bingResponse ->
                    newsMap.putAll(buildBingNews(SportType.Soccer.name().toLowerCase(), soccerQuery, bingResponse.getValue()))
                );
            }
        }

        if (SportType.Darts == sportType || SportType.All == sportType) {
            for (String dartsQuery : BING_DARTS_QUERIES) {
                queryBingNews(restTemplate, uri, bingLanguage, dartsQuery).ifPresent(bingResponse ->
                    newsMap.putAll(buildBingNews(SportType.Darts.name().toLowerCase(), dartsQuery, bingResponse.getValue()))
                );
            }
        }
        return newsMap;
    }

    @Cacheable(value=NEWS_JUICER_KEY, unless = "#result.left == false" )
    public Pair<Boolean, Map<String, Socials.NewsItem>> getSocialNews(Language lang, SportType sportType) {

        log.info("Get social news. Lang: {}. Sport: {}", lang, sportType);

        Map<String, Socials.NewsItem> newsMap = new HashMap<>();
        boolean cachable = true;

        if (SportType.Soccer == sportType || SportType.All == sportType) {
            SocialFeed feed = Language.EN == lang ? SocialFeed.SOCCER_EN : SocialFeed.SOCCER_NL;
            Socials socialNews = querySocial(feed);
            if ( socialNews == null){
                cachable = false;
                log.info("This Social result NOT cachable");
            }else{
                newsMap.putAll(buildSocialNews(feed, socialNews));
            }
        }
        if (SportType.Darts == sportType || SportType.All == sportType) {
            SocialFeed feed = Language.EN == lang ? SocialFeed.DARTS_EN : SocialFeed.DARTS_NL;
            Socials socialNews = querySocial(feed);
            if ( socialNews == null){
                cachable = false;
                log.info("This Social result NOT cachable");
            }else{
                newsMap.putAll(buildSocialNews(feed, socialNews));
            }
        }
        log.info("News map size is {}", newsMap.size());
        return Pair.of(cachable, newsMap);
    }

    @Scheduled(fixedRate = CLEAR_CACHE_INTERVAL)
    @CacheEvict(value = NEWS_BING_KEY, allEntries = true)
    public void evictNews() {
        log.info("Clear bing news cache.");
    }

    @Scheduled(fixedRate = CLEAR_CACHE_INTERVAL)
    @CacheEvict(value = NEWS_JUICER_KEY, allEntries = true)
    public void evictSocials() {
        log.info("Clear social news cache.");
    }


    private Socials querySocial(SocialFeed socialFeed) {
        String url = JUICER_URL + socialFeed.uri + "?per=" + 100 + "&page=1";
        log.info("Quering social feed {}", url);
        RestTemplate template = new RestTemplate();

        try {
            ResponseEntity<Socials> responseEntity = template.exchange(url, HttpMethod.GET, null, Socials.class);

            if (responseEntity.getStatusCodeValue() < 300) {
                Socials body = responseEntity.getBody();
                log.debug("Request success. Response contains {} posts", body == null ? null : body.getPosts().getItems().size());
                return responseEntity.getBody();
            } else {
                log.error("Error on fetching juicer news {}. Response code: {}", url, responseEntity.getStatusCodeValue());
            }
        } catch (Exception ex) {
            log.error("Error on fetching social news. {}", ex.getMessage());
        }
        return null;
    }

    private Optional<Bing> queryBingNews(RestTemplate restTemplate, String uri, String language, String soccerQuery) {
        final String callUri = uri + String.format("q=%s&count=50&offset=0&mkt=%s&safeSearch=moderate", soccerQuery, language);
        log.info("Fetching bing News by url '{}'", callUri.replaceAll("\\s", "%"));

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        try {
            final ResponseEntity<Bing> responseEntity = restTemplate.exchange(callUri, HttpMethod.GET, entity, Bing.class);

            if (responseEntity.getStatusCodeValue() < 300) {
                final Bing body = responseEntity.getBody();
                if (body == null) {
                    log.info("Response body is null.");
                    return Optional.empty();
                } else {
                    log.info("Get {} bing news ", body.getValue().size());
                    return Optional.of(body);
                }
            } else {
                log.error("Bing response failed");
            }
        } catch (Exception ex) {
            log.error("Error on fetching bing news. {}", ex.getMessage());
        }
        return Optional.empty();
    }

    private Map<String, News> buildBingNews(String sport, String sportQuery, List<Bing.Value> newsList) {
        Map<String, News> data = new HashMap<>();

        String posterName = null;
        String image = null;
        for (Bing.Value value : newsList) {
            final List<Bing.Provider> provider = value.getProvider();
            final Bing.ImageContainer image1 = value.getImage();
            if (image1 != null) {
                if (image1.getThumbnail() != null) {
                    image = image1.getThumbnail().getContentUrl();
                }
            }
            if (MyCollections.isNotEmpty(provider)) {
                final Bing.Provider provider1 = provider.get(0);
                posterName = provider1.getName();
            }
            final String hashed = DigestUtils.md5Hex(value.getName());
            String id = value.getDatePublished() + KEY_SEPARATOR + sport + hashed;
            final News news = News.builder()
                .id(id)
                .category(value.getCategory())
                .fullUrl(value.getUrl())
                .unformattedMessage(value.getName())
                .description(value.getDescription())
                .posterName(posterName)
                .source(sportQuery)
                .image(image != null ? image.replace("&pid=News", "") : null)
                .externalCreatedAt(value.getDatePublished()).build();

            data.put(id, news);
        }

        return data;
    }

    private Map<String, Socials.NewsItem> buildSocialNews(SocialFeed socialFeed, Socials socialNews) {
        Map<String, Socials.NewsItem> map = new HashMap<>();

        Socials.Posts posts = socialNews.getPosts();
        if (posts != null) {
            List<Socials.NewsItem> items = posts.getItems();

            if (MyCollections.isNotEmpty(items)) {
                for (Socials.NewsItem item : items) {
                    Instant now = Instant.now();
                    StringBuilder builder = new StringBuilder(ConverterHelpers.newsFormat.format(now));

                    switch (socialFeed) {
                        case SOCCER_EN:
                        case SOCCER_NL:
                            builder.append(KEY_SEPARATOR).append(SportType.Soccer.name().toLowerCase()).append(KEY_SEPARATOR).append(item.getId());
                            break;
                        case DARTS_EN:
                        case DARTS_NL:
                            builder.append(KEY_SEPARATOR).append(SportType.Darts.name().toLowerCase()).append(KEY_SEPARATOR).append(item.getId());
                            break;
                    }

                    map.put(builder.toString(), item);
                }
            }
        }
        return map;
    }

}
