package com.smart8.sportradar.services;

public enum StatusCode {

    NotStarted(0),
    Delayed(61),
    Postponed(60),
    Cancelled(70),
    Interrupted(80),
    Suspended(81),
    Abandoned(90),
    Started(20),
    Period1(1),
    Period2(2),
    Period3(3),
    Period4(4),
    Break(30),
    Overtime(40),
    Penalties1(51),
    Ended(100),
    Aet(110),
    Ap(120),
    FirstHalf(6),
    SecondHalf(7),
    Halftime(31),
    FirstExtra(41),
    SecondExtra(42),
    ExtraTimeHalftime(33),
    AwaitExtraTime(32),
    AwaitPenalties(34),
    Penalties(50);


    public int statusCode;

    StatusCode(int statusCode) {

        this.statusCode = statusCode;
    }

    public static boolean isEnded(StatusCode statusCode){
        return statusCode == Postponed || statusCode == Cancelled || statusCode == Abandoned || statusCode == Ended || statusCode == Aet || statusCode == Ap || statusCode == Suspended;
    }

    public static StatusCode getByCode(int code){
        for (StatusCode statusCode : StatusCode.values()) {
            if(statusCode.statusCode == code){
                return statusCode;
            }
        }

        return NotStarted;
    }

    public static int getCurrentMatchTimeByStatus(int statusCode, long periodStarted, long currentTime){
        final StatusCode byCode = getByCode(statusCode);

        switch (byCode) {
            case NotStarted:
            case Delayed:
            case Postponed:
            case Cancelled:
            case Abandoned:
            case Interrupted:
            case Started:
                return 0;
            case Period1:
            case FirstHalf:
                return diff((currentTime - periodStarted), 0);
            case Period2:
            case SecondHalf:
                return diff((currentTime - periodStarted), 45);
            case Period3:
            case FirstExtra:
                return diff((currentTime - periodStarted), 90);
            case Period4:
            case SecondExtra:
                return diff((currentTime - periodStarted), 105);
            case Break:
                return 45;
            case Overtime:
                return 45;
            case Penalties1:
                return 120;
            case Ended:
                return 90;
            case Aet:
                return 120;
            case Ap:
                return 120;
            case Halftime:
                return 45;
            case ExtraTimeHalftime:
                return 105;
            case AwaitExtraTime:
                return 105;
            case AwaitPenalties:
                return 120;
            case Penalties:
                return 120;
        }

        return 0;
    }

    private static int diff(long diff, int extraMinutes) {
        return (int) (diff / (60 * 1000)) + extraMinutes;
    }

    public static StatusCode getPeriodByStatusCode(int code) {
        final StatusCode byCode = getByCode(code);

        switch (byCode) {
            default:
                return null;
            case Started:
            case Period1:
            case FirstHalf:
                return FirstHalf;
            case Period2:
            case SecondHalf:
                return SecondHalf;
            case Period3:
            case FirstExtra:
                return FirstExtra;
            case Period4:
            case SecondExtra:
                return SecondExtra;
            case Overtime:
                return FirstExtra;
            case Penalties1:
                return Penalties;
            case Ended:
                return SecondHalf;
            case Aet:
                return SecondExtra;
            case Ap:
                return Penalties;
            case Halftime:
                return FirstHalf;
            case ExtraTimeHalftime:
                return FirstHalf;
            case AwaitExtraTime:
                return FirstExtra;
            case AwaitPenalties:
                return SecondExtra;
            case Penalties:
                return Penalties;
        }
    }
    public static boolean isFirstHalfEnded(int code){
        final StatusCode statusCode = getByCode(code);
        return statusCode == SecondHalf || statusCode == Overtime || statusCode == Ended
            || statusCode == Penalties || statusCode == Halftime || statusCode == FirstExtra
            || statusCode == SecondExtra || statusCode == ExtraTimeHalftime || statusCode == AwaitExtraTime
            || statusCode == AwaitPenalties;
    }

    public static boolean isSecondHalfEnded(int code){
        final StatusCode statusCode = getByCode(code);
        return statusCode == Overtime || statusCode == Ended
            || statusCode == Penalties || statusCode == FirstExtra
            || statusCode == SecondExtra || statusCode == ExtraTimeHalftime || statusCode == AwaitExtraTime
            || statusCode == AwaitPenalties;
    }

    public static boolean notStarted(StatusCode statusObj) {
        return statusObj == NotStarted || statusObj == Delayed || statusObj == Postponed;
    }
}
