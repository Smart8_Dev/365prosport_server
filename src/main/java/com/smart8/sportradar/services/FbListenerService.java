package com.smart8.sportradar.services;

import com.google.firebase.database.*;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.*;
import com.smart8.sportradar.models.MiniMatch;
import com.smart8.sportradar.repositories.FavoritesRepository;
import com.smart8.sportradar.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Slf4j
@Service
public class FbListenerService {

    @Autowired
    @Qualifier("fireBaseRef")
    private DatabaseReference firebaseDb;

    @Autowired
    private FavoritesRepository favoritesRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    DatabaseReference.CompletionListener logListener;

    @Autowired
    UserEventService userEventService;

    @Autowired
    private BetResolveService betResolveService;

    private static final String BETS = "bets";
    private static final String USER_ID = "userId";
    private static final String TYPE = "type";
    private static final String ENTITY_ID = "entityId";

    /** EntityId, Listener */
    private Map<Long, ChildEventListener> todayFavoritesListeners = new HashMap<>();
    private GenericTypeIndicator<Map<String, Object>> favoritesResponseType = new GenericTypeIndicator<Map<String, Object>>(){};

    /** MatchId, Listener */
    private Map<String, ChildEventListener> todayMatchBetsListeners = new HashMap<>();
    private Map<String, String> userLocaleMap = new HashMap<>();

    @PostConstruct
    public void init() {
        this.listenUserTokens();
        this.checkTodayMatches();
        this.listenUserProfile();
        this.listenFriendRequests();
    }

    @Scheduled(cron = "0 0 2 * * ?")
    public void updateData() {
        checkTodayMatches();
    }

    public boolean isMatchObservable(String matchId) {
        return todayFavoritesListeners.get(Long.parseLong(matchId)) != null;
    }

    String getUserLocaleStr(String userId) {
        return userLocaleMap.getOrDefault(userId, "en");
    }

    private void listenUserTokens() {
        firebaseDb.child("user/user-tokens").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                log.debug("New user token added...");
                userRepository.setUserToken(dataSnapshot.getKey(), (String) dataSnapshot.getValue());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
                log.debug("User token changed...");
                userRepository.setUserToken(dataSnapshot.getKey(), (String) dataSnapshot.getValue());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                log.debug("User token removed...");
                userRepository.removeUserToken(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    void updateBetStatus(String betId, FbBet.Status status) {
        log.debug("Updating bet status to {}.", status);
        firebaseDb.child(BETS).child(betId).child("status").setValue(status, logListener);
    }

    public void updateUserCoins(String userId, int addCoins, boolean added) {
        if (added) {
            log.debug("Adding {} coins to user {}. ", addCoins, userId);
        } else {
            log.debug("Returning {} coins to user {}. ", addCoins, userId);
        }

        DatabaseReference coinRef = firebaseDb.child("user/profile/" + userId);
        coinRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                MutableData coinsData = mutableData.child("coins");
                MutableData pointsData = mutableData.child("points");

                Integer points = pointsData.getValue(Integer.class);

                if (added) {
                    if (points == null) {
                        pointsData.setValue(addCoins);
                        log.debug("Points was null. User new point balance is {}", addCoins);
                    } else {
                        log.debug("User new point balance {} + {} = {}", points, addCoins, points + addCoins);
                        pointsData.setValue(points + addCoins);
                    }
                }
                Integer coins = coinsData.getValue(Integer.class);

                if (coins != null) {
                    log.debug("User new balance {} + {} is {}", coins, addCoins, coins + addCoins);
                    coinsData.setValue(coins + addCoins);

                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError error, boolean committed, DataSnapshot currentData) {
                if (!committed) {
                    log.error("Error on updating user balance. {}. Error: {}", currentData, error);
                }
            }
        });
    }

    private void listenUserProfile() {
        firebaseDb.child("user/profile").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                log.debug("New user profile added...");
                processUserNotificationSettings(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
                log.debug("User profile changed.");
                processUserNotificationSettings(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                log.debug("User profile {} removed.", dataSnapshot.getKey());
                userLocaleMap.remove(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void processUserNotificationSettings(DataSnapshot dataSnapshot) {
        try {
            FbUserProfile userProfile = dataSnapshot.getValue(FbUserProfile.class);
            log.debug("User profile '{}'", userProfile);
            if (userProfile.getLanguage() != null) {
                userLocaleMap.put(dataSnapshot.getKey(), userProfile.getLanguage());
            }
            if (userProfile.getNotifications() != null) {
                userRepository.storeUserNotificationSettings(dataSnapshot.getKey(), userProfile.getNotifications());
            }
        } catch (Exception ex) {
            log.error("Error on parsing userProfile {} {} ", dataSnapshot.getKey(), ex.getMessage());
        }
    }

    void removeFavoriteMatchListener(FbMatch match) {
        //Remove only match
        ChildEventListener matchListener = todayFavoritesListeners.get(Long.parseLong(match.getId()));
        if (matchListener == null) {
            log.warn("Listener for match '{}' not exists.", match.getId());
        }else{
            log.debug("Removing listener for match {}", match.getId());
            firebaseDb.child(Db.FAVORITES).orderByChild(ENTITY_ID).equalTo(match.getId()).removeEventListener(matchListener);
        }
    }


    /**
     * Find today matches. Add listeners and store in redis.
     * <p>
     * This method should be invoked on once a day basis.
     */
    private void checkTodayMatches() {
        clearFavoritesListeners();
        clearUserBets();

        final long startTimeMs = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).toEpochSecond(ZoneOffset.UTC) * 1000;
        final long endTimeMs = LocalDateTime.now().withHour(23).withMinute(59).withSecond(59).toEpochSecond(ZoneOffset.UTC) * 1000;
        log.info("Searching for today matches from {} to {}", startTimeMs, endTimeMs);

        List<String> sports = Arrays.asList(
            SportType.Soccer.name().toLowerCase(),
            SportType.Darts.name().toLowerCase()
        );

        sports.forEach(sport ->
            firebaseDb.child(sport).child(Db.MATCHES).orderByChild("dateTimeInt").startAt(startTimeMs).endAt(endTimeMs)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        log.debug("Number of today {} matches {}.", sport, snapshot.getChildrenCount());
                        handleTodayMatches(snapshot);
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {}
                })
        );
    }

    private void handleTodayMatches(DataSnapshot matchesSnapshot) {
        Set<Long> entityIds = new HashSet<>();

        for (DataSnapshot item : matchesSnapshot.getChildren()) {
            addTodayMatchBetsListener(item.getKey());

            entityIds.add(Long.parseLong(item.getKey()));
            try {
                MiniMatch match = item.getValue(MiniMatch.class);
                log.info("Today match: {}", match);
                match.getContestants().forEach(contestant -> entityIds.add(contestant.getId()));
                entityIds.add(match.getTournamentId());
            } catch (Exception ex) {
                log.error("Error on parsing match snapshot: {}. Error: {}", item, ex.getMessage());
            }
        }
        addTodayFavoritesListeners(entityIds);
    }

    private void clearFavoritesListeners() {
        log.debug("Clear match favorites listeneres.");
        for (Map.Entry<Long, ChildEventListener> entry : todayFavoritesListeners.entrySet()) {
            firebaseDb.child(Db.FAVORITES).orderByChild(ENTITY_ID).equalTo(entry.getKey()).removeEventListener(entry.getValue());

        }
        todayFavoritesListeners.clear();
    }

    private void clearUserBets() {
        log.debug("Clear User bets listeneres.");
        for (Map.Entry<String, ChildEventListener> entry : todayMatchBetsListeners.entrySet()) {
            firebaseDb.child(BETS).orderByChild("matchId").equalTo(entry.getKey()).removeEventListener(entry.getValue());
        }
        todayMatchBetsListeners.clear();
        betResolveService.clear();
    }

    /**
     * Method adds listeners that listen for added or removed users favorites
     * Any changes in firebase mirrored in redis.
     * Listener stored in map like entity -> listener  for @matchId
     * @param entityIdSet set of entity Ids where entity can be of type matches|teams|tournaments
     */
    private void addTodayFavoritesListeners(Set<Long> entityIdSet) {
        for( Long entityId : entityIdSet) {
            ChildEventListener listener = firebaseDb.child(Db.FAVORITES).orderByChild(ENTITY_ID).equalTo(entityId)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
                        try{
                            Map<String, Object> response = snapshot.getValue(favoritesResponseType);
                            String type = (String)response.get(TYPE);
                            String userId = (String)response.get(USER_ID);
                            log.info("UserId '{}' added {}:{} to favorites.", userId,type, entityId );
                            favoritesRepository.addUserToFavorites((String)response.get(TYPE), entityId, (String)response.get(USER_ID));
                        }catch (Exception ex){
                            log.error("Error on parsing favorties {}. Error {}", snapshot, ex.getMessage());
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
                        log.info("Key {} changed", previousChildName);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot snapshot) {
                        try {
                            Map<String, Object> response = snapshot.getValue(favoritesResponseType);
                            String type = (String)response.get(TYPE);
                            String userId = (String)response.get(USER_ID);
                            log.debug("UserId '{}' removed {}:{} from favorites.", userId,type, entityId );
                            favoritesRepository.removeUserFromFavorites(type, entityId, userId);
                        } catch (Exception ex) {
                            log.error("Error on parsing favorites {}. Error {}", snapshot, ex.getMessage());
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot snapshot, String previousChildName) {
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        log.warn("Listener removed. {}", error);
                    }
                });
            todayFavoritesListeners.put(entityId, listener);
        }
    }

    private void addTodayMatchBetsListener(String matchId) {
        log.info("Fetching bets on today match {}", matchId);
        ChildEventListener listener = firebaseDb.child(BETS).orderByChild("matchId").equalTo(matchId)
            .addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
                    try {
                        FbBet fbBet = snapshot.getValue(FbBet.class);
                        log.debug("User '{}' made bet of type '{}' on match '{}'.", fbBet.getUserId(), fbBet.getCriterionId(), matchId);
                        if (fbBet.getStatus() == FbBet.Status.Active) {
                            betResolveService.addBetToMatch(matchId, fbBet);
                        }
                    } catch (Exception ex) {
                        log.error("Error on parsing bet {} for match {}. Error {}", snapshot.getKey(), matchId, ex.getMessage());
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
                    log.info("Node bets Key {} changed", snapshot.getKey());
                }

                @Override
                public void onChildRemoved(DataSnapshot snapshot) {
                    FbBet fbBet = snapshot.getValue(FbBet.class);
                    log.debug("User '{}' removed bet {} on match {}.", fbBet.getUserId(), fbBet.getId(), matchId);
                    betResolveService.removeBetFromMatch(matchId, fbBet);
                }

                @Override
                public void onChildMoved(DataSnapshot snapshot, String previousChildName) {
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    log.warn("Listener removed. {}", error);
                }
            });
        todayMatchBetsListeners.put(matchId, listener);
    }

    private void listenFriendRequests() {
        firebaseDb.child("user/friend-requests").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                log.debug("New friend request '{}' added.", dataSnapshot);
                try {
                    FbFriendRequest request = dataSnapshot.getValue(FbFriendRequest.class);
                    userEventService.processFriendRequest(dataSnapshot.getKey(), request);
                } catch (Exception e) {
                    log.error("Error on parsing friend request");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
                log.debug("Friend request {}  changed.", dataSnapshot.getKey());
                try {
                    FbFriendRequest request = dataSnapshot.getValue(FbFriendRequest.class);
                    userEventService.processFriendRequest(dataSnapshot.getKey(), request);
                } catch (Exception e) {
                    log.error("Error on parsing friend request");
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                log.debug("Friend request removed. ID: {}", dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void returnMatchBets(String sportRef, String matchId, boolean deleteMatch) {
        log.debug("Ruturn bets for match {}:{}", sportRef, matchId);
        firebaseDb.child(BETS).orderByChild("matchId").equalTo(matchId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    log.debug("Bets to return {}", item.getKey());
                    try {
                        FbBet fbBet = item.getValue(FbBet.class);
                        if (fbBet.getStatus() == FbBet.Status.Active) {
                            log.debug("Returning bet on postponed/cancelled match. {}", fbBet);
                            updateBetStatus(fbBet.getId(), FbBet.Status.NoBet);
                            updateUserCoins(fbBet.getUserId(), fbBet.getStake(), false);
                        }
                    } catch (Exception ex) {
                        log.error("Error on parsing bet {} for match {}. Error {}", snapshot.getKey(), matchId, ex.getMessage());
                    }
                }
                if (deleteMatch) {
                    firebaseDb.child(sportRef).child("matches").child(matchId).setValue(null, (e, r) -> {
                        if (e == null) {
                            log.debug("Match {} Deleted.", matchId);
                        } else {
                            log.error("Error {} on deleting match {}.", e.getMessage(), matchId);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }
}

