package com.smart8.sportradar.services;

import com.smart8.sportradar.firebase.FbFriendRequest;
import com.smart8.sportradar.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserEventService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationService notificationService;

    void processFriendRequest(String requestId, FbFriendRequest request) {
        if (userRepository.incrimentFriendRequest(requestId, request.getStatus())) {
            if (request.getStatus() == FbFriendRequest.InviteStatus.REJECTED) {
                log.info( "User {} rejected friend request of {}", request.getInvited(), request.getInviter());
            }else{
                notificationService.notifyFriendRequest(request);
            }
        } else {
            log.debug("User already notified.");
        }
    }
}
