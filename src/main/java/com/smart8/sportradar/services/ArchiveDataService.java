package com.smart8.sportradar.services;

import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ArchiveDataService {

    @Value("${app.path.sportLiveData}")
    private String pathToData;

    /**
     * Number of folder to zip at once
     */
    private final static int NUM_OF_ZIP_FOLDERS = 7;

    private final static long G2 = 2*1024*1024*1024L;

    @Scheduled(cron = "0 1 1 * * ?")
    public void zipData() throws IOException {
        log.info("Run smart8Data folders zipping...");
        Path path = Paths.get(pathToData);

        if ( path.toFile().getFreeSpace() < G2 ){
            log.error("Not enough space for zipping file.");
            return;
        }
        LocalDateTime now = LocalDateTime.now();
        String todayDate = ConverterHelpers.shortDateFormat.format(now);
        List<Path> list = Files.list(path)
            .filter(Files::isDirectory)
            .filter(p -> !todayDate.equals(p.getFileName().toString()))
            .collect(Collectors.toList());

        int count = 0;
        for(Path folder : list ){
            if ( count >= NUM_OF_ZIP_FOLDERS){
                break;
            }
            zipFolder(folder);
            count++;
        }
    }

    private void zipFolder(Path folderPath){
        final String folderName = folderPath.getFileName().toString();
        File[] files = folderPath.toFile().listFiles();

        if (files == null ){
            log.warn("Folder {} is empty.", folderName);
            return;
        }
        try (SevenZOutputFile sevenZOutput = new SevenZOutputFile(new File(folderPath.getParent().toString() + "/" + folderName + ".7z"))) {
            for(File file : files){
                SevenZArchiveEntry entry = sevenZOutput.createArchiveEntry(file, folderName + "/" + file.getName());
                sevenZOutput.putArchiveEntry(entry);
                sevenZOutput.write(Files.readAllBytes(Paths.get(file.toURI())));
                sevenZOutput.closeArchiveEntry();
            }
            log.info("Folder {} zipped." , folderName );
        }catch (IOException ex) {
            log.error("Error on zipping {} folder.", folderName);
            return;
        }
        deleteFolder(folderPath);
    }

    private void deleteFolder(Path folderPath) {
        try {
            FileUtils.deleteDirectory(folderPath.toFile());
        } catch (IOException e) {
            log.error("Error on deleting {} folder.", folderPath.getFileName());
        }
    }
}
