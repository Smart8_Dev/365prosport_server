package com.smart8.sportradar.services;

import com.smart8.sportradar.models.unibet.UniLiveResult;
import com.smart8.sportradar.models.unibet.UniResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
public class UnibetService {

    @Value("${unibet.url}")
    private String baseUrl;

    @Value("${unibet.appId}")
    private String appId;

    @Value("${unibet.appKey}")
    private String appKey;

    public UniResult fetchPreMatchBets(int start, long mainGroupId, List<Long> subGroupIds){
        StringBuilder urlBuilder = new StringBuilder(baseUrl);
        urlBuilder.append(String.format("betoffer/group/%d.json?app_id=%s&app_key=%s&type=2&includeparticipants=true&outComeSortBy=lexical&outComeSortDir=desc&rangeStart=%d", mainGroupId,
                appId, appKey, start));

        for (long subGroupId : subGroupIds) {
            urlBuilder.append("&id=").append(subGroupId);
        }
        final String fullUrl = urlBuilder.toString();
        log.info("@fetchPreMatchBets > url: " + fullUrl);

        return getUniResult(fullUrl);
    }


    public UniLiveResult fetchLiveBets(int start, Long groupId) {
        final String fullUrl = baseUrl
            + String.format("betoffer/live/group/%d.json?app_id=%s&app_key=%s&type=2&includeparticipants=true&outComeSortBy=lexical&outComeSortDir=desc&rangeStart=%d", groupId,
            appId, appKey, start);

        log.debug("Fetch live bets for {} group. Offset {}", groupId, start);

        return getUniLiveResult(fullUrl);
    }

    private UniResult getUniResult(String url){
        RestTemplate template = new RestTemplate();
        final ResponseEntity<UniResult> entity;

        try {
            entity = template.getForEntity(url, UniResult.class);
            return entity.getStatusCodeValue() < 300 ? entity.getBody() : null;
        } catch (RestClientException e) {
            log.warn("Error while fetching {}", url);
            return null;
        }
    }

    private UniLiveResult getUniLiveResult(String url){
        RestTemplate template = new RestTemplate();
        final ResponseEntity<UniLiveResult> entity;

        try {
            entity = template.getForEntity(url, UniLiveResult.class);
            return entity.getStatusCodeValue() < 300 ? entity.getBody() : null;
        } catch (RestClientException e) {
            log.warn("Error while fetching {}", url);
            return null;
        }
    }
}
