package com.smart8.sportradar.services;

import com.smart8.sportradar.firebase.FbPlayer;
import com.smart8.sportradar.converters.stats.PlayerToFbPlayerConverter;
import com.smart8.sportradar.reports.PlayerMappingReport;
import com.smart8.sportradar.statisticsmodels.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SquadService {

    private FirebaseService fbService;

    private PlayerMappingReport report;

    private PlayerToFbPlayerConverter converter;

    private final static int INTERNATIONAL_CAT_ID = 4;
    private final static int INTERNATIONAL_YOUTH_CAT_ID = 392;

    @Autowired
    SquadService(FirebaseService fbService, PlayerMappingReport report, PlayerToFbPlayerConverter converter){
        this.fbService = fbService;
        this.report = report;
        this.converter = converter;
    }

    public void processSquad(SportradarData sportradarData) {

        final List<Sport> list = sportradarData.getSport();
        String fileName = sportradarData.getFileName();

        for (Sport sport : list) {
            final String sportRef = sport.getName().toLowerCase();
            final List<Team> teams = sport.getTeams().getTeam();
            List<FbPlayer> fbPlayerList = new ArrayList<>();

            for (Category cat:  sport.getCategory()){
                if (cat.getId() == INTERNATIONAL_CAT_ID || cat.getId() == INTERNATIONAL_YOUTH_CAT_ID){
                    log.info("Discard international team squad" );
                    return;
                }
            }

            for (Team radarTeam : teams) {

                final List<Player> players = radarTeam.getPlayers().getPlayer();

                for ( Player player : players) {
                    final FbPlayer fbPlayer = converter.convertRadarPlayerToFbPlayer(player);
                    fbPlayer.setTeam(radarTeam.getName());
                    fbPlayer.setTeamId(radarTeam.getId().longValue());
                    log.info("@processSquad > updating {} player {}:{} {}", sportRef, fbPlayer.getId(), fbPlayer.getFirstName(), fbPlayer.getLastName());

                    fbService.updateGeneralPlayer(fbPlayer, sportRef) ;

                    fbPlayerList.add(fbPlayer);
                }
            }

            report.writeReport(fbPlayerList, "/home/nodeuser/sportradar/missingData/missing-playersData-" + fileName + ".txt");
        }
    }

}
