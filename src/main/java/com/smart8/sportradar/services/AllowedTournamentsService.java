package com.smart8.sportradar.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.smart8.sportradar.mappers.ResourceMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

@Service
@Slf4j
public class AllowedTournamentsService {

    public static final String ALLOWED_TOURNAMENTS_JSON = "appdata/allowedTournaments.json";

    @Autowired
    ResourceMapper resourceMapper;

    private Map<String, Boolean> allowedTournaments;

    @PostConstruct
    public void init(){
        this.allowedTournaments = resourceMapper.readDataToMap(ALLOWED_TOURNAMENTS_JSON, new TypeReference<Map<String, Boolean>>() {});
        log.info("{} tournaments allowed", this.allowedTournaments.size());
    }

    boolean isAllowed(String tournamentId){
        return allowedTournaments != null && allowedTournaments.containsKey(tournamentId);
    }
}
