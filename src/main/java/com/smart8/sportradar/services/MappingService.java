package com.smart8.sportradar.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.models.MiniMatch;
import com.smart8.sportradar.repositories.MatchRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class MappingService {

    private MatchRepository repository;

    private ObjectMapper mapper;

    @Autowired
    public MappingService(MatchRepository repository){
        this.repository = repository;

        //Not using ObjectMapper injection because of custom configuration
        this.mapper = new ObjectMapper();
        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public List<MiniMatch> getUnmappedMatches() {
        final long startTimeMs = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).toEpochSecond(ZoneOffset.UTC) * 1000;

        List<MiniMatch> matches = repository.getUnmappedMatchesAfter(startTimeMs);
        log.debug("Found {} unmapped matches in future", matches.size());
        return matches;
    }

    public FbSoccerMatch.FbMiniMatch getUnmappedMatch(String matchId) {
        return  repository.getMatch(matchId);
    }

    public void storeTeamMapping(String unibetTeamId, String radarTeamId) {
        repository.storeTeamMapping(Long.parseLong(unibetTeamId), radarTeamId);
    }


    /**
     * Store tmatch mapping and remove unmapped match from db. Not transactional
     */
    public void storeMatchMapping(String unibetTeamId, String radarMatchId) {
        repository.storeMatchMapping(Long.parseLong(unibetTeamId), radarMatchId);
        repository.removeUnmappedEvent(unibetTeamId);
        repository.removeUnmappedMatch(radarMatchId);
    }

    public long getMappedTeam(long radarTeamId) {
        return repository.getUnibetTeamIdByRadarTeamId(radarTeamId);
    }

    public Map<String, String> findUnmappedEvents(String pattern) {
        return repository.findUnmappedEvents(pattern);
    }
}
