package com.smart8.sportradar.services;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.smart8.sportradar.fb.FbStandings;
import com.smart8.sportradar.firebase.*;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.mappers.ResourceMapper;
import com.smart8.sportradar.models.unibet.BetOffer;
import com.smart8.sportradar.repositories.MatchRepository;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.util.*;
import java.util.function.Consumer;

@Slf4j
@Service
public class FirebaseService {

    private static final String TOURNAMENTS_JSON = "appdata/tournaments.json";
    private static final String TEAMS_JSON = "appdata/teams.json";

    @Autowired
    @Qualifier("fireBaseRef")
    DatabaseReference firebaseDb;

    private DatabaseReference soccerDb;

    @Autowired
    DatabaseReference.CompletionListener logListener;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    ResourceMapper resourceMapper;

    @Autowired
    MatchRepository repository;

    @Autowired
    private AllowedTournamentsService allowedTournamentsService;

    private Map<String, FbTournament> tournamentMap;
    private Map<String, FbTeam> teamMap;

    @PostConstruct
    public void init() {
        initTournaments();
        initTeams();

        this.soccerDb = firebaseDb.child(Db.SOCCER);
        this.soccerDb.child("tournament-standing").removeValue();
    }

    public FbTeam getTeam(String id) {
        return teamMap.get(id);
    }

    public FbTournament getTournament(String id) {
        return tournamentMap.get(id);
    }

    private void initTeams() {
        Map<String, FbTeam> teams = resourceMapper.readDataToMap(TEAMS_JSON, new TypeReference<Map<String, FbTeam>>() {
        });
        log.info("{} teams initialized", teams.size());
        this.teamMap = teams;
    }

    private void initTournaments() {
        this.tournamentMap = resourceMapper.readDataToMap(TOURNAMENTS_JSON, new TypeReference<Map<String, FbTournament>>() {
        });
    }

    void saveTournaments(Map<String, FbTournament> map, String ref) {
        log.info("Saving {} tournaments", map.size());
        save(map, firebaseDb.child(ref).child(Db.TOURNAMENTS));
        log.info("Tournaments saved");
    }

    private <T> boolean save(Map<String, T> map, DatabaseReference ref) {
        ref.setValue(map, logListener);
        return true;
    }

    public boolean contains(FbTournament tournament) {
        return tournamentMap.containsKey(tournament.getId());
    }

    public void save(FbTournament tournament) {
        log.info("@save > saving tournament: " + tournament);
        soccerDb.child(Db.TOURNAMENTS).setValue(Collections.singletonMap(tournament.getId(), tournament));
    }

    void saveStandings(String tournamentId, String divisionId, FbStandings standingRow, String ref) {
        log.debug("Inserting {} table rows to standings. Path soccer->standings->{}->{}. ", standingRow.getStandings().size(), tournamentId, divisionId);
        firebaseDb.child(ref.toLowerCase()).child(Db.STANDINGS).child(tournamentId).child(divisionId).setValue(standingRow, logListener);
    }

    public void saveMatches(List<FbMatch> matches, String ref) {
        log.debug("Persisting {} matches to firebase node {}", matches.size(), ref);
        for (FbMatch match : matches) {
            Map<String, Object> flattenMap = ConverterHelpers.removeNullsAndFlattenMap(mapper.convertValue(match, Map.class), "");
            firebaseDb.child(ref).child(Db.MATCHES).child(String.valueOf(match.getId())).updateChildren(flattenMap, logListener);
            //saveToLocalDB(match);
        }
    }

    private void saveToLocalDB(FbMatch fbMatch) {
        log.debug("Saving match:{} to local DB", fbMatch.getId());
        final FbSoccerMatch.FbMiniMatch miniMatch = FbSoccerMatch.FbMiniMatch.builder()
            .contestants(fbMatch.getContestants())
            .dateTime(fbMatch.getDateTime())
            .dateTimeInt(fbMatch.getDateTimeInt())
            .id(String.valueOf(fbMatch.getId()))
            .tournamentId(fbMatch.getTournamentId().toString())
            .tournamentName(fbMatch.getTournamentName())
            .build();

        try {
            repository.storeRadarMatch(miniMatch.getId(), mapper.writeValueAsString(miniMatch));
        } catch (JsonProcessingException e) {
            log.error("Mapping match Error ", e);
        }
        repository.addMatchDate(fbMatch);

        if (repository.getUnibetMatchIdByRadarMatchId(miniMatch.getId()) == -1) {
            log.info("Can't find matching unibet id for match: {}", miniMatch.getId());
            repository.storeUnmappedMatch(fbMatch);
        }
    }

    public boolean isAllowed(BetradarLivescoreData.Sport.Category.Tournament tournament) {
        final BigInteger uniqueTournamentId = tournament.getUniqueTournamentId();
        if (uniqueTournamentId != null) {
            return isAllowed(String.valueOf(uniqueTournamentId));
        } else {
            return isAllowed(String.valueOf(tournament.getBetradarTournamentId()));
        }
    }

    boolean isAllowed(String tourId) {
        return allowedTournamentsService.isAllowed(tourId);
    }

    public void putTeam(FbTeam fbTeam) {
        this.teamMap.put(fbTeam.id, fbTeam);
    }

    public void updateOdds(Map<String, Set<BetOffer>> betOfferMap, String ref) {
        for (Map.Entry<String, Set<BetOffer>> entry : betOfferMap.entrySet()) {
            LinkedList<BetOffer> list = new LinkedList<>();
            for (BetOffer value : entry.getValue()) {
                if (value.getName().equalsIgnoreCase("Full Time")) {
                    list.addFirst(value);
                } else {
                    list.add(value);
                }
            }
            firebaseDb.child(ref.toLowerCase()).child(Db.MATCHES)
                .child(entry.getKey())
                .updateChildren(Collections.singletonMap("bets", list), (error, r) -> {
                    if (error != null) {
                        log.error("Error while updating bet offers.", error.toException());
                    }
                });
        }
    }

    void updateGeneralPlayer(FbPlayer player, String ref) {
        firebaseDb.child(ref).child(Db.PLAYERS).updateChildren(Collections.singletonMap(String.valueOf(player.getId()), player), logListener);
    }

    void saveGeneralTeam(FbTeam team, String ref) {
        firebaseDb.child(ref).child(Db.TEAMS).updateChildren(Collections.singletonMap(team.getId(), team), logListener);
    }

    void saveMatch(FbMatch match, String ref) {
        log.debug("Saving match to ref '{}'. Match {}", ref, match);
        Map<String, Object> flattenMap = ConverterHelpers.removeNullsAndFlattenMap(mapper.convertValue(match, Map.class), "");
        firebaseDb.child(ref).child(Db.MATCHES).child(String.valueOf(match.getId())).updateChildren(flattenMap, logListener);
        saveToLocalDB(match);
    }

    /**
     * This method is just for testing purposes
     */
    public void findMatch(String path, String team, Consumer<String> consumer) {
        firebaseDb.child("soccer/matches").orderByChild(path).equalTo(team)
            .addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (DataSnapshot item : snapshot.getChildren()) {
                        stringBuilder.append(item.getKey()).append(", ");
                    }
                    consumer.accept(stringBuilder.toString());
                }

                @Override
                public void onCancelled(DatabaseError error) {
                }
            });
    }

    void removeMatch(String matchId, String ref) {
        firebaseDb.child(ref).child(Db.MATCHES).child(matchId).setValue(null, logListener);
    }
}
