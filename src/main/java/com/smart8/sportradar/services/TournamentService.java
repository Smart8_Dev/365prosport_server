package com.smart8.sportradar.services;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.FbTournament;
import com.smart8.sportradar.converters.stats.TournamentToFbTournamentConverter;
import com.smart8.sportradar.statisticsmodels.Category;
import com.smart8.sportradar.statisticsmodels.Sport;
import com.smart8.sportradar.statisticsmodels.SportradarData;
import com.smart8.sportradar.statisticsmodels.Tournament;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class TournamentService {

    @Autowired
    private FirebaseService fbService;

    @Autowired
    private TournamentToFbTournamentConverter converter;

    public void processTournaments(SportradarData sportradarData){
        final List<Sport> list = sportradarData.getSport();

        for (Sport sport : list) {
            parseSoccerTournaments(sport);
        }
    }

    private void parseSoccerTournaments(Sport sport) {

        SportType sportType;
        try{
            sportType = SportType.findById(sport.getId());
        }catch (UnsupportedOperationException ex){
            log.error("Sport with ID:{} not allowed", sport.getId());
            return;
        }

        final List<Category> list = sport.getCategory();

        Map<String, FbTournament> tournamentMap = new HashMap<>();

        for (Category category : list) {
            final List<Tournament> tournaments = category.getTournament();

            for (Tournament tournament : tournaments) {
                if(sportType == SportType.Soccer && !fbService.isAllowed(String.valueOf(tournament.getUniqueTournamentId()))) {
                    log.info("Tournament is not allowed: {}", tournament.getUniqueTournamentId());
                    continue;
                }

                FbTournament fbTournament = converter.convertTournamentToFbTournament(category, tournament);
                tournamentMap.put(fbTournament.getId(), fbTournament);
            }
        }

        fbService.saveTournaments(tournamentMap, sportType.name().toLowerCase());
    }
}
