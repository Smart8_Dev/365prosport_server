package com.smart8.sportradar.services;

import com.smart8.sportradar.firebase.FbTeam;
import com.smart8.sportradar.converters.stats.TeamToFbTeamConverter;
import com.smart8.sportradar.reports.TeamMappingReport;
import com.smart8.sportradar.statisticsmodels.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for parsing Sportradar team feed
 */
@Service
@Slf4j
public class TeamService {

    @Autowired
    private FirebaseService fbService;

    @Autowired
    private TeamToFbTeamConverter teamConverter;

    @Autowired
    TeamMappingReport report;

    public void processTeams(SportradarData sportradarData) {

        final List<Sport> list = sportradarData.getSport();
        String fileName = sportradarData.getFileName();

        for (Sport sport : list) {

            final String sportName = sport.getName().toLowerCase();
            List<FbTeam> missingItems = new ArrayList<>();
            final List<Category> categoryList = sport.getCategory();
            for (Category category : categoryList) {

                final List<Tournament> tournaments = category.getTournament();
                for (Tournament tournament : tournaments) {
                    final List<Team> teams = tournament.getTeams().getTeam();

                    for (Team radarTeam : teams) {
                        parseGeneralTeam(radarTeam, sportName, missingItems);
                    }
                }
            }

            if (missingItems.size() > 0) {
                report.writeReport(missingItems, "/home/nodeuser/sportradar/missingData/missing-teams-" + fileName + ".txt");
            }
        }
    }



    private void parseGeneralTeam(Team radarTeam, String ref,  List<FbTeam> missingItems) {
        FbTeam fbTeam = teamConverter.createFbTeam(radarTeam);
        log.info("@parseGeneralTeam > creating new {} team: {}:{}",ref, radarTeam.getId(), radarTeam.getName());
        fbService.saveGeneralTeam(fbTeam, ref);
        missingItems.add(fbTeam);
    }
}
