package com.smart8.sportradar.services;

import com.smart8.sportradar.enums.MatchEventCounter;
import com.smart8.sportradar.firebase.FbBet;
import com.smart8.sportradar.firebase.FbFriendRequest;
import com.smart8.sportradar.firebase.FbMatch;

public interface NotificationService {
    void notifyMatch(FbMatch fbMatch, MatchEventCounter type);
    void notifyFriendRequest(FbFriendRequest request);
    void notifyBetResult(FbBet bet, FbMatch fbMatch);
}
