package com.smart8.sportradar.services;

import com.smart8.sportradar.converters.stats.DartsStatMatchConverter;
import com.smart8.sportradar.converters.stats.SoccerStatMatchConverter;
import com.smart8.sportradar.converters.stats.StatMatchConverter;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.statisticsmodels.*;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MatchDetailService {

    @Autowired
    FirebaseService firebaseService;

    @Autowired
    BetResolveService betResolveService;

    @Autowired
    SoccerStatMatchConverter soccerConverter;

    @Autowired
    DartsStatMatchConverter dartsConverter;

    public void handleMatchDetails(SportradarData source){
        final Sport sport = source.getSport().get(0);
        SportType sportType;
        try{
            sportType = SportType.findById(sport.getId());
        }catch (UnsupportedOperationException ex){
            log.error("Sport with ID:{} not allowed", sport.getId());
            return;
        }
        StatMatchConverter matchConverter;
        if ( sportType == SportType.Darts){
            matchConverter = dartsConverter;
        }else{
            matchConverter = soccerConverter;
        }

        final List<Category> categoryList = sport.getCategory();
        if (MyCollections.isNotEmpty(categoryList)){
            for (Category category: categoryList){
                for (Tournament tournament : category.getTournament()){

                    if ( !firebaseService.isAllowed(String.valueOf(tournament.getUniqueTournamentId()))){
                        log.info("Tournament id:{}  is not allowed.", tournament.getUniqueTournamentId());
                        continue;
                    }
                    for( Match match :  tournament.getMatches().getMatch()){
                        FbMatch fbMatch = matchConverter.convert(tournament, match);
                        firebaseService.saveMatch(fbMatch, sportType.name().toLowerCase());
                        betResolveService.resolve(fbMatch);
                    }
                }
            }
        }
    }
}
