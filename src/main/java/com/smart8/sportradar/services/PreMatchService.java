package com.smart8.sportradar.services;

import com.smart8.sportradar.converters.stats.DartsStatMatchConverter;
import com.smart8.sportradar.converters.stats.SoccerStatMatchConverter;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.statisticsmodels.*;
import com.smart8.sportradar.utils.ConverterHelpers;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PreMatchService {

    @Autowired
    FirebaseService firebaseService;

    @Autowired
    SoccerStatMatchConverter soccerConverter;

    @Autowired
    DartsStatMatchConverter dartsConverter;

    public void handlePreMatchGame(SportradarData source) {
        log.debug("Handle prematch feed");
        final Sport sport = source.getSport().get(0);

        SportType sportType;
        try {
            sportType = SportType.findById(sport.getId());
        } catch (UnsupportedOperationException ex) {
            log.error("Sport ID:{} not allowed", sport.getId());
            return;
        }

        final List<Category> categoryList = sport.getCategory();

        if (MyCollections.isNotEmpty(categoryList)) {
            final Category category = categoryList.get(0);
            final Tournament tournament = category.getTournament().get(0);
            final long tournamentId = ConverterHelpers.getTournamentId(tournament);

            if (sportType == SportType.Soccer && !firebaseService.isAllowed(String.valueOf(tournamentId))) {
                log.info("Tournament id:{}  is not allowed.", tournamentId);
                return;
            }

            final Match match = tournament.getMatches().getMatch().get(0);
            FbMatch fbMatch;
            if ( sportType == SportType.Darts){
                fbMatch = dartsConverter.convert(tournament, match);
            }else{
                fbMatch = soccerConverter.convert(tournament, match);
            }

            firebaseService.saveMatch(fbMatch, sportType.name().toLowerCase());
        }
    }
}
