package com.smart8.sportradar.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.Constants;
import com.smart8.sportradar.converters.SoccerConverter;
import com.smart8.sportradar.enums.CriterionType;
import com.smart8.sportradar.firebase.FbBet;
import com.smart8.sportradar.firebase.FbDartsMatch;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.models.MatchBets;
import com.smart8.sportradar.repositories.NotificationRepository;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Slf4j
@Service
public class BetResolveService {

    private Logger betResolveLog = LoggerFactory.getLogger("bet-resolver");

    private static final int HALF_1 = 1;
    private static final int HALF_2 = 2;
    private static final int FULL = 0;

    private ObjectMapper mapper;

    @Autowired
    private NotificationRepository repository;

    @Autowired
    private FbListenerService fbservice;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    BetResolveService(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    private Map<String, MatchBets> matchBetsMap = new HashMap<>();

    void clear() {
        matchBetsMap.clear();
    }

    public synchronized void resolve(FbMatch match) {
        MatchBets matchBets = matchBetsMap.get(match.getId());

        if (matchBets == null) {
            log.debug("No bets on match {} found. ", match.getId());
            return;
        }
        if ( match instanceof FbSoccerMatch){
            resolveSoccerBets( (FbSoccerMatch) match, matchBets );
        }else if (  match instanceof FbDartsMatch){
           resolveDartsBets( (FbDartsMatch) match, matchBets );
        }
    }

    private void resolveDartsBets(FbDartsMatch match, MatchBets matchBets) {
        if (match.getStatusCode() == StatusCode.Ended.statusCode) {
            resolveMatchOutcome( match, matchBets);
        }
    }

    private void resolveSoccerBets(FbSoccerMatch match, MatchBets matchBets){

         if (StatusCode.isFirstHalfEnded(match.getStatusCode())) {
             resolvePeriodWin(CriterionType.HALF_TIME, match, matchBets, HALF_1);
             resolvePeriodWinDrawNoBet(CriterionType.DRAW_NO_BET_1H, match, matchBets, HALF_1);
         }

         if (StatusCode.isSecondHalfEnded(match.getStatusCode())){
             resolvePeriodWin(CriterionType.FULL_TIME, match, matchBets, FULL);
             resolvePeriodWinDrawNoBet(CriterionType.DRAW_NO_BET, match, matchBets, FULL);

             resolvePeriodWin(CriterionType.SECOND_HALF, match, matchBets, HALF_2);
             resolvePeriodWinDrawNoBet(CriterionType.DRAW_NO_BET_2H, match, matchBets, HALF_2);

             resolveLastGoal(match, matchBets);
             resolveFirstGoal(match, matchBets);
         }
     }

    private void resolveMatchOutcome(FbDartsMatch match, MatchBets matchBets) {
        if (!repository.incrementMatchBet(match.getId(), CriterionType.MATCH_OUTCOME.name())) {
            log.debug("Bets {}  are already resolved for match {}", CriterionType.MATCH_OUTCOME, match.getId());
            return;
        }
        log.debug("Resolving {} bets for match {}", CriterionType.MATCH_OUTCOME.name(), match.getId());
        Set<FbBet> betList = matchBets.getCriterionBets(CriterionType.MATCH_OUTCOME);
        betList.forEach(bet -> {
            if (userWon( whoWonByScore(match), bet.getOn())) {
                handleUserBetWin(bet, match);
            } else {
                handleUserBetLose(bet, match);
            }
            logBet(bet);
        });
        matchBets.removeCriterionBets(CriterionType.MATCH_OUTCOME);
    }

    private void resolveLastGoal(FbSoccerMatch match, MatchBets matchBets) {
        if (!repository.incrementMatchBet(match.getId(), CriterionType.LAST_GOAL_NG_NB.name())) {
            log.debug("Bets {}  are already resolved for match {}", CriterionType.LAST_GOAL_NG_NB, match.getId());
            return;
        }
        log.debug("Resolving {} bets for match {}", CriterionType.LAST_GOAL_NG_NB.name(), match.getId());
        Set<FbBet> betList = matchBets.getCriterionBets(CriterionType.LAST_GOAL_NG_NB);

        betList.forEach(bet -> handleUserBetWithNoBet(whoScoredLast(match), bet, match));
        matchBets.removeCriterionBets( CriterionType.LAST_GOAL_NG_NB);
    }

    private void resolvePeriodWin(CriterionType criterionType, FbSoccerMatch match, MatchBets allBets, int period) {
        if (!repository.incrementMatchBet(match.getId(), criterionType.name())) {
            log.debug("Bets {}  are already resolved for match {}", criterionType.name(), match.getId());
            return;
        }
        log.debug("Resolving {} bets for match {}", criterionType.name(), match.getId());
        Set<FbBet> betList = allBets.getCriterionBets(criterionType);
        betList.forEach(bet -> {
            if (userWon(SoccerConverter.whoWonPeriod(match, period), bet.getOn())) {
                handleUserBetWin(bet, match);
            } else {
                handleUserBetLose(bet, match);
            }
            logBet(bet);
        });
        allBets.removeCriterionBets(criterionType);
    }

    private void resolvePeriodWinDrawNoBet(CriterionType criterionType, FbSoccerMatch match, MatchBets allBets, int period) {
        if (!repository.incrementMatchBet(match.getId(), criterionType.name())) {
            log.debug("Bet {} are already resolved for match {}", criterionType.name(), match.getId());
            return;
        }
        log.debug("Resolving {} bets for match {}", criterionType, match.getId());
        Set<FbBet> betList = allBets.getCriterionBets(criterionType);
        betList.forEach(bet -> handleUserBetWithNoBet(SoccerConverter.whoWonPeriod(match, period), bet, match));

        allBets.removeCriterionBets(criterionType);
    }

    private void resolveFirstGoal(FbSoccerMatch match, MatchBets matchBets){
        if (!repository.incrementMatchBet(match.getId(), CriterionType.FIRST_GOAL_NG_NB.name())) {
            log.debug("Bet {} are already resolved for match {}", CriterionType.FIRST_GOAL_NG_NB.name(), match.getId());
            return;
        }
        log.debug("Resolving {} bets for match {}", CriterionType.FIRST_GOAL_NG_NB, match.getId());
        Set<FbBet> betList = matchBets.getCriterionBets(CriterionType.FIRST_GOAL_NG_NB);

        betList.forEach(bet -> handleUserBetWithNoBet(whoScoredFirst( match), bet, match));
        matchBets.removeCriterionBets(CriterionType.FIRST_GOAL_NG_NB);
    }

    private int whoWonByScore(FbDartsMatch match) {
        long homeId;
        long awayId;
        if ( match.getContestants().get(0).getPosition().equals("1")){
            homeId = match.getContestants().get(0).getId();
            awayId = match.getContestants().get(1).getId();
        }else{
            homeId = match.getContestants().get(1).getId();
            awayId = match.getContestants().get(0).getId();
        }
        Map<String, Integer> score = match.getLiveData().getScore();
        return  score.get(String.valueOf(homeId)).compareTo(score.get(String.valueOf(awayId)));
    }


    private int whoScoredFirst(FbSoccerMatch match) {
        if ( match.getLiveData() != null && MyCollections.isNotEmpty(match.getLiveData().getGoals())){
            long homeId;
            if ( match.getContestants().get(0).getPosition().equalsIgnoreCase(Constants.HOME)){
                homeId = match.getContestants().get(0).getId();
            }else{
                homeId = match.getContestants().get(1).getId();
            }
            List<FbSoccerMatch.Goal> goals = match.getLiveData().getGoals();
            if ( goals.get(0).getContestantId() == homeId){
                return 1;
            }else {
                return -1;
            }
        }
        return 0;
    }

    /**
     *  Home scores last - return 1, away scores last - return -1  else 0;
     */
    private int whoScoredLast(FbSoccerMatch match) {
        if ( match.getLiveData() != null && MyCollections.isNotEmpty(match.getLiveData().getGoals())){
            long homeId;
            if ( match.getContestants().get(0).getPosition().equalsIgnoreCase(Constants.HOME)){
                homeId = match.getContestants().get(0).getId();
            }else{
                homeId = match.getContestants().get(1).getId();
            }
            List<FbSoccerMatch.Goal> goals = match.getLiveData().getGoals();
            if ( goals.get(goals.size()-1).getContestantId() == homeId){
                return 1;
            }else {
                return -1;
            }
        }
        return 0;
    }

    private void handleUserBetWithNoBet(int betResult, FbBet bet, FbSoccerMatch fbMatch) {
        if ( userWonNoDraw(betResult, bet.getOn()) ){
            handleUserBetWin(bet, fbMatch);
        }else if (betResult == 0){
            handleUserBetReturn(bet, fbMatch);
        } else {
            handleUserBetLose(bet, fbMatch);
        }
        logBet(bet);
    }

    private void handleUserBetWin(FbBet bet, FbMatch fbMatch) {
        log.debug("User '{}' Bet '{}' Wins", bet.getUserId(), bet.getId());
        bet.setStatus(FbBet.Status.Win);

        fbservice.updateBetStatus(bet.getId(), FbBet.Status.Win);
        int addedCoins = (int) Math.round(bet.getOdds() * bet.getStake());
        fbservice.updateUserCoins(bet.getUserId(), addedCoins, true);
        notificationService.notifyBetResult( bet, fbMatch);
    }

    private void handleUserBetReturn(FbBet bet, FbSoccerMatch fbMatch) {
        log.debug("No bet. User '{}' Bet '{}' returned", bet.getUserId(), bet.getId());
        bet.setStatus(FbBet.Status.NoBet);
        fbservice.updateBetStatus(bet.getId(), FbBet.Status.NoBet);
        fbservice.updateUserCoins(bet.getUserId(), bet.getStake(), false);
        notificationService.notifyBetResult(bet, fbMatch);
    }

    private void handleUserBetLose(FbBet bet, FbMatch fbMatch) {
        log.debug("User '{}' Bet '{}' lost", bet.getUserId(), bet.getId());
        bet.setStatus(FbBet.Status.Lose);
        fbservice.updateBetStatus(bet.getId(), FbBet.Status.Lose);
        notificationService.notifyBetResult( bet, fbMatch);
    }


    void addBetToMatch(String matchId, FbBet fbBet){
        MatchBets matchBets = matchBetsMap.computeIfAbsent(matchId, s -> new MatchBets());
        Optional<CriterionType> opt = CriterionType.findById(fbBet.getCriterionId());
        opt.ifPresent(type -> matchBets.addBet(type, fbBet));
    }

    void removeBetFromMatch(String matchId, FbBet fbBet) {
        MatchBets matchBets = matchBetsMap.get(matchId);
        if (matchBets != null) {
            Optional<CriterionType> opt = CriterionType.findById(fbBet.getCriterionId());
            opt.ifPresent(type -> matchBets.removeBet(type, fbBet));
        }
    }

    Set<String> getBetUsersByMatchId(String matchId) {
        MatchBets bets = matchBetsMap.get(matchId);
        if (bets != null) {
            return bets.getAllUsers();
        }
        return Collections.emptySet();
    }

    private boolean userWon(int result, FbBet.Outcome betOutcome) {
        return result == 1 && betOutcome == FbBet.Outcome.OT_ONE
            || result == -1 && betOutcome == FbBet.Outcome.OT_TWO
            || result == 0 && betOutcome == FbBet.Outcome.OT_CROSS;
    }

    private boolean userWonNoDraw(int result, FbBet.Outcome betOutcome) {
        return result == 1 && betOutcome == FbBet.Outcome.OT_ONE
            || result == -1 && betOutcome == FbBet.Outcome.OT_TWO;
    }

    private void logBet(FbBet bet) {
        try {
            String betStr = mapper.writeValueAsString(bet);
            betResolveLog.info("Bet resolved {}. {}", bet.getStatus(), betStr);
        } catch (IOException e) {
            log.error("Error while creating bet resolver report");
        }
    }
}
