package com.smart8.sportradar.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.smart8.sportradar.converters.NotificationCreator;
import com.smart8.sportradar.enums.MatchEventCounter;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.firebase.*;
import com.smart8.sportradar.models.FcmMessage;
import com.smart8.sportradar.repositories.FavoritesRepository;
import com.smart8.sportradar.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@Slf4j
@Service
public class NotificationServiceFcm implements NotificationService {

    private Logger notifyLog = LoggerFactory.getLogger("notification");

    @Value("${firebase.cloudServerKey}")
    private String cloudServerKey;

    private static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";

    @Autowired
    private FavoritesRepository favoritesRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FbListenerService fbListenerService;

    @Autowired
    private BetResolveService betResolverService;

    @Autowired
    NotificationCreator notificationCreator;

    @Override
    public void notifyMatch(FbMatch fbMatch, MatchEventCounter notifyType) {
        final String matchId = fbMatch.getId();
        log.info("Try send notification {} for match '{}'", notifyType, matchId);

        Map<String, Set<String>> userByLocaleMap = getUsersToNotify(notifyType, fbMatch);

        for (Map.Entry<String, Set<String>> userSet : userByLocaleMap.entrySet()) {
            List<String> tokens = userRepository.getUsersTokenList(userSet.getValue());

            Locale locale = new Locale(userSet.getKey());

            if (tokens.isEmpty()) {
                log.info("Found 0 tokens for users with locale {}. Nobody will see this message.", locale);
                continue;
            }

            FcmMessage notification = notificationCreator.buildMatchNotification(notifyType, fbMatch, locale);

            int count = 0;
            final int MAX = 1000; //Can send only 1000 messages at once.
            List<String> subList;
            int left = tokens.size();

            while (left > 0) {
                if (left > MAX) {
                    subList = tokens.subList(count, count + MAX);
                } else {
                    subList = tokens.subList(count, tokens.size());
                }
                notification.setRegistration_ids(subList);
                log.debug("Sending to tokens: {}", subList);

                if (sendNotification(notification)) {
                    if (notifyType == MatchEventCounter.Finished) {
                        fbListenerService.removeFavoriteMatchListener(fbMatch);
                        favoritesRepository.removeFavorites(Db.MATCHES, Long.parseLong(matchId));
                    }
                }
                count += MAX;
                left -= subList.size();
            }
        }
    }

    @Override
    public void notifyFriendRequest(FbFriendRequest request) {
        String recipientId = null;
        if (request.getStatus() == FbFriendRequest.InviteStatus.REQUESTED) {
            recipientId = request.getInvitedId();
        } else if (request.getStatus() == FbFriendRequest.InviteStatus.ACCEPTED) {
            recipientId = request.getInviterId();
        }

        String token = userRepository.getUserToken(recipientId);
        if (token == null) {
            log.warn("No token found for users {} or {}. No notification will be sent.", request.getInvitedId(), request.getInviterId());
            return;
        }

        Locale locale = new Locale(fbListenerService.getUserLocaleStr(recipientId));
        FcmMessage notification = notificationCreator.buildFriendRequestNotification(token, request, locale);

        log.debug("Sending notification to {}:{}. Notification: {}", recipientId, token, notification);
        sendNotification(notification);
    }

    @Override
    public void notifyBetResult(FbBet bet, FbMatch fbMatch) {
        if (!userRepository.isUserNotificationAllowed(bet.getUserId(), FbUserProfile.NotificationType.bets)) {
            notifyLog.debug("User {} don't want to see bets notifications", bet.getUserId());
            return;
        }
        notifyLog.debug("Sending Bet notification to user '{}' that betid '{}' is resolved", bet.getUserId(), bet.getId());
        String token = userRepository.getUserToken(bet.getUserId());
        if (token == null) {
            notifyLog.warn("No token for user {} found. No notification will be sent.", bet.getUserId());
            return;
        }
        Locale locale = new Locale(fbListenerService.getUserLocaleStr(bet.getUserId()));
        FcmMessage notification = notificationCreator.buildBetNotification(token, bet, fbMatch, locale);
        sendNotification(notification);
    }

    private Map<String, Set<String>> getUsersToNotify(MatchEventCounter notifyType, FbMatch fbMatch) {
        Set<String> userIdSet = favoritesRepository.getUsersByFavorites(Db.MATCHES, Long.parseLong(fbMatch.getId()));
        userIdSet.addAll(favoritesRepository.getUsersByFavorites(Db.TOURNAMENTS, fbMatch.getTournamentId()));
        for (Contestant contestant : fbMatch.getContestants()) {
            userIdSet.addAll(favoritesRepository.getUsersByFavorites(Db.TEAMS, contestant.getId()));
        }

        Set<String> betUsers = betResolverService.getBetUsersByMatchId(fbMatch.getId());
        log.debug("{} users bet on match '{}'", betUsers.size(), fbMatch.getId());
        userIdSet.addAll(betUsers);

        log.info("Notification shoud be sent to {} users", userIdSet.size());
        return filterRecipientsBySettings(userIdSet, notifyType);
    }

    /**
     * Filter users by there settings and split by locale
     */
    private Map<String, Set<String>> filterRecipientsBySettings(Set<String> userIdList, MatchEventCounter notifyType) {
        log.debug("Filter {} users.", userIdList.size());
        Map<String, Set<String>> userIdByLocaleMap = new HashMap<>();
        for (String userId : userIdList) {
            if (doesUserWantNotification(userId, notifyType)) {
                String localeStr = fbListenerService.getUserLocaleStr(userId);
                Set<String> set = userIdByLocaleMap.computeIfAbsent(localeStr, item -> new HashSet<>());
                set.add(userId);
            }
        }

        return userIdByLocaleMap;
    }

    private boolean doesUserWantNotification(String userId, MatchEventCounter notifyType) {
        switch (notifyType) {
            case Lineup:
                return userRepository.isUserNotificationAllowed(userId, FbUserProfile.NotificationType.lineups);
            case SoonStart:
                return userRepository.isUserNotificationAllowed(userId, FbUserProfile.NotificationType.prepare);
            case Goal:
                return userRepository.isUserNotificationAllowed(userId, FbUserProfile.NotificationType.goals);
            case Penalty:
                return userRepository.isUserNotificationAllowed(userId, FbUserProfile.NotificationType.penalties);
            case Card:
                return userRepository.isUserNotificationAllowed(userId, FbUserProfile.NotificationType.cards);
            default:
                return true;
        }
    }



    private boolean sendNotification(FcmMessage notification) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            String notifyStr = mapper.writeValueAsString(notification);
            return sendRequest(notifyStr);
        } catch (JsonProcessingException e) {
            log.error("Error on parsing notification: {}", notification);
        }
        return false;
    }

    private boolean sendRequest(String jsonString) {
        try {
            URL url = new URL(FCM_URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", "key=" + cloudServerKey);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.writeBytes(jsonString);
            writer.flush();
            writer.close();

            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                log.error("Google fcm response is {}. Failed send notification", responseCode);
                notifyLog.error("Google fcm response is {}. Error message: {}, Failed  sending: {}", responseCode, connection.getResponseMessage(), jsonString);
                return false;
            } else {
                notifyLog.info("Notification sent. {}", jsonString);
                log.info("Notification sent!.");
                return true;
            }
        } catch (IOException ex) {
            log.error("Error on sending notification to Google Fcm");
            return false;
        }
    }

}
