package com.smart8.sportradar.wsclient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.messaging.simp.stomp.*;

import java.lang.reflect.Type;

@Slf4j
public class MySessionHandler extends StompSessionHandlerAdapter {

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        log.info("New Session: {}", session.getSessionId());
    }

    @Override
    public void handleException(StompSession session, @Nullable StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        log.error("Error in session", exception);
    }

    @Override
    public void handleFrame(StompHeaders headers, @Nullable Object payload) {
        log.info("Received: {}", payload);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return String.class;
    }
}
