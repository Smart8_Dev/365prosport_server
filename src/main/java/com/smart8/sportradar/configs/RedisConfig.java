package com.smart8.sportradar.configs;

import com.smart8.sportradar.handlers.CancelledMatchHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import static com.smart8.sportradar.repositories.MatchRepository.TOPIC_CANCELLED;

@SpringBootConfiguration
public class RedisConfig {

    @Autowired
    RedisConnectionFactory factory;

    @Bean
    public StringRedisTemplate redisTemplate( ) {
        final StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(factory);
        return redisTemplate;
    }

    @Bean
    RedisMessageListenerContainer container( RedisConnectionFactory factory, MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        container.addMessageListener(listenerAdapter, new PatternTopic(TOPIC_CANCELLED));
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(CancelledMatchHandler handler) {
        return new MessageListenerAdapter(handler, "receiveMessage");
    }
}
