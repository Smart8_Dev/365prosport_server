package com.smart8.sportradar.configs;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.smart8.sportradar.firebase.Db;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

@Profile("!test")
@SpringBootConfiguration
@Slf4j
public class FireBaseConfig {

    @Value("${firebase.url}")
    private String fireBaseUrl;

    @Value("${firebase.configPath}")
    private String fireBaseConfig;

    @Autowired
    ResourceLoader resourceLoader;

    @PostConstruct
    public void init(){
        Resource resource = resourceLoader.getResource("classpath:" + fireBaseConfig);
        try (InputStream serviceAccount = resource.getInputStream()) {
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredential(FirebaseCredentials.fromCertificate(serviceAccount))
                .setDatabaseUrl(fireBaseUrl)
                .build();
            FirebaseApp.initializeApp(options);
            log.info("Firebase initialized. FB url: {}", fireBaseUrl);
        } catch (IOException e) {
            log.error("error Firebase", e);
        }

    }

    @Bean
    public DatabaseReference fireBaseRef(){
        return FirebaseDatabase.getInstance().getReference();
    }

    @Bean
    public DatabaseReference news(){
        return FirebaseDatabase.getInstance().getReference().child(Db.NEWS);
    }

    @Bean
    public DatabaseReference.CompletionListener logOnCompletionListener(){
        return (error, ref) -> {
            if(error != null){
                log.warn("Error on saving data: {}", error.getMessage());
            }
            else{
                log.info("Saved data to key {}", ref.getKey());
            }
        };
    }
}
