package com.smart8.sportradar.handlers;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.repositories.MatchRepository;
import com.smart8.sportradar.services.FbListenerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.smart8.sportradar.repositories.MatchRepository.TOPIC_CANCELLED;

@Slf4j
@Component
public class CancelledMatchHandler {

    @Autowired
    private MatchRepository matchRepository;

    private FbListenerService fbListenerService;

    private static final String STRING_SPLIT = ":";

    @Autowired
    public CancelledMatchHandler(MatchRepository repo, FbListenerService fbService){
        this.matchRepository = repo;
        this.fbListenerService = fbService;
    }

    public void sendMatchCancelledMessage(SportType sport, String matchId) {
        log.debug("Match {} is cancelled.", matchId);
        matchRepository.sendCancelledMessage(sport.name().toLowerCase() + STRING_SPLIT + matchId);
    }

    @SuppressWarnings("unused")
    public void receiveMessage(String message, String topic) {
        if (topic.equals(TOPIC_CANCELLED)){
            String[] strings = message.split(STRING_SPLIT);
            log.info("Return bets for cancelled match ID {}", strings[1]);
            fbListenerService.returnMatchBets(strings[0], strings[1], true );
        }
    }
}
