package com.smart8.sportradar.handlers;


import com.smart8.sportradar.Constants;
import com.smart8.sportradar.converters.SoccerConverter;
import com.smart8.sportradar.enums.MatchEventCounter;
import com.smart8.sportradar.firebase.FbDartsMatch;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.repositories.NotificationRepository;
import com.smart8.sportradar.services.FbListenerService;
import com.smart8.sportradar.services.NotificationService;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class MatchEventsHandler {

    private static final int PRESTART_NOTIFY_TIME_MS = 15 * 60 * 1000;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private FbListenerService fbListenerService;

    @Autowired
    private NotificationService notificationService;

    public synchronized void handleMatchEvents(FbMatch fbMatch) {
        if (!fbListenerService.isMatchObservable(fbMatch.getId())) {
            log.debug("Match:{} is not observable.", fbMatch.getId());
            return;
        }

        checkPreMatchEvents(fbMatch);

        if (!checkMatchStarted(fbMatch)) {
            return;   //Only Lineups or StartSoon notifications can be sent before match started
        }
        if (checkMatchFinished(fbMatch)) {
            return;
        }

        if (fbMatch instanceof FbDartsMatch) {
            checkMatchStartPeriod(fbMatch, MatchEventCounter.Started, StatusCode.Started.statusCode);
            checkDartsMatchScore((FbDartsMatch) fbMatch);

        } else if (fbMatch instanceof FbSoccerMatch) {
            checkMatchStartPeriod(fbMatch, MatchEventCounter.StartFirst, StatusCode.FirstHalf.statusCode);
            checkMatchStartPeriod(fbMatch, MatchEventCounter.StartSecond, StatusCode.SecondHalf.statusCode);
            checkMatchStartPeriod(fbMatch, MatchEventCounter.Ht, StatusCode.Halftime.statusCode);
            FbSoccerMatch soccerMatch = (FbSoccerMatch)fbMatch;
            checkMatchGoals(soccerMatch);
            checkMatchCards(soccerMatch);
        }
    }

    private void checkPreMatchEvents(FbMatch fbMatch) {
        if (fbMatch instanceof FbSoccerMatch) {
            checkMatchLineups((FbSoccerMatch) fbMatch);
        }
    }

    private void checkMatchCards(FbSoccerMatch fbMatch) {
        if (fbMatch.getLiveData() == null || fbMatch.getLiveData().getCards() == null) {
            return;
        }
        int sentBefore = sentBefore(fbMatch.getId(), MatchEventCounter.Card);

        if (fbMatch.getLiveData().getCards().size() > sentBefore) {
            FbSoccerMatch.Card card = fbMatch.getLiveData().getCards().get(fbMatch.getLiveData().getCards().size() - 1);
            if (card.getType().equals(SoccerConverter.R_CARD)) {
                notificationService.notifyMatch(fbMatch, MatchEventCounter.Card);
                notificationRepository.setMatchNotification(fbMatch.getId(), MatchEventCounter.Card, String.valueOf(sentBefore + 1));
            } else {
                log.debug("Card type is {}. No notification should be send but counter '{}' incremented by 1", card.getType(), sentBefore);
                notificationRepository.setMatchNotification(fbMatch.getId(), MatchEventCounter.Card, String.valueOf(sentBefore + 1));
            }
        }
    }

    private void checkMatchGoals(FbSoccerMatch fbMatch) {
        if (fbMatch.getLiveData() == null || fbMatch.getLiveData().getGoals() == null) {
            return;
        }
        int sentBefore = sentBefore(fbMatch.getId(), MatchEventCounter.Goal);

        if (fbMatch.getLiveData().getGoals().size() > sentBefore) {
            notificationService.notifyMatch(fbMatch, MatchEventCounter.Goal);
            notificationRepository.setMatchNotification(fbMatch.getId(), MatchEventCounter.Goal, String.valueOf(sentBefore + 1));
        }
    }

    private void checkDartsMatchScore(FbDartsMatch fbMatch) {

        if (fbMatch.getLiveData() == null || MyCollections.isEmpty((fbMatch.getLiveData()).getSets())) {
            return;
        }
        FbDartsMatch.DartsLiveData liveData = fbMatch.getLiveData();
        int sentSetsBefore = sentBefore(fbMatch.getId(), MatchEventCounter.Sets);
        int currentSet = liveData.getSets().size();
        Map<String, Integer> gameScoreMap = liveData.getScore();
        int setsPlayed = gameScoreMap.values().stream().mapToInt(Number::intValue).sum();

        int sentLegsBefore = sentBefore(fbMatch.getId(), MatchEventCounter.Legs);
        Map<String, Integer> setScoreMap = liveData.getSets().get(currentSet - 1);
        int totalSetScore = setScoreMap.values().stream().mapToInt(Number::intValue).sum();

        /*
            Send legs notification when current set is not finished (setsPlayed < currentSet)
            OR current set is finished and sentLegs counter is not reset to Zero
            ( 2nd happens when set is finished and we have to notify about sets and legs only one time
                after this leg notification counter will be reset to zero
                amd no notification should be sent about already finished set)
         */
        if ( totalSetScore > sentLegsBefore &&  setsPlayed < currentSet
            || totalSetScore > sentLegsBefore && setsPlayed == currentSet && sentLegsBefore != 0) {
            log.debug("Set score changed. Legs played: {}", totalSetScore);
            notificationService.notifyMatch(fbMatch, MatchEventCounter.Legs);
            notificationRepository.setMatchNotification(fbMatch.getId(), MatchEventCounter.Legs, String.valueOf(sentLegsBefore + 1));

        }

        //Send notification about set when game score is changed
        if (setsPlayed > sentSetsBefore) {
            log.debug("Game score changed. Sending sets notification. Sets played: {}", setsPlayed);
            notificationService.notifyMatch(fbMatch, MatchEventCounter.Sets);
            notificationRepository.setMatchNotification(fbMatch.getId(), MatchEventCounter.Sets, String.valueOf(sentSetsBefore + 1));
            log.debug("Reset leg score counter for match {}", fbMatch.getId());
            notificationRepository.setMatchNotification(fbMatch.getId(), MatchEventCounter.Legs, "0");
        }
    }

    private boolean checkMatchFinished(FbMatch fbMatch) {
        if (fbMatch.getStatusCode() == StatusCode.Ended.statusCode) {
            int sentBefore = sentBefore(fbMatch.getId(), MatchEventCounter.Finished);
            if (sentBefore != -1) {
                notificationService.notifyMatch(fbMatch, MatchEventCounter.Finished);
                notificationRepository.setMatchNotification(fbMatch.getId(), MatchEventCounter.Finished, String.valueOf(sentBefore + 1));
            }
            return true;
        }
        return false;
    }


    private void checkMatchStartPeriod(FbMatch fbMatch, MatchEventCounter type, int statusCode) {
        if (fbMatch.getStatusCode() == statusCode) {
            int sentBefore = sentBefore(fbMatch.getId(), type);
            if (sentBefore != -1) {
                notificationService.notifyMatch(fbMatch, type);
                notificationRepository.setMatchNotification(fbMatch.getId(), type, String.valueOf(sentBefore + 1));
            }
        }
    }


    private void checkMatchLineups(FbSoccerMatch fbMatch) {

        if (fbMatch.getLiveData() == null || fbMatch.getLiveData().getLineup() == null) {
            return;
        }
        int sentBefore = sentBefore(fbMatch.getId(), MatchEventCounter.Lineup);

        if (sentBefore != -1) {
            List<String> players = new ArrayList<>();
            for (FbSoccerMatch.Lineup lineup : fbMatch.getLiveData().getLineup()) {
                if (lineup.getPlayers() != null) {
                    for (FbSoccerMatch.LineupPlayer player : lineup.getPlayers()) {
                        players.add(player.getLastName());
                    }
                }
            }
            if (!players.isEmpty()) {
                notificationService.notifyMatch(fbMatch, MatchEventCounter.Lineup);
                notificationRepository.setMatchNotification(fbMatch.getId(),  MatchEventCounter.Lineup, String.valueOf(sentBefore + 1));
            }
        }
    }

    private boolean checkMatchStarted(FbMatch fbMatch) {
        if (fbMatch.getStatusCode() == StatusCode.NotStarted.statusCode) {
            if (fbMatch.getDateTimeInt() - System.currentTimeMillis() > PRESTART_NOTIFY_TIME_MS) {
                return false;
            }
            int sentBefore = sentBefore(fbMatch.getId(), MatchEventCounter.SoonStart);
            if (sentBefore != -1) {
                notificationService.notifyMatch(fbMatch, MatchEventCounter.SoonStart);
                notificationRepository.setMatchNotification(fbMatch.getId(),  MatchEventCounter.SoonStart, String.valueOf(sentBefore + 1));
            }
            return false;
        }
        return true;
    }

    /**
     * Check the number of notification of @type that was sent before. If more than @type limit return -1.
     */
    private int sentBefore(String matchId, MatchEventCounter type) {
        String value = notificationRepository.getMatchNotification(matchId, type.name());
        log.debug("'{}' notification of type '{}' sent before for match {} ", value, type, matchId);
        if (value == null) {
            return 0;
        } else if (type.limit <= Integer.parseInt(value)) {
            log.debug("Number of '{}' notifications for match {} already more or equal {}.", type, matchId, value);
            return Constants.NO_VALUE;
        } else {
            return Integer.parseInt(value);
        }
    }
}
