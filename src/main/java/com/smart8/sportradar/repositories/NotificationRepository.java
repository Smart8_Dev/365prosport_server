package com.smart8.sportradar.repositories;

import com.smart8.sportradar.enums.MatchEventCounter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Slf4j
@Repository
public class NotificationRepository {

    private static final String MATCHES_NOTIFICATION = "matches:notification:";

    private StringRedisTemplate redisTemplate;

    private HashOperations<String, String, String> hashOps;

    @Autowired
    public NotificationRepository(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOps = redisTemplate.opsForHash();
    }

    /**
     * Get all types of notification for match
     */
    public Map<String, String> getMatchNotifications(String matchId){
        return hashOps.entries(MATCHES_NOTIFICATION + matchId);
    }

    public String getMatchNotification(String matchId, String notifyType){
        return hashOps.get(MATCHES_NOTIFICATION + matchId, notifyType);
    }

    public void setMatchNotification(String matchId, MatchEventCounter notifyType, String count){
        log.debug("Set notification '{}' value to {} for match id : {}", notifyType, count, matchId);
        hashOps.put(MATCHES_NOTIFICATION + matchId, notifyType.name(), count);
    }

    public boolean incrementMatchBet(String matchId, String notifyType) {
        return hashOps.putIfAbsent(MATCHES_NOTIFICATION + matchId, notifyType, "1");
    }

    /**
     * Only for testing. Remove afterwards
     */
    public void removeMatchBetCounter(String matchId) {
        log.debug("Reset match {} bets counter", matchId);
        redisTemplate.delete(MATCHES_NOTIFICATION);
    }


}
