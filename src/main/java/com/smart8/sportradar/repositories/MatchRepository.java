package com.smart8.sportradar.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.Constants;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.models.MiniMatch;
import com.smart8.sportradar.models.unibet.UniResult;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.smart8.sportradar.Constants.REDIS_MAX_VIEW_ITEMS;

@Slf4j
@Repository
public class MatchRepository {

    public static final String TOPIC_CANCELLED = "cancelled-match-topic";
    public static final String HASH_UNI_TO_RADAR_MATCH = "hash_uni_radar_m";
    public static final String HASH_RADAR_TO_UNI_MATCH = "hash_radar_uni_m";
    public static final String HASH_UNI_TEAMS = "uni_teams";
    public static final String HASH_RADAR_TEAMS = "radar_teams";
    public static final String HASH_UNI_TO_RADAR_TEAM = "hash_uni_radar_t";
    public static final String HASH_RADAR_TO_UNI_TEAM = "hash_radar_uni_t";
    public static final String HASH_RADAR_TO_UNI_TOURNAMENTS = "mapping:tournaments:radar-uni";
    public static final String HASH_MATCHES = "hash_matches";
    public static final String HASH_MATCHES_DATE = "hash_matches_date";
    public static final String SET_LIVE_MATCHES = "matches:live";
    public static final String UNMAPPED_UNI_EVENTS = "unievents:unmapped";
    public static final String UNMAPPED_MATCHES = "matches:unmapped";

    public static final int NO_VALUE = -1;

    private StringRedisTemplate redisTemplate;
    private HashOperations<String, String, String> hashOps;
    private SetOperations<String, String> setOps;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    public MatchRepository(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init() {
        hashOps = redisTemplate.opsForHash();
        setOps = redisTemplate.opsForSet();
    }

    public void storeMatchMapping(long unibetMatchId, String sportRadarMatchId) {
        hashOps.put(HASH_UNI_TO_RADAR_MATCH, String.valueOf(unibetMatchId), sportRadarMatchId);
        hashOps.put(HASH_RADAR_TO_UNI_MATCH, sportRadarMatchId, String.valueOf(unibetMatchId));
    }


    public long getUnibetMatchIdByRadarMatchId(String radarMatchId) {
        final String result = hashOps.get(HASH_RADAR_TO_UNI_MATCH, radarMatchId);
        return result != null ? Long.parseLong(result) : NO_VALUE;
    }

    public String getRadarMatchIdFromUnibetMatchId(long unibetMatchId) {
        return hashOps.get(HASH_UNI_TO_RADAR_MATCH, String.valueOf(unibetMatchId));
    }


    public long getRadarTeamIdFromUnibetTeamId(long unibetTeamId) {
        final String result = hashOps.get(HASH_UNI_TO_RADAR_TEAM, String.valueOf(unibetTeamId));
        return result != null ? Long.parseLong(result) : NO_VALUE;
    }

    /**
     * Find unibet team id by sportradar team Id
     */
    public long getUnibetTeamIdByRadarTeamId(long radarTeamId) {
        final String result = hashOps.get(HASH_RADAR_TO_UNI_TEAM, String.valueOf(radarTeamId));
        return result != null ? Long.parseLong(result) : NO_VALUE;
    }

    public void storeTeamMapping(long unibetId, String radarTeamId) {
        hashOps.put(HASH_RADAR_TO_UNI_TEAM, radarTeamId, String.valueOf(unibetId));
        hashOps.put(HASH_UNI_TO_RADAR_TEAM, String.valueOf(unibetId), radarTeamId);
    }

    public void storeUnibetTeam(long unibetId, String unibetName) {
        hashOps.put(HASH_UNI_TEAMS, String.valueOf(unibetId), unibetName);
    }

    public void storeRadarTeam(String radarId, String name) {
        hashOps.put(HASH_RADAR_TEAMS, radarId, name);
    }

    public Map<String, String> getRadarTeams() {
        return hashOps.entries(HASH_RADAR_TEAMS);
    }

    public Map<String, String> getUnibetTeams() {
        return hashOps.entries(HASH_UNI_TEAMS);
    }


    /**
     * Currently we save the match as json in redis. This is because calling firebase on each
     * bet is really expensive. This game info is minimal - the teams and the date
     *
     * @param s
     */
    public void storeRadarMatch(String matchId, String s) {
        hashOps.put(HASH_MATCHES, matchId, s);
    }

    public void storeMultipleMatches(Map<String, String> matches) {
        hashOps.putAll(HASH_MATCHES, matches);
    }

    public List<MiniMatch> getMatches(Collection<String> matchesId) {
        final List<String> strings = hashOps.multiGet(HASH_MATCHES, matchesId);

        return convertToMatch(strings);
    }

    public FbSoccerMatch.FbMiniMatch getMatch(String matchId) {
        final String s = hashOps.get(HASH_MATCHES, matchId);
        try {
            return s != null ? mapper.readValue(s, FbSoccerMatch.FbMiniMatch.class) : null;
        } catch (IOException e) {
            log.error("Error while mapping Minimatch {}", s);
            return null;
        }
    }


    public long getLatestGameForMatch() {
        final Set<ZSetOperations.TypedTuple<String>> typedTuples = redisTemplate.opsForZSet().rangeByScoreWithScores(HASH_MATCHES_DATE, Double.MAX_VALUE, 0, 0, 1);

        for (ZSetOperations.TypedTuple<String> typedTuple : typedTuples) {
            return typedTuple.getScore().longValue();
        }
        return Instant.now().toEpochMilli() + Constants.MILLIS_IN_MINUTE * 60 * 24 * 5;
    }

    private List<MiniMatch> convertToMatch(Collection<String> strings) {
        return strings.parallelStream()
            .map(s -> {
                try {
                    return mapper.readValue(s, MiniMatch.class);
                } catch (IOException e) {
                    return null;
                }
            }).filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    public List<MiniMatch> getMatchesAfterDate(long dateStart, long dateEnd) {
        final ZSetOperations<String, String> zSet = redisTemplate.opsForZSet();
        final Set<String> data = zSet.rangeByScore(HASH_MATCHES_DATE, dateStart, dateEnd);
        return getMatches(data);
    }

    public void addMatchDate(FbMatch match) {
        redisTemplate.opsForZSet().add(HASH_MATCHES_DATE, String.valueOf(match.getId()), match.getDateTimeInt());
    }

    public void addLiveMatches(List<? extends FbMatch> matches) {
        if (MyCollections.isNotEmpty(matches)) {
            setOps.add(SET_LIVE_MATCHES, matches.stream().map(FbMatch::getId).toArray(String[]::new));
        }
    }

    public List<MiniMatch> getLiveMatches() {
        Set<String> liveMatchIds = setOps.members(SET_LIVE_MATCHES);
        List<String> list = hashOps.multiGet(HASH_MATCHES, liveMatchIds);
        return list.stream().map(item -> {
            try {
                return mapper.readValue(item, MiniMatch.class);
            } catch (IOException e) {
                log.error("Error in parsing fbMatch. {}", e.getMessage());
            }
            return null;
        }).collect(Collectors.toList());
    }

    public void removeLiveMatches(List<? extends FbMatch> matches) {
        if (MyCollections.isNotEmpty(matches)) {
            final String[] values = matches.stream().map(FbMatch::getId).toArray(String[]::new);
            setOps.remove(SET_LIVE_MATCHES, (Object[]) values);
        }
    }

    public void addTournamentMapping(Map<Long, String> uniLeagueToRadarLeague) {

        for (Map.Entry<Long, String> entry : uniLeagueToRadarLeague.entrySet()) {
            hashOps.put(HASH_RADAR_TO_UNI_TOURNAMENTS, entry.getValue(), entry.getKey().toString());
        }
    }

    public String getUniTournamentIdByRadarId(String radarId) {
        return hashOps.get(HASH_RADAR_TO_UNI_TOURNAMENTS, radarId);
    }

    public List<MiniMatch> getUnmappedMatchesAfter(long afterDate) {
        final ZSetOperations<String, String> zSet = redisTemplate.opsForZSet();
        Set<String> data = zSet.rangeByScore(UNMAPPED_MATCHES, afterDate, Long.MAX_VALUE);
        return getMatches(data);
    }

    public Map<String, String> findUnmappedEvents(String pattern) {
        Map<String, String> resultMap = new HashMap<>();

        ScanOptions.ScanOptionsBuilder optionBuilder = new ScanOptions.ScanOptionsBuilder();
        optionBuilder.match(pattern + "*");
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(UNMAPPED_UNI_EVENTS, optionBuilder.build());
        int cnt = 0;
        while (cursor.hasNext()) {
            if (cnt >= REDIS_MAX_VIEW_ITEMS) {
                log.debug("List for pattern '{}' is to big. Cutting...", pattern);
                break;
            }
            Map.Entry<Object, Object> entry = cursor.next();
            try {
                UniResult.Event event = mapper.readValue((String) entry.getValue(), UniResult.Event.class);
                resultMap.put((String) entry.getKey(), event.getName());
            } catch (IOException e) {
                log.error("Error during UniResult.Event parsing. {}", e.getMessage());
            }
            cnt++;
        }

        log.debug("Founded {} results.", resultMap.size());

        try {
            cursor.close();
        } catch (IOException e) {
            log.error("Redis curser close error.");
        }
        return resultMap;
    }

    public void setUnmappedEvent(String eventId, String event) {
        hashOps.put(UNMAPPED_UNI_EVENTS, eventId, event);
    }

    public void removeUnmappedEvent(String unibetTeamId) {
        hashOps.delete(UNMAPPED_UNI_EVENTS, unibetTeamId);
    }

    public void removeUnmappedMatch(String matchId) {
        ZSetOperations<String, String> zsetOps = redisTemplate.opsForZSet();
        zsetOps.remove(UNMAPPED_MATCHES, matchId);
    }

    public void storeUnmappedMatch(FbMatch fbMatch) {
        ZSetOperations<String, String> zsetOps = redisTemplate.opsForZSet();
        zsetOps.add(UNMAPPED_MATCHES, fbMatch.getId(), fbMatch.getDateTimeInt());
    }

    public void sendCancelledMessage(String message){
        log.debug("Sending message {} that match is canceled", message);
        redisTemplate.convertAndSend(TOPIC_CANCELLED, message );
    }
}
