package com.smart8.sportradar.repositories;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Repository
public class FavoritesRepository {
    private static final String FAVORITES = "favorites:";

    private StringRedisTemplate redisTemplate;
    private SetOperations<String, String> setOps;

    @Autowired
    public FavoritesRepository(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.setOps = redisTemplate.opsForSet();
    }

    public Set<String> getUsersByFavorites(String type, long entityId){
        Set<String> users = setOps.members(FAVORITES + type + ":" + entityId);
        log.debug("{} users favorites {}:{}", users.size(), type, entityId);
        return users;
    }

    /**
     * @param type - type of favorites (matches/teams/tournaments)
     */
    public void addUserToFavorites(String type, long entityId, String userId) {
        log.debug("Add user {}: to favorites {}:{}", userId, type, entityId);
        final String key = FAVORITES + type + ":" + entityId;
        setOps.add(key, userId);
        redisTemplate.expire(key, 30L, TimeUnit.HOURS);
    }

    public void removeUserFromFavorites(String type, long entityId, String userId) {
        log.debug("Removing user {}: from favorites {}:{}", userId, type, entityId);
        setOps.remove(FAVORITES + type + ":" + entityId, userId);
    }

    /**
     * Remove favorite match/team/type with all users
     */
    public void removeFavorites(String type, long entityId) {
        log.debug("Removing {}:{} from favorites", type, entityId);
        redisTemplate.delete(FAVORITES + type + ":"+ entityId);
    }
}
