package com.smart8.sportradar.repositories;

import com.smart8.sportradar.firebase.FbFriendRequest;
import com.smart8.sportradar.firebase.FbUserFriend;
import com.smart8.sportradar.firebase.FbUserProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class UserRepository {
    private  static final String USERS_FRIENDS = "users:friends:";
    private   static final String USERS_TOKENS = "users:tokens";
    private  static final String USERS_SETTINGS = "users:settings:";
    private  static final String USERS_FRIEND_REQUESTS = "users:friend-requests:";

    private StringRedisTemplate redisTemplate;

    private HashOperations<String, String, String> hashOps;

    @Autowired
    UserRepository(StringRedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
        this.hashOps = redisTemplate.opsForHash();
    }

    /**
     * Update friends list. Filter friends that not in our db, if any drop and add user friend list.
     */
    public Set<String> updateFriends(String userId, Map<String, FbUserFriend> items) {
        SetOperations<String, String> setOps = redisTemplate.opsForSet();
        Set<String> allFriends = items.keySet();
        Set<String> newFriends = allFriends.stream().filter(
            item -> !setOps.isMember(USERS_FRIENDS + userId, item)
        ).collect(Collectors.toSet());
        log.debug("Founded {} new friends.", newFriends.size());

        if ( ! newFriends.isEmpty()) {//Maybe this not needed. Or needed clearer solution.
            redisTemplate.delete(USERS_FRIENDS + userId);
            setOps.add(USERS_FRIENDS + userId, allFriends.toArray(new String[]{}));
        }

        return newFriends;
    }


    public void removeUserToken(String userId) {
        log.debug("Remove user with id : {}.", userId);
        hashOps.delete(UserRepository.USERS_TOKENS, userId);
    }

    public List<String> getUsersTokenList(Set<String> userIds){
        return hashOps.multiGet(UserRepository.USERS_TOKENS, userIds);
    }

    public String getUserToken(String userId){
        return hashOps.get(UserRepository.USERS_TOKENS, userId);
    }

    public void storeUserNotificationSettings(String userId, Map<String, Boolean> notifications) {
        log.debug("Store User {} Notification settings {}", userId, notifications);
        for(Map.Entry<String, Boolean> entry:  notifications.entrySet()){
            hashOps.put(USERS_SETTINGS + userId, entry.getKey(), entry.getValue().toString());
        }
    }
    public boolean isUserNotificationAllowed(String userId, FbUserProfile.NotificationType notifyType){
        String value = hashOps.get(USERS_SETTINGS + userId, notifyType.name());
        return value != null && value.equals("true");
    }

    public void setUserToken(String userId, String token) {
        log.debug("Set new user token. {}:{}", userId, token);
        hashOps.put(UserRepository.USERS_TOKENS, userId, token);
    }

    public Boolean incrimentFriendRequest(String requestId, FbFriendRequest.InviteStatus status){
        return hashOps.putIfAbsent(USERS_FRIEND_REQUESTS + requestId, status.name(), "1");
    }


}
