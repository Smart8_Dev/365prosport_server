package com.smart8.sportradar.tasks;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.enums.CriterionType;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.mappers.ResourceMapper;
import com.smart8.sportradar.models.MappedLeagues;
import com.smart8.sportradar.models.MiniMatch;
import com.smart8.sportradar.models.unibet.BetOffer;
import com.smart8.sportradar.models.unibet.UniLiveResult;
import com.smart8.sportradar.models.unibet.UniResult;
import com.smart8.sportradar.repositories.MatchRepository;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.services.UnibetService;
import com.smart8.sportradar.utils.ConverterHelpers;
import com.smart8.sportradar.utils.MatchComperator;
import com.smart8.sportradar.utils.MyCollections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.stream.Collectors;

import static com.smart8.sportradar.Constants.ODDS_DELIM;

@Slf4j
@Component
public class UnibetTask {
    public static final long PRE_MATCH_INTERVAL = 12 * 3600 * 1000L;
    public static final long LIVE_BETS_INTERVAL = 30 * 1000L;
    public static final int FIVE_HOURS = 5 * 3600 * 1000;

    private static final String LEAGUES_JSON = "appdata/mapped_leagues.json";

    private static final String EVEN_ODDS = "Evens";

    private Map<Long, List<Long>> groupToSubGroups = new HashMap<>();

    private static final String SOCCER_UNI = "FOOTBALL";
    private static final String DARTS_UNI = "DARTS";

    @Autowired
    ResourceMapper resourceMapper;

    @Autowired
    private UnibetService unibetService;

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private FirebaseService firebaseService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MatchComperator comperator;

    private Map<Long, String> uniLeagueToRadarLeague = new HashMap<>();

    private Set<Long> found = new HashSet<>();
    private Set<String> foundRadarGamesIds = new HashSet<>();
    private Set<Long> relevantEvents = new HashSet<>();
    private List<MatchComperator.MatchMapper> foundMatches = new ArrayList<>();
    private Map<Long, String> unmappedTournaments = new HashMap<>();

    @PostConstruct
    public void init() {
        List<MappedLeagues> leagues = resourceMapper.readDataMap(LEAGUES_JSON, new TypeReference<List<MappedLeagues>>() {
        });

        if (leagues == null) {
            throw new UnsupportedOperationException("no Mapped leagues");
        }

        for (MappedLeagues league : leagues) {
            groupToSubGroups.putIfAbsent(league.getParentid(), new ArrayList<>());
            groupToSubGroups.get(league.getParentid()).add(league.getUniId());
            uniLeagueToRadarLeague.put(league.getUniId(), league.getSrId());
        }

        matchRepository.addTournamentMapping(uniLeagueToRadarLeague);
    }

    @Scheduled(fixedRate = LIVE_BETS_INTERVAL)
    public void getLiveBets() {
        List<MiniMatch> liveMatches = matchRepository.getLiveMatches();
        if (liveMatches.isEmpty()) {
            return;
        }
        Map<Long, MiniMatch> mappedLiveMatches = new HashMap<>();
        liveMatches.forEach(item -> {
            long uniId = matchRepository.getUnibetMatchIdByRadarMatchId(item.getId());
            if (uniId > 0) {
                mappedLiveMatches.put(uniId, item);
            }
        });

        log.debug("Number of mapped live matches: {}", mappedLiveMatches.size());
        log.debug("Mapped live matches {}", mappedLiveMatches.keySet());

        Set<Long> radarTourIdList = mappedLiveMatches.values().stream().map(item -> Long.valueOf(item.getTournamentId()))
            .collect(Collectors.toSet());

        List<UniLiveResult> results = new ArrayList<>();

        for (Long tournamentId : radarTourIdList) {
            String uniTournamentId = matchRepository.getUniTournamentIdByRadarId(tournamentId.toString());
            if (uniTournamentId != null) {

                int start = 0;
                UniLiveResult uniResult;
                do {
                    uniResult = unibetService.fetchLiveBets(start, Long.parseLong(uniTournamentId));

                    if (uniResult == null) break;
                    start += 100;
                    results.add(uniResult);
                } while (uniResult.getRange() != null && uniResult.getRange().getTotal() <= start);
            }
        }
        try {
            parseUniLiveBets(results, mappedLiveMatches);
        } catch (Exception ex) {
            log.warn("Error on parsing uniLiveBets MappedLiveMatches {}", mappedLiveMatches);
            log.warn("Error on parsing UniLiveResults {}", results);
        }
    }

    @Scheduled(fixedRate = PRE_MATCH_INTERVAL)
    public void runPreMatch() {

        List<UniResult> results = new ArrayList<>();
        for (Map.Entry<Long, List<Long>> entry : groupToSubGroups.entrySet()) {
            int start = 0;
            boolean isDone = false;

            log.info("@runPreMatch > query for group: " + entry.getKey() + ", start");

            while (!isDone) {
                final UniResult uniResult = unibetService.fetchPreMatchBets(start, entry.getKey(), entry.getValue());
                if (uniResult == null) {
                    break;
                }
                final UniResult.Range range = uniResult.getRange();
                start += 100;
                isDone = range == null || range.getTotal() <= start;
                results.add(uniResult);
            }

            log.info("FINISHED query for group: " + entry.getKey() + ", start = " + start + " FINISHED ******");
        }

        Map<Long, UniResult.Event> events = new HashMap<>();
        for (UniResult result : results) {
            final List<UniResult.Event> events1 = result.getEvents();
            for (UniResult.Event event : events1) {
                events.put(event.getId(), event);
            }
        }

        parseUniResults(results, events);
    }

    private void parseUniLiveBets(List<UniLiveResult> results, Map<Long, MiniMatch> mappedLiveMatches) {
        log.debug("Parsing live bets..");
        Map<Long, UniLiveResult.Event> events = new HashMap<>();
        for (UniLiveResult result : results) {
            for (UniLiveResult.LiveEvent liveEvent : result.getEvents()) {
                events.put(liveEvent.getEvent().getId(), liveEvent.getEvent());
            }
        }
        Map<String, Set<BetOffer>> matchBetOffersSoccerMap = new HashMap<>();
        Map<String, Set<BetOffer>> matchBetOffersDartsMap = new HashMap<>();

        for (UniLiveResult result : results) {
            final List<UniLiveResult.BetOffer> offers = result.getBetoffers();

            for (UniLiveResult.BetOffer uniOffer : offers) {
                UniLiveResult.Event event = events.get(uniOffer.getEventId());
                if (event == null) {
                    log.warn("event {} not exists. BetOffer:{}", uniOffer.getEventId(), uniOffer);
                    continue;
                }
                if (isCriterionAllowed(event.getSport(), uniOffer)) {
                    MiniMatch match = mappedLiveMatches.get(uniOffer.getEventId());
                    log.debug("Add bet offers to match {} fetched by eventId {} ", match, uniOffer.getEventId());
                    if (match != null) {
                        Set<BetOffer> betOfferSet;
                        if (event.getSport().equalsIgnoreCase(DARTS_UNI)) {
                            betOfferSet = matchBetOffersDartsMap.computeIfAbsent(match.getId(), l -> new HashSet<>());
                        } else {
                            betOfferSet = matchBetOffersSoccerMap.computeIfAbsent(match.getId(), l -> new HashSet<>());
                        }

                        if (uniOffer.getOutcomes() != null) {
                            final BetOffer betOffer = new BetOffer();
                            betOffer.setName(uniOffer.getCriterion().getLabel());
                            betOffer.setCriterionId(uniOffer.getCriterion().getId());

                            for (UniLiveResult.Outcome outcome : uniOffer.getOutcomes()) {
                                betOffer.addOutcome(new BetOffer.Outcome(calculateOdds(outcome.getOddsFractional()), outcome.getType()));
                            }
                            log.debug("Adding betoffer {} to match {}", betOffer, match.getId());
                            betOfferSet.add(betOffer);
                        }
                    }
                }
            }
        }
        firebaseService.updateOdds(matchBetOffersSoccerMap, SportType.Soccer.name());
        firebaseService.updateOdds(matchBetOffersDartsMap, SportType.Darts.name());
    }

    private boolean isCriterionAllowed(String sport, UniResult.BetOffer uniOffer) {
        Optional<CriterionType> optinal = CriterionType.findById(uniOffer.getCriterion().getId());
        return (sport.equalsIgnoreCase(SOCCER_UNI) && optinal.isPresent())
            || (sport.equalsIgnoreCase(DARTS_UNI) && optinal.isPresent() && optinal.get() == CriterionType.MATCH_OUTCOME);
    }

    private boolean isCriterionAllowed(String sport, UniLiveResult.BetOffer uniOffer) {
        Optional<CriterionType> optinal = CriterionType.findById(uniOffer.getCriterion().getId());
        return (sport.equalsIgnoreCase(SOCCER_UNI) && optinal.isPresent())
            || (sport.equalsIgnoreCase(DARTS_UNI) && optinal.isPresent() && optinal.get() == CriterionType.MATCH_OUTCOME);
    }



    //TODO AD: this method should be rewritten. Too complicated and duplicated
    private void parseUniResults(List<UniResult> results, Map<Long, UniResult.Event> events) {
        try {
            log.info("Processing {} odds results", results.size());
            Map<String, Set<BetOffer>> betOfferSoccerMap = new HashMap<>();
            Map<String, Set<BetOffer>> betOfferDartsMap = new HashMap<>();

            int counter = 0;
            for (UniResult result : results) {
                counter++;
                final List<UniResult.BetOffer> betOffers = result.getBetoffers();
                for (UniResult.BetOffer betOffer : betOffers) {

                    final long eventId = betOffer.getEventId();
                    final UniResult.Event event = events.get(eventId);

                    if (event == null) {
                        log.info("@runPreMatch > no event for bet: " + betOffer);
                        continue;
                    }

                    final String sRadarId = matchRepository.getRadarMatchIdFromUnibetMatchId(eventId);

                    if (sRadarId != null) {
                        log.info("Event {}: '{}' mapped to sportradar match {}", eventId, event.getName(), sRadarId);
                        found.add(eventId);
                        if (event.getSport().equalsIgnoreCase(SOCCER_UNI)) {
                            List<BetOffer> list = collectOdds(result.getBetoffers(), sRadarId, eventId, event.getSport());
                            Set<BetOffer> betOfferList = betOfferSoccerMap.computeIfAbsent(sRadarId, l -> new HashSet<>());
                            betOfferList.addAll(list);
                        } else if (event.getSport().equalsIgnoreCase(DARTS_UNI)) {
                            List<BetOffer> list = collectOdds(result.getBetoffers(), sRadarId, eventId, event.getSport());
                            Set<BetOffer> betOfferList = betOfferDartsMap.computeIfAbsent(sRadarId, l -> new HashSet<>());
                            betOfferList.addAll(list);
                        }

                        continue;
                    }

                    Collection<MiniMatch> matches = possibleMatchesForEvent(event);

                    Optional<MatchComperator.MatchMapper> comparableMatch = findExactMatch(event, matches);

                    comparableMatch.ifPresent(matchMapper -> {
                        saveMapping(eventId, matchMapper);
                        if (event.getSport().equalsIgnoreCase(SOCCER_UNI)) {
                            List<BetOffer> list = collectOdds(result.getBetoffers(), matchMapper.sRadarId, matchMapper.unibetId, event.getSport());
                            Set<BetOffer> betOfferList = betOfferSoccerMap.computeIfAbsent(matchMapper.sRadarId, l -> new HashSet<>());
                            betOfferList.addAll(list);
                        } else if (event.getSport().equalsIgnoreCase(DARTS_UNI)) {
                            List<BetOffer> list = collectOdds(result.getBetoffers(), matchMapper.sRadarId, matchMapper.unibetId, event.getSport());
                            Set<BetOffer> betOfferList = betOfferDartsMap.computeIfAbsent(matchMapper.sRadarId, l -> new HashSet<>());
                            betOfferList.addAll(list);
                        }
                    });


                    if (!comparableMatch.isPresent()) {
                        log.warn("Can't find matching match for event {}:{}", event.getId(), event.getName());
                        matchRepository.setUnmappedEvent(String.valueOf(event.getId()), objectMapper.writeValueAsString(event));
                    }
                }
            }
            firebaseService.updateOdds(betOfferSoccerMap, SportType.Soccer.name());
            firebaseService.updateOdds(betOfferDartsMap, SportType.Darts.name());
            log.debug("Done parsing {} results", counter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<BetOffer> collectOdds(List<UniResult.BetOffer> betOffers, String matchId, long eventId, String sport) {

        final List<BetOffer> betOfferList = betOffers.parallelStream()
            .filter(betOffer -> betOffer.getEventId() == eventId)
            .filter(betOffer -> isCriterionAllowed(sport, betOffer))
            .map(betOffer -> {
                final BetOffer offer = new BetOffer();
                offer.setName(betOffer.getCriterion().getLabel());
                offer.setCriterionId(betOffer.getCriterion().getId());

                final List<UniResult.Outcome> outcomes = betOffer.getOutcomes();
                for (UniResult.Outcome outcome : outcomes) {
                    offer.addOutcome(new BetOffer.Outcome(calculateOdds(outcome.getOddsFractional()), outcome.getType()));
                }

                return offer;
            }).collect(Collectors.toList());

        matchRepository.removeUnmappedMatch(matchId);
        matchRepository.removeUnmappedEvent(String.valueOf(eventId));

        return betOfferList;
    }

    private float calculateOdds(String oddsFractional) {
        if ( oddsFractional.equals(EVEN_ODDS)){
            return 1f;
        }
        final String[] strings = oddsFractional.split(ODDS_DELIM);

        float a;
        float b;
        try {
            a = Float.parseFloat(strings[0]);
            b = Float.parseFloat(strings[1]);
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            log.error("Error while calc odds {}. {}", oddsFractional, e.getMessage());
            return 2.00f;
        }
        final float v = a / b;

        return round(1 + v);
    }

    private float round(float value) {
        final BigDecimal bigDecimal = new BigDecimal(Float.toString(value));
        final BigDecimal toReturn = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
        return toReturn.floatValue();
    }

    private void saveMapping(long eventId, MatchComperator.MatchMapper matchMapper) {
        matchRepository.storeMatchMapping(matchMapper.unibetId, matchMapper.sRadarId);
        matchRepository.storeTeamMapping(matchMapper.teamA.unibetTeam, matchMapper.teamA.sRadarTeam);
        matchRepository.storeTeamMapping(matchMapper.teamB.unibetTeam, matchMapper.teamB.sRadarTeam);
        found.add(eventId);
        foundMatches.add(matchMapper);
        foundRadarGamesIds.add(matchMapper.sRadarId);
    }

    private Optional<MatchComperator.MatchMapper> findExactMatch(UniResult.Event event, Collection<MiniMatch> matches) {

        for (MiniMatch match : matches) {
            final Optional<MatchComperator.MatchMapper> matchMapper = comperator.matchesComperator(event, match);

            if (matchMapper.isPresent()) {
                log.info("@findExactMatch > FOUND match:\n{}\nFbMatch: {}\nEvent: {}", matchMapper, match, event);

                return Optional.of(matchMapper.get());
            }

        }

        return Optional.empty();
    }

    private Collection<MiniMatch> possibleMatchesForEvent(UniResult.Event event) {
        final String start = event.getStart();
        final TemporalAccessor parse = ConverterHelpers.unibetFormat.parse(start);

        final Instant from = Instant.from(parse);
        Instant instant = from.minusMillis(FIVE_HOURS);
        final Instant until = from.plusMillis(FIVE_HOURS);

        final List<MiniMatch> matchesAfterDate = matchRepository.getMatchesAfterDate(instant.toEpochMilli(), until.toEpochMilli());

        if (MyCollections.isNotEmpty(matchesAfterDate)) {
            relevantEvents.add(event.getId());

            final long groupId = event.getGroupId();

            final String sRadarId = uniLeagueToRadarLeague.get(groupId);

            if (sRadarId == null) {
                log.info("@possibleMatchesForEvent > no league mapping for event\n{}", event);
                unmappedTournaments.put(groupId, event.getGroup());
                return Collections.emptyList();
            }

            return matchesAfterDate.parallelStream().filter(match -> Long.valueOf(sRadarId) == match.getTournamentId())
                .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

}
