package com.smart8.sportradar.handlers;

/*
@RunWith(SpringRunner.class)
@DataRedisTest( includeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = MatchRepository.class),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = SportRadarMatchToFbMatchConverter.class)
})
public class CancelledMatchHandlerIT {

    private static  FbListenerService listenerService;

    @Autowired
    SportRadarMatchToFbMatchConverter converter;

    @Import({RedisConfig.class})
    @TestConfiguration
    public static class Configuration {
        @Bean
        public ObjectMapper mapper() {
            return new ObjectMapper();
        }

        @Bean
        CancelledMatchHandler matchHandler(MatchRepository matchRepository){
            listenerService = Mockito.mock(FbListenerService.class);
            return  new CancelledMatchHandler(matchRepository, listenerService );
        }
    }


    @Test
    public void testCancelledMatchMessage() throws InterruptedException, IOException {
        SportradarData data = TestHelper.getSportRadarDataFromXml("sportradar/statfeeds/matchdetail-soccer-postponed.xml");
        Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
        Match match = tournament.getMatches().getMatch().get(0);
        FbMatch fbMatch = converter.convertMatchToFbMatch(SportType.Soccer, tournament, match);

        verify(listenerService).returnMatchBets(SportType.Soccer.name().toLowerCase(), fbMatch.getId(), true);
        verifyNoMoreInteractions(listenerService);
    }
}*/
