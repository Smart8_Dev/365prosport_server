package com.smart8.sportradar.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.repositories.MatchRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.redis.DataRedisTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static com.smart8.sportradar.Constants.REDIS_MAX_VIEW_ITEMS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataRedisTest( includeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = MatchRepository.class),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = MappingService.class)
})
public class MappingServiceIT {

    private static final String UNI_EVENT_STR = "{\"state\":\"NOT_STARTED\",\"sport\":\"FOOTBALL\",\"originalStartTime\":null,\"type\":\"ET_MATCH\",\"id\":1004058405,\"groupId\":1000094985,\"start\":\"2017-11-24T20:00Z\",\"awayName\":\"Leicester City\",\"name\":\"West Ham - Leicester City\",\"homeName\":\"West Ham\",\"group\":\"Premier League\",\"participants\":[{\"name\":\"West Ham\",\"participantId\":1000000082},{\"name\":\"Leicester City\",\"participantId\":1000040947}],\"path\":[{\"id\":1000093190,\"englishName\":\"Football\",\"name\":\"Football\"},{\"id\":1000461733,\"englishName\":\"England\",\"name\":\"England\"},{\"id\":1000094985,\"englishName\":\"Premier League\",\"name\":\"Premier League\"}]}";
    @TestConfiguration
    public static class Configuration {

        @Bean
        public ObjectMapper mapper() {
            return new ObjectMapper();
        }
    }

    @Autowired
    private MappingService mappingService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Before
    public void setUp(){
        assertEquals("===!!!DB IS NOT CLEAN!!!===. Are you sure it's a test DB?", 0, redisTemplate.keys("*").size());
    }

    @Test
    public void TestRedisSearchByPattern() throws Exception {
        assertEquals( 0, redisTemplate.keys("*").size());
        Map<String, String> preloadData = new HashMap<>();
        preloadData.put("111111", "miss");
        preloadData.put("112111", UNI_EVENT_STR);
        preloadData.put("1123242", UNI_EVENT_STR);
        preloadData.put("113456", "miss");
        preloadData.put("112234234", UNI_EVENT_STR);

        redisTemplate.opsForHash().putAll(MatchRepository.UNMAPPED_UNI_EVENTS, preloadData);

        final String PATTERN = "112";
        Map<String, String> result = mappingService.findUnmappedEvents(PATTERN);

        assertEquals(3, result.size());

        for ( Map.Entry<String, String> entry : result.entrySet()){
            assertTrue(entry.getKey().substring(0, PATTERN.length()).equals(PATTERN));
            assertEquals("West Ham - Leicester City", entry.getValue());
        }
    }

    @Test
    public void TestRedisSearchByPattern_WithToManyFoundedItems() throws Exception {
        assertEquals( 0, redisTemplate.keys("*").size());
        final String HASH_KEY = MatchRepository.UNMAPPED_UNI_EVENTS;
        Map<String, String> preloadData = new HashMap<>();
        final String PATTERN = "112";

        for ( int i = 0; i < REDIS_MAX_VIEW_ITEMS + 5; i++){
            preloadData.put(PATTERN + i, UNI_EVENT_STR);
        }
        redisTemplate.opsForHash().putAll(HASH_KEY, preloadData);

        Map<String, String> result = mappingService.findUnmappedEvents(PATTERN);

        assertEquals(REDIS_MAX_VIEW_ITEMS, result.size());
    }

    @Test
    public void testStoreMatchMapping(){
        assertEquals( 0, redisTemplate.keys("*").size());
        final String UNI_EVENT_ID = "11111111";
        final String RADAR_MATCH_ID = "222222222";

        redisTemplate.opsForHash().put(MatchRepository.UNMAPPED_UNI_EVENTS, UNI_EVENT_ID, "{event}");
        redisTemplate.opsForZSet().add(MatchRepository.UNMAPPED_MATCHES, RADAR_MATCH_ID, 111L);

        assertEquals(2, redisTemplate.keys("*").size());

        mappingService.storeMatchMapping(UNI_EVENT_ID, RADAR_MATCH_ID);

        assertEquals(2, redisTemplate.keys("*").size());


        assertEquals(RADAR_MATCH_ID, redisTemplate.opsForHash().get(MatchRepository.HASH_UNI_TO_RADAR_MATCH, UNI_EVENT_ID));
        assertEquals(UNI_EVENT_ID, redisTemplate.opsForHash().get(MatchRepository.HASH_RADAR_TO_UNI_MATCH, RADAR_MATCH_ID));
    }

    @After
    public void cleanUp(){
        redisTemplate.delete(MatchRepository.UNMAPPED_UNI_EVENTS);
        redisTemplate.delete(MatchRepository.HASH_UNI_TO_RADAR_MATCH);
        redisTemplate.delete(MatchRepository.HASH_RADAR_TO_UNI_MATCH);
        redisTemplate.delete(MatchRepository.UNMAPPED_MATCHES);
    }
}
