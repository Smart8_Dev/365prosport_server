package com.smart8.sportradar.configs;


import com.google.firebase.database.DatabaseReference;
import com.smart8.sportradar.services.FbListenerService;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.tasks.UnibetTask;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;

@TestConfiguration
public class TestConfig {

    @MockBean
    FirebaseService service;

    @MockBean
    FbListenerService fbservice;


    @MockBean
    UnibetTask task;

    @MockBean
    @Qualifier("fireBaseRef")
    DatabaseReference firebaseDb;

    @MockBean
    @Qualifier("news")
    DatabaseReference newsDb;
}
