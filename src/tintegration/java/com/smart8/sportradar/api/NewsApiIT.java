package com.smart8.sportradar.api;


import com.smart8.sportradar.configs.TestConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(TestConfig.class)
@AutoConfigureMockMvc
public class NewsApiIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private static final String USER_ID = "asdaswerYUUI";
    private static final String TOKEN = "j11241234lH1:idfasdf-ljasdfjdasfew";
    @Before
    public void setUp(){
        redisTemplate.opsForHash().put("users:tokens", USER_ID, TOKEN);
    }

    @Test
    public void testFetchingBingNews() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.put("userId", Collections.singletonList(USER_ID));
        params.put("token", Collections.singletonList(TOKEN));
        this.mockMvc.perform(get("/api/news").params(params))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("externalCreatedAt")));
    }

    @Test
    public void testFetchingSocialNews() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.put("userId", Collections.singletonList(USER_ID));
        params.put("token", Collections.singletonList(TOKEN));
        this.mockMvc.perform(get("/api/socials").params(params).param("lang", "en"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("unformatted_message")));
    }

    @After
    public void cleanUp(){
        Set<String> keys = redisTemplate.keys("news-*");
        keys.forEach( key -> redisTemplate.delete(key));
        redisTemplate.opsForHash().delete("users:tokens", USER_ID);
    }



}
