package com.smart8.sportradar.services;

import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

@Slf4j
public class ArchiveDataServiceTest {

    private ArchiveDataService service;

    private final long FIRST_BYTES_7Z = 0x377ABCAF;

    @Before
    public void setUp() {
        service = new ArchiveDataService();
    }

    @Test
    public void testZippingFiles() throws IOException {
        final int NUMBER_OF_FOLDERS = 5;
        final String testFolder = "temp-test-folder";
        ReflectionTestUtils.setField(service, "pathToData", testFolder);

        Path path = Paths.get(testFolder);
        createFolders(path, NUMBER_OF_FOLDERS);

        File[] listOfFiles = path.toFile().listFiles();
        assert listOfFiles != null;
        assertEquals(NUMBER_OF_FOLDERS, Files.list(path).filter(Files::isDirectory).count());

        for (File f : listOfFiles) {
            if (f.isFile()) {
                fail("Should be no files");
            }
        }

        service.zipData();

        assertEquals(1, Files.list(path).filter(Files::isDirectory).count());
        Files.list(path).filter(Files::isRegularFile).forEach(f -> assertTrue(isZipFile(f.toFile())));

        log.debug("Deleting folder {}", path);
        FileUtils.deleteDirectory(path.toFile());
    }


    private boolean isZipFile(File file) {
        try {
            DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
            int test;
            test = in.readInt();
            in.close();
            return test == FIRST_BYTES_7Z;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void createFolders(Path path, int num) throws IOException {
        log.debug("Creating folder {}", path);
        LocalDateTime now = LocalDateTime.now();
        Path newD = Files.createDirectories(path);
        for (int i = 0; i < num; i++) {
            String date = ConverterHelpers.shortDateFormat.format(now.minusDays(i));
            Path p = Files.createDirectories(newD.resolve(Paths.get(date)));
            log.debug("Creating child folder {}", p);
            for (int j = 0; j < 10; j++) {
                String lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
                Files.write(p.resolve(j + ".txt"), lorem.getBytes());
            }
        }
    }
}
