package com.smart8.sportradar.services;


import com.smart8.sportradar.converters.SoccerConverter;
import com.smart8.sportradar.enums.MatchEventCounter;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.firebase.FbDartsMatch;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.handlers.MatchEventsHandler;
import com.smart8.sportradar.helpers.MatchBuilderHelper;
import com.smart8.sportradar.repositories.NotificationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class MatchEventsSeriviceTest {
    @Mock
    private NotificationRepository notificationRepository;

    @Mock
    private FbListenerService fbListenerService;

    @Mock
    private NotificationServiceFcm notificationService;

    @InjectMocks
    private MatchEventsHandler service;

    private final String MATCH_ID = "1000";

    @Before
    public void setUp() {
        when(fbListenerService.isMatchObservable(MATCH_ID)).thenReturn(true);
    }

    private static final String PLAYER_1 = "100";
    private static final String PLAYER_2 = "200";
    private static final long TEAM_1 = 1001L;
    private static final long TEAM_2 = 2002L;

    @Test
    public void testMatchNotStartingSoon_NoNotificatinSent() {
        final long TIME_IN_5_HOURS = System.currentTimeMillis() + 5 * 60 * 60 * 1000;
        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setDateTimeInt(TIME_IN_5_HOURS);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService, Mockito.times(0)).notifyMatch(fbMatch, MatchEventCounter.SoonStart);
        Mockito.verifyNoMoreInteractions(notificationRepository);
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchStartingSoon_SoonStartNotificatin_ShouldBeSent_Soccer() {
        final long TIME_IN_10_MIN = System.currentTimeMillis() + 10 * 60 * 1000;

        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setDateTimeInt(TIME_IN_10_MIN);

        service.handleMatchEvents(fbMatch);
        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.SoonStart);
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchStartingSoon_SoonStartNotificatin_ShouldBeSent_Darts() {
        final long TIME_IN_10_MIN = System.currentTimeMillis() + 10 * 60 * 1000;
        FbDartsMatch fbMatch = new FbDartsMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setDateTimeInt(TIME_IN_10_MIN);

        service.handleMatchEvents(fbMatch);
        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.SoonStart);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.SoonStart, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }


    @Test
    public void testMatchLineups_LineupNotificatin_ShouldBeSent() {
        final long TIME_IN_10_MIN = System.currentTimeMillis() + 10 * 60 * 1000;

        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.SoonStart.name())).thenReturn("1");

        FbSoccerMatch.LineupPlayer player = FbSoccerMatch.LineupPlayer.builder()
            .lastName("Lastnameplayer").build();
        FbSoccerMatch.Lineup lineup = FbSoccerMatch.Lineup.builder()
            .players(Collections.singletonList(player)).build();
        FbSoccerMatch.SoccerLiveData livedata = FbSoccerMatch.SoccerLiveData.builder()
            .lineup(Collections.singletonList(lineup)).build();

        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setDateTimeInt(TIME_IN_10_MIN);
        fbMatch.setLiveData(livedata);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Lineup);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.Lineup, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchStartFirstNotification() {
        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setStatusCode(StatusCode.FirstHalf.statusCode);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.StartFirst);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.StartFirst, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchStartSecondNotification() {
        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setStatusCode(StatusCode.SecondHalf.statusCode);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.StartSecond);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.StartSecond, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchHalfTimeNotification() {
        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setStatusCode(StatusCode.Halftime.statusCode);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Ht);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.Ht, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchFinishedNotification() {
        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setStatusCode(StatusCode.Ended.statusCode);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Finished);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.Finished, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

   @Test
    public void testMatchGoalsdNotification() {
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.StartFirst.name())).thenReturn("1");

        Map<String, Integer> score = new HashMap<>();
        score.put("1", 1);
        score.put("2", 0);

        FbSoccerMatch fbMatch = buildSoccerMatch(score, MatchBuilderHelper.createGoalsByTeamIdAndPeriod("1:1"));

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Goal);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.Goal, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }



    @Test
    public void testMatchGoalsdNotification_ForThirdGoal() {
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.StartFirst.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Goal.name())).thenReturn("2");

        Map<String, Integer> score = new HashMap<>();
        score.put("1", 1);
        score.put("2", 2);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod("1:1", "1:2", "2:2");

        FbSoccerMatch fbMatch = buildSoccerMatch(score, goals);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Goal);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.Goal, "3");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchCardsdNotification_WhenCardIsRed() {
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.StartFirst.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Card.name())).thenReturn("1");

        FbSoccerMatch.Card card = FbSoccerMatch.Card.builder().contestantId(1).type(SoccerConverter.Y_CARD).build();
        FbSoccerMatch.Card card2 = FbSoccerMatch.Card.builder().contestantId(2).type(SoccerConverter.R_CARD).build();

        FbSoccerMatch.SoccerLiveData livedata = FbSoccerMatch.SoccerLiveData.builder()
            .cards(Arrays.asList(card, card2))
            .build();

        FbSoccerMatch fbMatch = buildSoccerMatch(null, null);
        fbMatch.setLiveData(livedata);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Card);
        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.Card, "2");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testMatchCardsdNotification_WhenCardIsYellow_ShouldBeNoNotification() {
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.StartFirst.name())).thenReturn("1");

        FbSoccerMatch.Card card = FbSoccerMatch.Card.builder().contestantId(1).type(SoccerConverter.Y_CARD).build();

        FbSoccerMatch.SoccerLiveData livedata = FbSoccerMatch.SoccerLiveData .builder()
            .cards(Collections.singletonList(card))
            .build();

        FbSoccerMatch fbMatch = buildSoccerMatch(null, null);
        fbMatch.setLiveData(livedata);

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationRepository).setMatchNotification(fbMatch.getId(), MatchEventCounter.Card, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }



    @Test
    public void testDartsSetsAndLegsNotify_WhenSetIsFinished_LegsShouldBeResetToZero() {

        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Started.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Sets.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Legs.name())).thenReturn("4");

        FbMatch fbMatch = buildDartsStartedMatch(createSets("0:2" ).get(0), createSets("1:2", "2:3" ));

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Sets);
        Mockito.verify(notificationRepository).setMatchNotification(MATCH_ID, MatchEventCounter.Sets, "2");
        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Legs);
        Mockito.verify(notificationRepository).setMatchNotification(MATCH_ID, MatchEventCounter.Legs, "0");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testDartsLegsNotification_For2Set2Leg() {
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Started.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Sets.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Legs.name())).thenReturn("1");

        FbMatch fbMatch = buildDartsStartedMatch(createSets("0:1" ).get(0), createSets("1:2", "1:1"  ));

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Legs);
        Mockito.verify(notificationRepository).setMatchNotification(MATCH_ID, MatchEventCounter.Legs, "2");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testDartsLegsNotification_WhenNewLegIsPlayed() {
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Started.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Sets.name())).thenReturn("1");

        FbMatch fbMatch = buildDartsStartedMatch(createSets("0:1" ).get(0), createSets("1:3", "0:1"  ));

        service.handleMatchEvents(fbMatch);

        Mockito.verify(notificationService).notifyMatch(fbMatch, MatchEventCounter.Legs);
        Mockito.verify(notificationRepository).setMatchNotification(MATCH_ID, MatchEventCounter.Legs, "1");
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    @Test
    public void testNoNotificationSent_WhenSetsNotifySentAndNoNewSetsStarted_AndWhenLegsNotifyCountReturns0(){
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Started.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Sets.name())).thenReturn("1");
        when(notificationRepository.getMatchNotification(MATCH_ID, MatchEventCounter.Legs.name())).thenReturn("0");

        FbMatch fbMatch = buildDartsStartedMatch(createSets("0:1").get(0), createSets("1:2"));

        service.handleMatchEvents( fbMatch);
        Mockito.verify(notificationRepository, times(3)).getMatchNotification(anyString(), anyString());
        Mockito.verifyNoMoreInteractions(notificationRepository);
        Mockito.verifyNoMoreInteractions(notificationService);
    }

    private List<Map<String, Integer>> createSets(String ... values) {
        List<Map<String, Integer>> setScoreList = new ArrayList<>();
        for (String value : values) {
            Map<String, Integer> setScore = new HashMap<>();
            String[] scoreStr = value.split(":");
            setScore.put(PLAYER_1, Integer.parseInt(scoreStr[0]));
            setScore.put(PLAYER_2, Integer.parseInt(scoreStr[1]));
            setScoreList.add(setScore);
        }
        return setScoreList;
    }

    private FbMatch buildDartsStartedMatch(Map<String, Integer> score, List<Map<String, Integer>> setsScore){
        FbDartsMatch.DartsLiveData livedata = FbDartsMatch.DartsLiveData.builder()
            .score(score)
            .sets(setsScore)
            .build();

        FbDartsMatch fbMatch = new FbDartsMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setStatusCode(StatusCode.Started.statusCode);
        fbMatch.setLiveData(livedata);

        return fbMatch;
    }

    private FbSoccerMatch buildSoccerMatch(Map<String, Integer> score, List<FbSoccerMatch.Goal> goals) {
        Contestant homeTeam = Contestant.builder().position("home").id(TEAM_1).build();
        Contestant awayTeam = Contestant.builder().position("away").id(TEAM_2).build();

        FbSoccerMatch.SoccerLiveData livedata = FbSoccerMatch.SoccerLiveData.builder()
            .score(score)
            .goals(goals)
            .build();

        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setStatusCode(StatusCode.FirstHalf.statusCode);
        fbMatch.setContestants(Arrays.asList(homeTeam, awayTeam));
        fbMatch.setLiveData(livedata);
        return  fbMatch;
    }
}
