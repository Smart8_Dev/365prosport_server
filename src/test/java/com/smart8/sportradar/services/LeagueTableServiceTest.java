package com.smart8.sportradar.services;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.fb.FbStandings;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.statisticsmodels.SportradarData;
import com.smart8.sportradar.statisticsmodels.Tournament;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LeagueTableServiceTest {

    private LeagueTableService service;
    private FirebaseService fbService;

    @Before
    public void setUp() throws IOException {
        fbService = mock(FirebaseService.class);
        service = new LeagueTableService( fbService);
    }

    @Test
    public void testTeamStandingsSoccer_WithNoErrors() throws IOException {

        SportradarData data = TestHelper.getSportRadarDataFromXml("sportradar/statfeeds/leaguetable-soccer.xml");
        service.processTables(data);
        Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
        Integer subTournamentId = tournament.getLeagueTable().get(0).getId();
        verify(fbService).saveStandings(eq(tournament.getUniqueTournamentId().toString()), eq(subTournamentId.toString()), any(FbStandings.class), eq(SportType.Soccer.name()));
    }

    @Test
    public void testTeamStandingsDarts_WithNoErrors() throws IOException {
        SportradarData data = TestHelper.getSportRadarDataFromXml("sportradar/statfeeds/leaguetable-darts.xml");
        service.processTables(data);
        Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
        Integer subTournamentId = tournament.getLeagueTable().get(0).getId();
        verify(fbService).saveStandings(eq(tournament.getUniqueTournamentId().toString()), eq(subTournamentId.toString()), any(FbStandings.class), eq(SportType.Darts.name()));
    }
}
