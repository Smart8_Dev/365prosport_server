package com.smart8.sportradar.services;

import com.smart8.sportradar.firebase.FbPlayer;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.converters.stats.PlayerToFbPlayerConverter;
import com.smart8.sportradar.reports.PlayerMappingReport;
import com.smart8.sportradar.statisticsmodels.Player;
import com.smart8.sportradar.statisticsmodels.SportradarData;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class SquadServiceTest {

    private SquadService squadService;

    private  static SportradarData data;

    @Mock
    private PlayerMappingReport report;

    @Mock
    private FirebaseService firebaseService;

    @Mock
    private PlayerToFbPlayerConverter converter;

    private static List<Player> players;


    @BeforeClass
    public static void before() throws IOException {
       data = TestHelper.getSportRadarDataFromXml("sportradar/statfeeds/feed-soccer-nurnberg-squad.xml");
    }

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        squadService = new SquadService(firebaseService, report, converter );

    }

    @Test
    public void processPlayersData_ForUpdatingFB() throws IOException {
        List<Player> players = data.getSport().get(0).getTeams().getTeam().get(0).getPlayers().getPlayer();

        FbPlayer.FbPlayerBuilder fbPlayerBuilder = FbPlayer.builder().id(101L);
        FbPlayer fbPlayer = fbPlayerBuilder.build();
        when(converter.convertRadarPlayerToFbPlayer(any(Player.class))).thenReturn(fbPlayer);

        squadService.processSquad(data);
        verify(firebaseService, times(players.size())).updateGeneralPlayer(fbPlayer, data.getSport().get(0).getName().toLowerCase());

        assertEquals("Player team is incorrect", data.getSport().get(0).getTeams().getTeam().get(0).getName(), fbPlayer.getTeam());

        verify(report).writeReport(anyList(), anyString());
    }



}
