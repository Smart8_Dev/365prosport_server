package com.smart8.sportradar.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.Constants;
import com.smart8.sportradar.enums.CriterionType;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.firebase.FbBet;
import com.smart8.sportradar.firebase.FbDartsMatch;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.helpers.MatchBuilderHelper;
import com.smart8.sportradar.repositories.NotificationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class BetResolverServiceTest {

    private final static String MATCH_ID = "1001";
    private final double ODDS_1 = 1.5;
    private final int STAKE_1 = 10;

    private final static long TEAM_HOME = 100L;
    private final static long TEAM_AWAY = 200L;

    private ObjectMapper objectMapper;

    @Mock
    private NotificationRepository repository;

    @Mock
    private FbListenerService fbservice;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    private BetResolveService service;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
        service = new BetResolveService(objectMapper);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testResolveFullTime_WhenBetOnDraw_AndWin() {
        FbBet bet = getSampleBet(CriterionType.FULL_TIME, FbBet.Outcome.OT_CROSS);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_AWAY + ":2");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.FULL_TIME.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(), FbBet.Status.Win);
        int addedCoins = (int) (ODDS_1 * STAKE_1);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(), addedCoins, true);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveFullTime_WhenBetOnHome_AndLose(){
        FbBet bet = getSampleBet(CriterionType.FULL_TIME, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_AWAY + ":1", TEAM_AWAY + ":2");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.FULL_TIME.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Lose);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveDrawNoBet_ReturnedBet_WhenBetOnHome_AndWasDraw(){
        FbBet bet = getSampleBet(CriterionType.DRAW_NO_BET, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_AWAY + ":1",
            TEAM_AWAY + ":2", TEAM_HOME + ":2");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.DRAW_NO_BET.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.NoBet);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(), STAKE_1, false );
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveDrawNoBet_LostBet_WhenBetOnHome_AndAwayWins(){
        FbBet bet = getSampleBet(CriterionType.DRAW_NO_BET, FbBet.Outcome.OT_ONE);

        Map<String, Integer> score = new HashMap<>();
        score.put(String.valueOf(TEAM_HOME),1);
        score.put(String.valueOf(TEAM_AWAY),2);

        FbMatch match = buildSoccerMatch(StatusCode.Ended, null, score);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.DRAW_NO_BET.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Lose);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveHalfTimeBet_WinBet_WhenBetOnHome_AndHomeWin(){
        FbBet bet = getSampleBet(CriterionType.HALF_TIME, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_HOME + ":1", TEAM_HOME + ":1");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.HALF_TIME.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Win);
        int addedCoins = (int)(ODDS_1 * STAKE_1);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(), addedCoins, true );
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveHalfTimeNoBet_ReturnBet_WhenBetOnHome_AndWasDraw(){
        FbBet bet = getSampleBet(CriterionType.DRAW_NO_BET_1H, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_AWAY + ":1");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.DRAW_NO_BET_1H.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.NoBet);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(), STAKE_1, false);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveLastGoalBet_Win_WhenBetOnHomeGoal_AndHomeGoalLast(){
        FbBet bet = getSampleBet(CriterionType.LAST_GOAL_NG_NB, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_AWAY + ":1", TEAM_HOME + ":1");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.LAST_GOAL_NG_NB.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Win);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(),  (int)(ODDS_1 * STAKE_1), true);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveLastGoalBet_Lose_WhenBetOnHomeGoal_AndAwayScoresLast(){
        FbBet bet = getSampleBet(CriterionType.LAST_GOAL_NG_NB, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_AWAY + ":1");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.LAST_GOAL_NG_NB.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Lose);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveLastGoalBet_NoBet_WhenBetOnHomeGoal_AndNoScore(){
        FbBet bet = getSampleBet(CriterionType.LAST_GOAL_NG_NB, FbBet.Outcome.OT_ONE);

        FbMatch match = buildSoccerMatch(StatusCode.Ended, null, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.LAST_GOAL_NG_NB.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.NoBet);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(),  STAKE_1, false);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveSecondHalfBet_Win_WhenBetOnHome_AndHomeWons(){
        FbBet bet = getSampleBet(CriterionType.SECOND_HALF, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_HOME + ":2",
            TEAM_AWAY + ":2", TEAM_HOME + ":2");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.SECOND_HALF.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Win);
        int addedCoins = (int)(ODDS_1 * STAKE_1);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(), addedCoins , true);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }


    @Test
    public void testResolveSecondHalfBet_NoBet_WhenBetOnHome_AndWasDraw(){
        FbBet bet = getSampleBet(CriterionType.DRAW_NO_BET_2H, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":1", TEAM_HOME + ":1",
            TEAM_AWAY + ":2", TEAM_HOME + ":2");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.DRAW_NO_BET_2H.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.NoBet);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(), STAKE_1 , false);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveFirstGoal_Win_WhenBetOnHome_AndHomeGoalFirst(){
        FbBet bet = getSampleBet(CriterionType.FIRST_GOAL_NG_NB, FbBet.Outcome.OT_ONE);

        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_HOME + ":2", TEAM_AWAY + ":2", TEAM_HOME + ":2");
        FbMatch match = buildSoccerMatch(StatusCode.Ended, goals, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.FIRST_GOAL_NG_NB.name())).thenReturn(true).thenReturn(false);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Win);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(),  (int)(ODDS_1 * STAKE_1), true);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveFirstGoal_NoBet_WhenBetOnHome_AndNoScore(){
        FbBet bet = getSampleBet(CriterionType.FIRST_GOAL_NG_NB, FbBet.Outcome.OT_ONE);
        FbMatch match = buildSoccerMatch(StatusCode.Ended, null, null);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.FIRST_GOAL_NG_NB.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.NoBet);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(),  STAKE_1, false);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testResolveFirstGoal_NotResolved_WhenNoGoals_AndMatchNotEnded(){
        FbBet bet = getSampleBet(CriterionType.FIRST_GOAL_NG_NB, FbBet.Outcome.OT_ONE);
        FbMatch match = buildSoccerMatch(StatusCode.SecondHalf, null, null);

        service.addBetToMatch(match.getId(), bet);

        service.resolve(match);

        Mockito.verifyZeroInteractions(fbservice);
        Mockito.verifyZeroInteractions(notificationService);
    }


    @Test
    public void testReturnAllUsers_ThatBetOnMatch(){
        final String USER_1 = "user_1";
        final String USER_2 = "user_2";

        FbBet bet1 = FbBet.builder().criterionId(CriterionType.FULL_TIME.id).userId(USER_1).build();
        FbBet bet2 = FbBet.builder().criterionId(CriterionType.HALF_TIME.id).userId(USER_2).build();

        service.addBetToMatch(MATCH_ID, bet1);
        service.addBetToMatch(MATCH_ID, bet2);

        Set<String> set =  service.getBetUsersByMatchId(MATCH_ID);
        assertEquals(2, set.size());
        assertTrue(set.contains(USER_1));
        assertTrue(set.contains(USER_2));
    }

    @Test
    public void testDartsResolveMatchOutcome_Win_WhenBetOnFirst_AndFirstWins(){
        FbBet bet = getSampleBet(CriterionType.MATCH_OUTCOME, FbBet.Outcome.OT_ONE);

        Map<String, Integer> score = new HashMap<>();
        score.put(String.valueOf(TEAM_HOME), 14);
        score.put(String.valueOf(TEAM_AWAY), 10);

        FbMatch match = getSampleDartsMatch(StatusCode.Ended, score);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.MATCH_OUTCOME.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Win);
        Mockito.verify(fbservice).updateUserCoins(bet.getUserId(),  (int)(ODDS_1 * STAKE_1), true);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testDartsResolveMatchOutcome_Lose_WhenBetOnFirst_AndSecondWins(){
        FbBet bet = getSampleBet(CriterionType.MATCH_OUTCOME, FbBet.Outcome.OT_ONE);

        Map<String, Integer> score = new HashMap<>();
        score.put(String.valueOf(TEAM_HOME), 14);
        score.put(String.valueOf(TEAM_AWAY), 20);

        FbMatch match = getSampleDartsMatch(StatusCode.Ended, score);

        service.addBetToMatch(match.getId(), bet);

        Mockito.when(repository.incrementMatchBet(MATCH_ID, CriterionType.MATCH_OUTCOME.name())).thenReturn(true);

        service.resolve(match);

        Mockito.verify(fbservice).updateBetStatus(bet.getId(),FbBet.Status.Lose);
        Mockito.verifyNoMoreInteractions(fbservice);
        Mockito.verify(notificationService).notifyBetResult(bet, match);
    }

    @Test
    public void testReturnEmptyList_OfUsers_WhenMatchIdNotExists(){
        Set<String> set =  service.getBetUsersByMatchId("1212");
        assertTrue(set.isEmpty());
    }

    private FbBet getSampleBet(CriterionType type, FbBet.Outcome outcome) {
        return FbBet.builder()
            .id("beTId1022")
            .criterionId(type.id)
            .matchId(MATCH_ID)
            .odds(ODDS_1)
            .stake(STAKE_1)
            .status(FbBet.Status.Active)
            .userId("userIDDDD")
            .on(outcome).build();
    }

    private FbSoccerMatch buildSoccerMatch(StatusCode code, List<FbSoccerMatch.Goal> goals, Map<String, Integer> score) {

        List<Contestant> contestants = Arrays.asList(
            Contestant.builder().id(TEAM_HOME).position(Constants.HOME).build(),
            Contestant.builder().id(TEAM_AWAY).position(Constants.AWAY).build()
        );

        FbSoccerMatch.SoccerLiveData liveData = FbSoccerMatch.SoccerLiveData.builder().goals(goals).build();
        liveData.setScore(score);

        FbSoccerMatch fbMatch = new FbSoccerMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setContestants(contestants);
        fbMatch.setStatusCode(code.statusCode);
        fbMatch.setLiveData(liveData);

        return fbMatch;
    }

    private FbDartsMatch getSampleDartsMatch(StatusCode code, Map<String, Integer> score) {

        List<Contestant> contestants = Arrays.asList(
            Contestant.builder().id(TEAM_HOME).position("1").build(),
            Contestant.builder().id(TEAM_AWAY).position("2").build()
        );

        FbDartsMatch.DartsLiveData liveData = FbDartsMatch.DartsLiveData.builder().score(score).build();

        FbDartsMatch fbMatch = new FbDartsMatch();
        fbMatch.setId(MATCH_ID);
        fbMatch.setContestants(contestants);
        fbMatch.setStatusCode(code.statusCode);
        fbMatch.setLiveData(liveData);
        return fbMatch;
    }

}
