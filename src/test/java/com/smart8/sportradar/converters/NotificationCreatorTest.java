package com.smart8.sportradar.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smart8.sportradar.converters.live.DartsLiveMatchConverter;
import com.smart8.sportradar.converters.live.SoccerLiveMatchConverter;
import com.smart8.sportradar.enums.MatchEventCounter;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.FbFriendRequest;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament;
import com.smart8.sportradar.models.FcmMessage;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.utils.ObjectUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import java.io.IOException;
import java.util.Collections;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class NotificationCreatorTest {

    private final static String FULL_DARTS_WITH_SETS = "sportradar/livefeeds/full-darts-with-periods.xml";
    private final static String FULL_DARTS_WITH_LEGS_STARTED = "sportradar/livefeeds/full-darts-with-legs.xml";
    private final static String FULL_SOCCER_WITH_LEGS = "sportradar/livefeeds/full-soccer-with-penalty-shootout.xml";

    @InjectMocks
    private SoccerLiveMatchConverter soccerConverter;

    @InjectMocks
    private DartsLiveMatchConverter dartsConverter;

    @InjectMocks
    private NotificationCreator creator;

    @Mock
    private MessageSource messageSource;

    @Mock
    @SuppressWarnings("unused")
    private ObjectMapper mapper;

    @Mock
    private FirebaseService service;

    @Before
    public void setUp() {
        soccerConverter = new SoccerLiveMatchConverter();
        dartsConverter = new DartsLiveMatchConverter();
        creator = new NotificationCreator();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDartsWin_NotificaitionCreation() throws IOException {

        BetradarLivescoreData data = TestHelper.getSportRadarLiveDataFromXml(FULL_DARTS_WITH_SETS);
        BetradarLivescoreData.Sport.Category.Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
        Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
        FbMatch fbMatch = dartsConverter.convert(tournament, match);

        final String SET_WIN = "Set win";
        Locale locale = new Locale("en");
        when(messageSource.getMessage("match.notify.setwin", null, locale)).thenReturn(SET_WIN);
        FcmMessage notification = creator.buildMatchNotification(MatchEventCounter.Sets, fbMatch, locale);

        assertEquals("Pipe, Justin 3:2 Smith, Bernie", notification.getNotification().getTitle());
        assertEquals(FcmMessage.MessageType.event, notification.getData().getType());
        assertEquals(SET_WIN, notification.getNotification().getBody());
    }

    @Test
    public void testFriendRequest_NotificationCreation_FriendRequsted() {
        final String USER_TOKEN = "token_dsasdasd";
        final String INVITER = "Inviter Name";
        final String INVITER_ID = "InviterId98asdfasdGdsf";

        FbFriendRequest request = new FbFriendRequest();
        request.setInviter(INVITER);
        request.setInviterId(INVITER_ID);
        request.setStatus(FbFriendRequest.InviteStatus.REQUESTED);

        final String FRIEND_REQUEST = "Friend request";
        Locale locale = new Locale("en");
        when(messageSource.getMessage("users.notify.friend.request", null, locale)).thenReturn(FRIEND_REQUEST);

        FcmMessage notification = creator.buildFriendRequestNotification(USER_TOKEN, request, locale);

        assertEquals(FRIEND_REQUEST, notification.getNotification().getTitle());
        assertEquals(Collections.singletonList(USER_TOKEN), notification.getRegistration_ids());
    }

    @Test
    public void testFriendRequest_NotificationCreation_FriendAccpted() {
        final String USER_TOKEN = "token_dsasdasd";
        final String INVITER = "Inviter Name";
        final String INVITED = "InviteD Name";
        final String INVITER_ID = "InviterId98asdfasdGdsf";

        FbFriendRequest request = new FbFriendRequest();
        request.setInviter(INVITER);
        request.setInviterId(INVITER_ID);
        request.setInvited(INVITED);
        request.setStatus(FbFriendRequest.InviteStatus.ACCEPTED);

        final String FRIEND_REQUEST = "Friend request";
        Locale locale = new Locale("en");
        when(messageSource.getMessage("users.notify.friend.accepted", null, locale)).thenReturn(FRIEND_REQUEST);

        FcmMessage notification = creator.buildFriendRequestNotification(USER_TOKEN, request, locale);

        assertEquals(FRIEND_REQUEST, notification.getNotification().getTitle());
        assertEquals(INVITED, notification.getNotification().getBody());
        assertEquals(Collections.singletonList(USER_TOKEN), notification.getRegistration_ids());
    }

    @Test
    public void testSoccerFirstHalfEvent_NotificaitionCreation() throws IOException {

        BetradarLivescoreData data = TestHelper.getSportRadarLiveDataFromXml(FULL_SOCCER_WITH_LEGS);
        BetradarLivescoreData.Sport.Category.Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
        Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
        FbMatch fbMatch = soccerConverter.convert(tournament, match);

        final String MATCH_STARTS = "Match started";
        Locale locale = new Locale("en");
        when(messageSource.getMessage("match.notify.starts", null, locale)).thenReturn(MATCH_STARTS);
        FcmMessage notification = creator.buildMatchNotification(MatchEventCounter.StartFirst, fbMatch, locale);

        assertEquals("FC Twente Enschede : Ajax Amsterdam", notification.getNotification().getTitle());
        assertEquals(FcmMessage.MessageType.event, notification.getData().getType());
        assertEquals(MatchEventCounter.StartFirst.name(), notification.getData().getEventType());
        assertEquals(SportType.Soccer.name().toLowerCase(), notification.getData().getSportType());
        assertEquals(MATCH_STARTS, notification.getNotification().getBody());
    }

    @Test
    public void testDartsLegWin_NotificaitionCreation() throws IOException {

        BetradarLivescoreData data = TestHelper.getSportRadarLiveDataFromXml(FULL_DARTS_WITH_LEGS_STARTED);
        BetradarLivescoreData.Sport.Category.Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
        Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
        FbMatch fbMatch = dartsConverter.convert(tournament, match);
        ObjectUtils.displayObj(fbMatch);

        final String LEG_WIN = "Leg win";
        Locale locale = new Locale("en");
        when(messageSource.getMessage("match.notify.legwin", null, locale)).thenReturn(LEG_WIN);
        FcmMessage notification = creator.buildMatchNotification(MatchEventCounter.Legs, fbMatch, locale);

        ObjectUtils.displayObj(notification);
        assertEquals("Pipe, Justin 4:2 Smith, Bernie", notification.getNotification().getTitle());
        assertEquals(FcmMessage.MessageType.event, notification.getData().getType());
        assertEquals(MatchEventCounter.Legs.name(), notification.getData().getEventType());
        assertEquals(SportType.Darts.name().toLowerCase(), notification.getData().getSportType());
        assertEquals(LEG_WIN, notification.getNotification().getBody());
    }
}
