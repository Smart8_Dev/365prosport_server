package com.smart8.sportradar.converters;

import com.smart8.sportradar.converters.live.DartsLiveMatchConverter;
import com.smart8.sportradar.converters.live.SoccerLiveMatchConverter;
import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbTournament;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport;
import com.smart8.sportradar.services.BetResolveService;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.handlers.MatchEventsHandler;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class DocumentToFbDataConverterTest {

    private  static BetradarLivescoreData data;
    private DocumentToFbDataConverter converter;

    @Mock
    private FirebaseService fbService;

    @Mock
    private MatchEventsHandler matchEventsService;

    @Mock
    private BetResolveService betResolveService;

    @InjectMocks
    private SoccerLiveMatchConverter soccerConverter;

    @InjectMocks
    private DartsLiveMatchConverter dartsConverter;

    @BeforeClass
    public static void before() throws IOException {
        data = TestHelper.getSportRadarLiveDataFromXml("sportradar/livefeeds/future-live-feed.xml");
    }

    @Before
    public void setUp(){

        soccerConverter = new SoccerLiveMatchConverter();
        dartsConverter = new DartsLiveMatchConverter();
        MockitoAnnotations.initMocks(this);
        converter = new DocumentToFbDataConverter(fbService, matchEventsService, betResolveService, soccerConverter, dartsConverter);

    }


    @Test
    public void testDataConversion_ShouldSplitDataBySport(){
        List<FbMatch> soccerMatches = new ArrayList<>();
        List<FbMatch> dartsMatches = new ArrayList<>();

        for( Sport sport : data.getSport()) {
            for (Sport.Category category : sport.getCategory()){
                for (BetradarLivescoreData.Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {

                        if ( sport.getBetradarSportId().intValue() == 1){
                            FbMatch fbMatch = soccerConverter.convert(tournament, match);
                            soccerMatches.add(fbMatch);
                        }else if ( sport.getBetradarSportId().intValue() == 22 ){
                            FbMatch fbMatch = dartsConverter.convert(tournament, match);
                            dartsMatches.add(fbMatch);
                        }
                    }
                }
            }
        }
        FbTournament fbTournament = FbTournament.builder().build();

        Mockito.when(fbService.isAllowed(any(BetradarLivescoreData.Sport.Category.Tournament.class))).thenReturn(true);
        Mockito.when(fbService.getTournament(anyString())).thenReturn(fbTournament);

        converter.convert(data);

        Mockito.verify(fbService).saveMatches(soccerMatches, SportType.Soccer.name().toLowerCase());
        Mockito.verify(fbService).saveMatches(dartsMatches, SportType.Darts.name().toLowerCase());
    }
}
