package com.smart8.sportradar.converters;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.helpers.MatchBuilderHelper;
import com.smart8.sportradar.utils.ConverterHelpers;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SoccerConverterTest {

    private final String TEAM_1 = "101";
    private final String TEAM_2 = "202";
    @Test
    public void testPeriodByTime(){
        int period = SoccerConverter.getPeriodIdByTime(90);
        assertEquals( 2, period);

        period = SoccerConverter.getPeriodIdByTime(0);
        assertEquals( 1, period);

        period = SoccerConverter.getPeriodIdByTime(45);
        assertEquals( 1, period);

        period = SoccerConverter.getPeriodIdByTime(105);
        assertEquals( 3, period);

        period = SoccerConverter.getPeriodIdByTime(120);
        assertEquals( 4, period);

        period = SoccerConverter.getPeriodIdByTime(15);
        assertEquals( 1, period);

        period = SoccerConverter.getPeriodIdByTime(87);
        assertEquals( 2, period);
    }

    @Test
    public void testRelevantByPeriodTime(){
        int period = SoccerConverter.getTimeRelevantByPeriod(90);
        assertEquals( 45, period);

        period = SoccerConverter.getTimeRelevantByPeriod(0);
        assertEquals( 0, period);

        period = SoccerConverter.getTimeRelevantByPeriod(45);
        assertEquals( 45, period);

        period = SoccerConverter.getTimeRelevantByPeriod(105);
        assertEquals( 15, period);

        period = SoccerConverter.getTimeRelevantByPeriod(120);
        assertEquals( 15, period);

        period = SoccerConverter.getTimeRelevantByPeriod(15);
        assertEquals( 15, period);

        period = SoccerConverter.getTimeRelevantByPeriod(87);
        assertEquals( 42, period);
    }

    @Test
    public void testPlayerNumberPositionToStringConversion(){
        assertNull( SoccerConverter.convertPlayerNumberPosition(null));
        assertNull(SoccerConverter.convertPlayerNumberPosition(""));
        assertEquals("G", SoccerConverter.convertPlayerNumberPosition("1"));
        assertEquals("D", SoccerConverter.convertPlayerNumberPosition("2"));
        assertEquals("D", SoccerConverter.convertPlayerNumberPosition("3"));
        assertEquals("D", SoccerConverter.convertPlayerNumberPosition("4"));
        assertEquals("D", SoccerConverter.convertPlayerNumberPosition("5"));
        assertEquals("M", SoccerConverter.convertPlayerNumberPosition("6"));
        assertEquals("M", SoccerConverter.convertPlayerNumberPosition("7"));
        assertEquals("M", SoccerConverter.convertPlayerNumberPosition("8"));
        assertEquals("M", SoccerConverter.convertPlayerNumberPosition("9"));
        assertEquals("F", SoccerConverter.convertPlayerNumberPosition("10"));
        assertEquals("F", SoccerConverter.convertPlayerNumberPosition("11"));
    }

    @Test
    public void testCreateTeamCode(){

        final String BAR = "Barcelona";
        final String name2 = "Munchester United";
        final String name3 = "Dynamo Kiev Fc";
        final String name4 = "D Full";
        final String name5 = "Df F";
        final String name6 = "F.C. Dyk";
        final String name7 = "F F";
        final String PSG = "Paris Saint-Germain F.C.";

        assertEquals("BAR", ConverterHelpers.createTeamCode(BAR));
        assertEquals("MUU", ConverterHelpers.createTeamCode(name2));
        assertEquals("DKF", ConverterHelpers.createTeamCode(name3));
        assertEquals("DFU", ConverterHelpers.createTeamCode(name4));
        assertEquals("DFF", ConverterHelpers.createTeamCode(name5));
        assertEquals("FCD", ConverterHelpers.createTeamCode(name6));
        assertEquals("FF",  ConverterHelpers.createTeamCode(name7));
        assertEquals("PSG", ConverterHelpers.createTeamCode(PSG));
        assertNull("Should be null", ConverterHelpers.createTeamCode(""));
        assertNull(ConverterHelpers.createTeamCode(null));

    }

    @Test
    public void testNameAsTeamCodeConversion(){
        final String KIM = "King, Mervyn";
        final String VDP = "Van De Pas, Benito";
        final String VGM = "van Gerwen, Michael";

        assertEquals("KIM", ConverterHelpers.createTeamCode(KIM));
        assertEquals("VDP", ConverterHelpers.createTeamCode(VDP));
        assertEquals("VGM", ConverterHelpers.createTeamCode(VGM));
    }

    @Test
    public void testWhoWin_HomeWin(){
        final long TEAM_ID_1 = 1L;
        final long TEAM_ID_2 = 2L;
        List<Contestant> contestants = Arrays.asList(
            Contestant.builder().id(TEAM_ID_1).position(Constants.HOME).build(),
            Contestant.builder().id(TEAM_ID_2).position(Constants.AWAY).build()
        );
        Map<String, Integer> score = new HashMap<>();
        score.put(String.valueOf(TEAM_ID_1), 2);
        score.put(String.valueOf(TEAM_ID_2), 1);

        FbSoccerMatch.SoccerLiveData liveData = FbSoccerMatch.SoccerLiveData.builder().score(score).build();
        FbSoccerMatch match = new FbSoccerMatch();
        match.setContestants(contestants);
        match.setLiveData(liveData);

        assertEquals(1, SoccerConverter.whoWonPeriod(match, 0));
    }

    @Test
    public void testWhoWonFirstPeriod_WhenHomeWinsFirst_AndAwayWinsSecond(){
        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_1 + ":1", TEAM_2 + ":2", TEAM_2 + ":2" );
        FbSoccerMatch match = buildMatch(goals, null);

        assertEquals(1, SoccerConverter.whoWonPeriod(match, 1));
        assertEquals(-1, SoccerConverter.whoWonPeriod(match, 2));
    }

    @Test
    public void testWhoWonFirstPeriod_WhenDraw_AndAwayWinsSecond(){
        List<FbSoccerMatch.Goal> goals = MatchBuilderHelper.createGoalsByTeamIdAndPeriod(TEAM_1 + ":1", TEAM_2 + ":1", TEAM_2 + ":2" );

        FbSoccerMatch match = buildMatch(goals, null);
        assertEquals(0, SoccerConverter.whoWonPeriod(match, 1));
        assertEquals(-1, SoccerConverter.whoWonPeriod(match, 2));
    }

    @Test
    public void testWhoWin_AwayWin(){
        Map<String, Integer> score = new HashMap<>();
        score.put(String.valueOf(TEAM_1), 1);
        score.put(String.valueOf(TEAM_2), 2);

        FbSoccerMatch match = buildMatch(null, score);

        assertEquals(-1, SoccerConverter.whoWonPeriod(match, 0));
    }

    @Test
    public void testWhoWin_Draw(){
        Map<String, Integer> score = new HashMap<>();
        score.put(String.valueOf(TEAM_1), 2);
        score.put(String.valueOf(TEAM_2), 2);

        FbSoccerMatch match = buildMatch(null, score);
        assertEquals(0, SoccerConverter.whoWonPeriod(match, 0));
    }


    @Test
    public void testPeriodToMatchTimeConvertion(){
        final int time1 = SoccerConverter.convertPeriodTimeToMatchTimeString(1, 10);
        assertEquals(10, time1);

        final int time2 = SoccerConverter.convertPeriodTimeToMatchTimeString(2, 10);
        assertEquals(55, time2);

        final int time3 = SoccerConverter.convertPeriodTimeToMatchTimeString(3, 10);
        assertEquals(100, time3);

        final int time4 = SoccerConverter.convertPeriodTimeToMatchTimeString(2, 50);
        assertEquals(95, time4);

        final int time5 = SoccerConverter.convertPeriodTimeToMatchTimeString(2, 3);
        assertEquals(48, time5);
    }


    private FbSoccerMatch buildMatch(List<FbSoccerMatch.Goal> goals,  Map<String, Integer> score){
        List<Contestant> contestants = Arrays.asList(
            Contestant.builder().id(Long.valueOf(TEAM_1)).position(Constants.HOME).build(),
            Contestant.builder().id(Long.valueOf(TEAM_2)).position(Constants.AWAY).build()
        );

        FbSoccerMatch.SoccerLiveData liveData = FbSoccerMatch.SoccerLiveData.builder()
            .goals(goals)
            .score(score)
            .build();
        FbSoccerMatch match = new FbSoccerMatch();
        match.setContestants(contestants);
        match.setLiveData(liveData);
        return match;
    }

}
