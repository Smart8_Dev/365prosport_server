package com.smart8.sportradar.converters.live;

import com.smart8.sportradar.Constants;
import com.smart8.sportradar.converters.SoccerConverter;
import com.smart8.sportradar.firebase.FbDartsMatch;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.firebase.FbTeam;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament;
import com.smart8.sportradar.livemodels.BetradarLivescoreData.Sport.Category.Tournament.Match.Lineups.TeamPlayer;
import com.smart8.sportradar.livemodels.TStatistics;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.StringUtils;

import javax.xml.bind.JAXBElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@Slf4j
@RunWith(Enclosed.class)
public class LiveMatchConverterTest {

    private static BetradarLivescoreData data;


    @RunWith(Parameterized.class)
    public static class ParameterizedPart {


        @Mock
        private FirebaseService service;

        @InjectMocks
        private SoccerLiveMatchConverter soccerConverter;

        @InjectMocks
        private DartsLiveMatchConverter dartsConverter;

        private String testFeedName;

        public ParameterizedPart(String testFeedName) {
            this.testFeedName = testFeedName;
        }

        @Parameterized.Parameters
        public static Collection testFeedNames() {
            return Arrays.asList(
                "sportradar/livefeeds/delta-live-feed.xml",
                "sportradar/livefeeds/delta-live-feed-2.xml",
                "sportradar/livefeeds/full-live-feed.xml",
                "sportradar/livefeeds/delta-live-feed-argentinos-arsenal.xml",
                "sportradar/livefeeds/delta-live-feed-darts-n-soccer.xml",
                "sportradar/livefeeds/full-live-feed-darts-n-soccer.xml"


            );
        }

        @Before
        public void setUp() throws IOException {
            log.info("Test file: " + testFeedName);
            data = TestHelper.getSportRadarLiveDataFromXml(testFeedName);
            soccerConverter = new SoccerLiveMatchConverter();
            dartsConverter = new DartsLiveMatchConverter();
            MockitoAnnotations.initMocks(this);

            when(service.getTeam(any())).thenReturn(FbTeam.builder().build());
        }

        @Test
        public void testFbMatch_Soccer() throws IOException {
            for (Sport.Category category : data.getSport().get(0).getCategory()) {
                for (Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {
                        log.info("Testing match id: " + match.getId());

                        FbMatch fbMatch = soccerConverter.convert(tournament, match);

                        assertEquals(match.getId().toString(), fbMatch.getId());
                        assertEquals(2, fbMatch.getContestants().size());

                        for (int i = 0; i < fbMatch.getContestants().size(); i++) {
                            assertFalse(StringUtils.isEmpty(fbMatch.getContestants().get(i).getName()));
                            assertFalse(StringUtils.isEmpty(fbMatch.getContestants().get(i).getCode()));
                            assertTrue(fbMatch.getContestants().get(i).getCode().length() <= 3);
                        }


                        assertEquals(ConverterHelpers.parseDateOfMatchString(match.getMatchDate()), fbMatch.getDateTime());
                        assertEquals(Long.valueOf(ConverterHelpers.parseMatchDateTimeToLong(match.getMatchDate())), fbMatch.getDateTimeInt());

                        if (match.getStatus().getCode().intValue() == 100) {
                            assertEquals("Game ended time incorrect", Long.valueOf(ConverterHelpers.parseMatchDateTimeToLong(match.getCurrentPeriodStart())), fbMatch.getGameEndedInt());
                        }

                        if (match.getStatus().getCode().intValue() != 60) {
                            assertFalse(fbMatch.getPostponed());
                        } else {
                            assertTrue(fbMatch.getPostponed());
                        }

                        if (match.getStatus().getCode().intValue() == 70) {
                            assertTrue(fbMatch.isCanceled());
                        } else {
                            assertFalse(fbMatch.isCanceled());
                        }

                        if (match.getReferee() != null) {
                            assertEquals(match.getReferee().getName2(), fbMatch.getReferee().get(0).getName());
                        } else {
                            assertNull(fbMatch.getReferee());
                        }

                        assertEquals(tournament.getUniqueTournamentId().intValue(), fbMatch.getTournamentId().intValue());
                        assertFalse(fbMatch.getTournamentName().isEmpty());
                        assertEquals(tournament.getName().get(0).getValue(), fbMatch.getTournamentName());
                        assertNull(fbMatch.getSubTournamentId());

                        if (match.getVenue() != null) {
                            assertEquals(match.getVenue().getStadium().getId().longValue(), fbMatch.getVenueId().longValue());
                            assertFalse(fbMatch.getVenueName().isEmpty());
                            assertEquals(match.getVenue().getStadium().getName().get(0).getValue(), fbMatch.getVenueName());
                        }

                        assertEquals(match.getStatus().getCode().intValue(), fbMatch.getStatusCode());
                        assertFalse(fbMatch.getStatus().isEmpty());
                        assertEquals(StatusCode.getByCode(match.getStatus().getCode().intValue()).toString(), fbMatch.getStatus());
                        log.info(" - Success!");
                    }

                }
            }
        }

        @Test
        public void testFbMatch_SoccerLiveDataCreation() {
            for (Sport.Category category : data.getSport().get(0).getCategory()) {
                for (Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {
                        log.info("Testing match id: " + match.getId());
                        final String team1Id = match.getTeam1().getUniqueTeamId().toString();
                        final String team2Id = match.getTeam2().getUniqueTeamId().toString();
                        final FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);

                        final FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();

                        int statusCode = match.getStatus().getCode().intValue();
                        if (statusCode == 100) {
                            assertEquals(90, fbMatch.getLiveData().getMinutes());
                        }
                        if (statusCode == 0 || statusCode == 60 || statusCode == 70) {
                            assertNull(liveData.getScore());
                            assertNull(liveData.getGoals());
                            assertNull(liveData.getCards());
                            assertNull(liveData.getSubs());
                        } else {
                            assertEquals(match.getScores().getScore().get(0).getTeam1().getValue().intValue(), liveData.getScore().get(team1Id).intValue());
                            assertEquals(match.getScores().getScore().get(0).getTeam2().getValue().intValue(), liveData.getScore().get(team2Id).intValue());
                        }
                        log.info(" - Success!");
                    }

                }
            }
        }

        @Test
        public void testFbMatch_GoalsCreation_Soccer() {
            for (Sport.Category category : data.getSport().get(0).getCategory()) {
                for (Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {
                        log.info("Testing match id: " + match.getId());

                        final FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
                        final FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();

                        if (match.getGoals() != null) {
                            assertEquals(match.getGoals().getGoal().size(), liveData.getGoals().size());

                            for (int i = 0; i < match.getGoals().getGoal().size(); i++) {

                                long scoringTeamId = match.getTeam1().getUniqueTeamId().longValue();
                                if (match.getGoals().getGoal().get(i).getScoringTeam().intValue() == 2) {
                                    scoringTeamId = match.getTeam2().getUniqueTeamId().longValue();
                                }

                                assertEquals(scoringTeamId, liveData.getGoals().get(i).getContestantId());
                                assertEquals(SoccerConverter.getPeriodIdByTime(match.getGoals().getGoal().get(i).getTime().intValue()), liveData.getGoals().get(i).getPeriodId());
                                assertEquals(SoccerConverter.getTimeRelevantByPeriod(match.getGoals().getGoal().get(i).getTime().intValue()), liveData.getGoals().get(i).getTimeMin());

                                if (match.getGoals().getGoal().get(i).getPlayer() != null) {
                                    assertNotNull(liveData.getGoals().get(i).getScorer());
                                    assertEquals(match.getGoals().getGoal().get(i).getPlayer().getContent().get(0), liveData.getGoals().get(i).getScorer());
                                } else {
                                    assertNull(liveData.getGoals().get(i).getScorer());
                                }

                                if (match.getGoals().getGoal().get(i).getAssist1() != null) {
                                    assertEquals("Assister name incorrect", match.getGoals().getGoal().get(i).getAssist1().getName(), liveData.getGoals().get(i).getAssist());
                                    assertEquals(match.getGoals().getGoal().get(i).getAssist1().getId(), liveData.getGoals().get(i).getAssistId().toString());
                                }
                            }
                        }

                        log.info(" - Success!");
                    }
                }
            }
        }

        @Test
        public void testFbMatch_CardCreation_Soccer() {
            for (Sport.Category category : data.getSport().get(0).getCategory()) {
                for (Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {
                        log.info("Testing match id: " + match.getId());

                        final FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
                        final FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();

                        if (match.getCards() != null) {
                            assertEquals(match.getCards().getCard().size(), liveData.getCards().size());
                            for (int i = 0; i < match.getCards().getCard().size(); i++) {
                                assertNotNull(liveData.getCards().get(i).getPlayer());
                                assertEquals(match.getCards().getCard().get(i).getPlayer().getContent().get(0), liveData.getCards().get(i).getPlayer());
                                assertEquals(SoccerConverter.getCardCode(match.getCards().getCard().get(i).getType()), liveData.getCards().get(i).getType());
                            }
                        } else {
                            assertNull(liveData.getCards());
                        }

                        log.info(" - Success!");
                    }
                }
            }
        }

        @Test
        public void testFbMatch_SubstitutionCreation_Soccer() {
            for (Sport.Category category : data.getSport().get(0).getCategory()) {
                for (Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {
                        log.info("Testing match id: " + match.getId());
                        final FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
                        final FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();
                        if (match.getSubstitutions() != null) {
                            for (int i = 0; i < match.getSubstitutions().getSubstitution().size(); i++) {
                                assertEquals(match.getSubstitutions().getSubstitution().size(), liveData.getSubs().size());
                                assertNotNull(liveData.getSubs().get(i).getPlayerOn());
                                assertNotNull(liveData.getSubs().get(i).getPlayerOff());
                                assertEquals(match.getSubstitutions().getSubstitution().get(i).getPlayerIn().getContent().get(0), liveData.getSubs().get(i).getPlayerOn());
                                assertEquals(match.getSubstitutions().getSubstitution().get(i).getPlayerOut().getContent().get(0), liveData.getSubs().get(i).getPlayerOff());
                            }
                        } else {
                            assertNull(liveData.getSubs());
                        }

                        log.info(" - Success!");
                    }
                }
            }
        }

        @Test
        public void testFbMatch_StatisticsCreation_Soccer() {
            for (Sport.Category category : data.getSport().get(0).getCategory()) {
                for (Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {

                        if (match.getId().intValue() != 11928086) {
                            continue;
                        }
                        log.info("Testing match id: " + match.getId());
                        final FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
                        final FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();

                        if (match.getStatistics() != null) {
                            assertEquals(2, liveData.getStats().size());
                            assertEquals(match.getTeam1().getUniqueTeamId().longValue(), liveData.getStats().get(0).getContestantId());
                            assertEquals(match.getTeam2().getUniqueTeamId().longValue(), liveData.getStats().get(1).getContestantId());


                            if (match.getStatistics().getBallPossessionOrShotsOnGoalOrShotsOffGoal().size() > 0) {
                                if (parseStatistic(match, "BallPossession", 0) != null) {
                                    assertEquals(100, liveData.getStats().get(0).getBallPossession() + liveData.getStats().get(1).getBallPossession());
                                }
                                assertEquals(parseStatistic(match, "ShotsOnGoal", 0), liveData.getStats().get(0).getShotsTarget());
                                assertEquals(parseStatistic(match, "ShotsOnGoal", 1), liveData.getStats().get(1).getShotsTarget());
                                assertEquals(parseStatistic(match, "FreeKicks", 0), liveData.getStats().get(0).getFreeKicks());
                                assertEquals(parseStatistic(match, "FreeKicks", 1), liveData.getStats().get(1).getFreeKicks());
                                assertEquals(parseStatistic(match, "CornerKicks", 0), liveData.getStats().get(0).getCornerKicks());
                                assertEquals(parseStatistic(match, "CornerKicks", 1), liveData.getStats().get(1).getCornerKicks());
                                assertEquals(parseStatistic(match, "Offsides", 0), liveData.getStats().get(0).getOffsides());
                                assertEquals(parseStatistic(match, "Offsides", 1), liveData.getStats().get(1).getOffsides());
                                assertEquals(parseStatistic(match, "Fouls", 0), liveData.getStats().get(0).getFouls());
                                assertEquals(parseStatistic(match, "Fouls", 1), liveData.getStats().get(1).getFouls());
                                assertEquals(parseStatistic(match, "ShotsOffGoal", 0), liveData.getStats().get(0).getShots());
                                assertEquals(parseStatistic(match, "ShotsOffGoal", 1), liveData.getStats().get(1).getShots());
                            }
                        } else {
                            assertNull(liveData.getStats());
                        }

                        log.info(" - Success!");
                    }
                }
            }
        }

        @Test
        public void testFbMatch_LineupsCreation_Soccer() {
            for (Sport.Category category : data.getSport().get(0).getCategory()) {
                for (Sport.Category.Tournament tournament : category.getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {
                        log.info("Testing match id: " + match.getId());
                        final FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
                        final FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();

                        assertEquals(2, liveData.getLineup().size());
                        assertEquals(match.getTeam1().getUniqueTeamId().longValue(), liveData.getLineup().get(0).getContestantId());
                        if (match.getTeam1().getFormation() != null) {
                            assertEquals(match.getTeam1().getFormation().replaceAll("-", ""), liveData.getLineup().get(0).getFormation());
                        } else {
                            assertEquals("442", liveData.getLineup().get(0).getFormation());
                        }

                        if (match.getTeam2().getFormation() != null) {
                            assertEquals(match.getTeam2().getUniqueTeamId().longValue(), liveData.getLineup().get(1).getContestantId());
                            assertEquals(match.getTeam2().getFormation().replaceAll("-", ""), liveData.getLineup().get(1).getFormation());
                        } else {
                            assertEquals("442", liveData.getLineup().get(1).getFormation());
                        }

                        if (match.getLineups() != null) {
                            List<TeamPlayer> homePlayers = parseTeamPlayers(match.getLineups(), 1);
                            for (int i = 0; i < homePlayers.size(); i++) {
                                assertEquals(homePlayers.get(i).getPlayer().getId().longValue(), liveData.getLineup().get(0).getPlayers().get(i).getId());
                                assertEquals(homePlayers.get(i).getShirtNumber().intValue(), liveData.getLineup().get(0).getPlayers().get(i).getNumber());
                                assertFalse(liveData.getLineup().get(0).getPlayers().get(i).getLastName().isEmpty());
                                if (homePlayers.get(i).getPos() == null){
                                    assertEquals(-1, liveData.getLineup().get(0).getPlayers().get(i).getPosition());
                                }else{
                                    assertEquals(Integer.parseInt(homePlayers.get(i).getPos()), liveData.getLineup().get(0).getPlayers().get(i).getPosition());
                                }

                            }

                            List<TeamPlayer> awayPlayers = parseTeamPlayers(match.getLineups(), 2);
                            for (int i = 0; i < awayPlayers.size(); i++) {
                                assertEquals(awayPlayers.get(i).getPlayer().getId().longValue(), liveData.getLineup().get(1).getPlayers().get(i).getId());
                                assertEquals(awayPlayers.get(i).getShirtNumber().intValue(), liveData.getLineup().get(1).getPlayers().get(i).getNumber());
                                assertFalse(liveData.getLineup().get(1).getPlayers().get(i).getLastName().isEmpty());
                                if (awayPlayers.get(i).getPos() == null){
                                    assertEquals(-1, liveData.getLineup().get(1).getPlayers().get(i).getPosition());
                                }else{
                                    assertEquals(Integer.parseInt(awayPlayers.get(i).getPos()), liveData.getLineup().get(1).getPlayers().get(i).getPosition());
                                }
                            }
                        } else {
                            assertNull(liveData.getLineup().get(0).getPlayers());
                            assertNull(liveData.getLineup().get(1).getPlayers());
                        }
                        log.info(" - Success!");
                    }
                }
            }
        }

        @Test
        public void testDartsParsing() {
            for (Sport sport : data.getSport()) {
                if (sport.getBetradarSportId().intValue() != Constants.DARTS_ID) {
                    continue;
                }
                for (Sport.Category.Tournament tournament : sport.getCategory().get(0).getTournament()) {
                    for (Sport.Category.Tournament.Match match : tournament.getMatch()) {
                        log.info("Testing darts match id: " + match.getId());

                        FbDartsMatch fbMatch = (FbDartsMatch)dartsConverter.convert(tournament, match);

                        assertEquals(match.getId().toString(), fbMatch.getId());
                        assertEquals(2, fbMatch.getContestants().size());

                        for (int i = 0; i < fbMatch.getContestants().size(); i++) {
                            assertFalse(StringUtils.isEmpty(fbMatch.getContestants().get(i).getName()));
                            assertFalse(StringUtils.isEmpty(fbMatch.getContestants().get(i).getCode()));
                            assertTrue(fbMatch.getContestants().get(i).getCode().length() <= 3);
                            assertNull(fbMatch.getContestants().get(i).getLastFive());
                            assertNull(fbMatch.getContestants().get(i).getLastFiveId());
                        }


                        assertEquals(ConverterHelpers.parseDateOfMatchString(match.getMatchDate()), fbMatch.getDateTime());
                        assertEquals(Long.valueOf(ConverterHelpers.parseMatchDateTimeToLong(match.getMatchDate())), fbMatch.getDateTimeInt());

                        if (match.getStatus().getCode().intValue() == 70) {
                            assertTrue(fbMatch.isCanceled());
                        } else {
                            assertFalse(fbMatch.isCanceled());
                        }

                        if (match.getStatus().getCode().intValue() == 100) {
                            assertEquals("Game ended time incorrect", Long.valueOf(ConverterHelpers.parseMatchDateTimeToLong(match.getCurrentPeriodStart())), fbMatch.getGameEndedInt());
                        }

                        if (match.getStatus().getCode().intValue() != 60) {
                            assertFalse(fbMatch.getPostponed());
                        } else {
                            assertTrue(fbMatch.getPostponed());
                        }

                        if (match.getStatus().getCode().intValue() == 20) {
                            assertTrue(fbMatch.getLiveData().getMinutes() > 0);
                            assertEquals(2, fbMatch.getLiveData().getScore().size());
                        }

                        assertEquals(tournament.getUniqueTournamentId().intValue(), fbMatch.getTournamentId().intValue());
                        assertFalse(fbMatch.getTournamentName().isEmpty());
                        assertEquals(tournament.getName().get(0).getValue(), fbMatch.getTournamentName());
                        assertNull(fbMatch.getSubTournamentId());

                        assertNull(match.getVenue());

                        assertEquals(match.getStatus().getCode().intValue(), fbMatch.getStatusCode());
                        assertFalse(fbMatch.getStatus().isEmpty());
                        assertEquals(StatusCode.getByCode(match.getStatus().getCode().intValue()).toString(), fbMatch.getStatus());

                        log.info(" - Success!");
                    }
                }
            }
        }
    }


    /**
     * Get statistic type value
     *
     * @param statType name of statistic type
     * @param teamNum  home team(0) or away team(1)
     * @return statistic type value
     */
    private static Integer parseStatistic(Tournament.Match match, String statType, int teamNum) {
        for (JAXBElement<?> entity : match.getStatistics().getBallPossessionOrShotsOnGoalOrShotsOffGoal()) {
            if (entity.getDeclaredType() == TStatistics.class) {
                TStatistics tStatistics = (TStatistics) entity.getValue();
                if (entity.getName().getLocalPart().toLowerCase().equalsIgnoreCase(statType)) {
                    return teamNum == 0 ? Integer.parseInt(tStatistics.getTeam1().getValue()) : Integer.parseInt(tStatistics.getTeam2().getValue());
                }
            }
        }
        return null;
    }

    private static List<TeamPlayer> parseTeamPlayers(Category.Tournament.Match.Lineups lineups, int home) {
        List<Object> teamPlayerOrTeamOfficial = lineups.getTeamPlayerOrTeamOfficial();

        List<TeamPlayer> playerList = new ArrayList<>();
        for (Object obj : teamPlayerOrTeamOfficial) {

            if (obj instanceof TeamPlayer) {
                Tournament.Match.Lineups.TeamPlayer player = (Sport.Category.Tournament.Match.Lineups.TeamPlayer) obj;

                if (player.getPlayerTeam().intValue() == home) {
                    playerList.add(player);
                }
            }
        }
        return playerList;
    }

    public static class NonParametrizePart {

        @Mock
        private FirebaseService service;

        @InjectMocks
        private SoccerLiveMatchConverter soccerConverter;

        @InjectMocks
        private DartsLiveMatchConverter dartsConverter;

        private final static String FUTURE_LIVE_FEED = "sportradar/livefeeds/full-live-feed.xml";
        private final static String DELTA_WITH_ADDED_TIMES_AND_AGG_SCORE = "sportradar/livefeeds/delta-with-added-time-n-agg-score.xml";
        private final static String FULL_DARTS_WITH_LEGS = "sportradar/livefeeds/full-darts-with-periods.xml";
        private final static String FULL_SOCCER_WITH_PENALTIES = "sportradar/livefeeds/full-soccer-with-penalty-shootout.xml";


        @Before
        public void setUp() throws IOException {
            soccerConverter = new SoccerLiveMatchConverter();
            dartsConverter = new DartsLiveMatchConverter();
            MockitoAnnotations.initMocks(this);
            when(service.getTeam(any())).thenReturn(FbTeam.builder().build());
        }

        @Test
        public void testLineupPositions() throws IOException {
            data = TestHelper.getSportRadarLiveDataFromXml(FUTURE_LIVE_FEED);

            //Get single match OSC Lille - Olympique Marseille
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert( tournament, match);

            assertEquals(2, fbMatch.getLiveData().getLineup().size());

            List<FbSoccerMatch.LineupPlayer> playersLille = fbMatch.getLiveData().getLineup().get(0).getPlayers();
            List<FbSoccerMatch.LineupPlayer> playersMarseille = fbMatch.getLiveData().getLineup().get(1).getPlayers();

            assertEquals(18, playersLille.size());
            assertEquals(4, playersLille.get(0).getPosition());
            assertEquals(5, playersLille.get(3).getPosition());
            assertEquals(Constants.NO_VALUE, playersLille.get(11).getPosition());

            assertEquals(11, playersLille.stream().filter(p -> p.getPosition() != Constants.NO_VALUE ).count());

            assertEquals(18, playersMarseille.size());
            assertEquals(6, playersMarseille.get(0).getPosition());
            assertEquals(5, playersMarseille.get(1).getPosition());
            assertEquals(8, playersMarseille.get(2).getPosition());
            assertEquals(Constants.NO_VALUE, playersMarseille.get(11).getPosition());

            assertEquals(11, playersMarseille.stream().filter(p -> p.getPosition() != Constants.NO_VALUE ).count());
        }

        @Test
        public void testMatchEvents_WithAddedTImes() throws IOException {
            data = TestHelper.getSportRadarLiveDataFromXml(DELTA_WITH_ADDED_TIMES_AND_AGG_SCORE);

            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
            FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();

            assertEquals( 45+2, liveData.getGoals().get(1).getTimeMin());
            assertEquals( 2, liveData.getGoals().get(1).getPeriodId());
            assertEquals(990063L, liveData.getGoals().get(1).getScorerId().longValue());

            assertEquals("Time should be with added time", 45+6, liveData.getCards().get(1).getTimeMin());
            assertEquals( 2, liveData.getCards().get(1).getPeriodId());
            assertEquals("YC", liveData.getCards().get(1).getType());

            assertEquals( 45+1, liveData.getSubs().get(0).getTimeMin());
            assertEquals( 1, liveData.getSubs().get(0).getPeriodId());
            assertEquals("Ovelar, Roberto", liveData.getSubs().get(0).getPlayerOn());

        }

        @Test
        public void testDartsLegResults() throws IOException {
            data = TestHelper.getSportRadarLiveDataFromXml(FULL_DARTS_WITH_LEGS);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
            FbDartsMatch fbMatch = (FbDartsMatch)dartsConverter.convert(tournament, match);
            assertEquals(5, fbMatch.getLiveData().getSets().size());
            assertEquals(3, fbMatch.getLiveData().getSets().get(0).get("35970").intValue());
            assertEquals(0, fbMatch.getLiveData().getSets().get(0).get("265337").intValue());
            assertEquals(2, fbMatch.getLiveData().getSets().get(1).size());
            assertEquals(2, fbMatch.getLiveData().getSets().get(2).size());
            assertEquals(2, fbMatch.getLiveData().getSets().get(3).size());
            assertEquals(2, fbMatch.getLiveData().getSets().get(4).size());
        }

        @Test
        public void testSoccerFullLive_WithPenaltyShootout() throws IOException {
            data = TestHelper.getSportRadarLiveDataFromXml(FULL_SOCCER_WITH_PENALTIES);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);

            assertEquals(14, fbMatch.getLiveData().getShootouts().size());
            assertEquals(1, fbMatch.getLiveData().getShootouts().get(0).getSequense());
            assertTrue(fbMatch.getLiveData().getShootouts().get(0).isScore());
            assertEquals("Assaidi, Oussama", fbMatch.getLiveData().getShootouts().get(0).getPlayer());
            assertEquals(12820L, fbMatch.getLiveData().getShootouts().get(2).getPlayerId().longValue());
            assertFalse(fbMatch.getLiveData().getShootouts().get(13).isScore());
        }

        @Test
        public void testSoccerAggregatedScore() throws IOException {
            data = TestHelper.getSportRadarLiveDataFromXml(DELTA_WITH_ADDED_TIMES_AND_AGG_SCORE);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            Tournament.Match match = data.getSport().get(0).getCategory().get(0).getTournament().get(0).getMatch().get(0);
            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);

            assertEquals(2, fbMatch.getLiveData().getAggScore().size());

            assertEquals(1, fbMatch.getLiveData().getAggScore().get("6105").intValue());
            assertEquals(4, fbMatch.getLiveData().getAggScore().get("5981").intValue());
        }
    }
}
