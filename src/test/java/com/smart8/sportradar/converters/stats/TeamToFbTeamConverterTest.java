package com.smart8.sportradar.converters.stats;

import com.smart8.sportradar.firebase.FbTeam;
import com.smart8.sportradar.statisticsmodels.Team;
import org.junit.Test;

import static org.junit.Assert.*;

public class TeamToFbTeamConverterTest {

    @Test
    public void fbTeamShoudBeUpdated(){
        final int id = 1001;
        final String teamName = "Sporting FC";

        final FbTeam fbTeam = FbTeam.builder().id(id + "").build();

        final Team radarTeam = new Team();
        radarTeam.setId(id);
        radarTeam.setName(teamName);

        TeamToFbTeamConverter converter = new TeamToFbTeamConverter();
        boolean response = converter.updateFbTeam(fbTeam,radarTeam);

        assertTrue(response);
        assertEquals(String.valueOf(id), fbTeam.getId());
        assertEquals("SPF", fbTeam.getCode());
        assertEquals(teamName, fbTeam.getName());
    }

    @Test
    public void fbTeamShoudNotBeUpdated(){
        final int id = 1001;
        final String teamName = "Sporting FC";

        final FbTeam fbTeam = FbTeam.builder().id(id + "").code("SPF").name(teamName).build();

        final Team radarTeam = new Team();
        radarTeam.setId(id);
        radarTeam.setName(teamName);

        TeamToFbTeamConverter converter = new TeamToFbTeamConverter();
        boolean response = converter.updateFbTeam(fbTeam,radarTeam);

        assertFalse(response);
    }
}
