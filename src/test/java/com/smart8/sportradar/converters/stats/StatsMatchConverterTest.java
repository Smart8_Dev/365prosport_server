package com.smart8.sportradar.converters.stats;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.fb.Contestant;
import com.smart8.sportradar.firebase.FbMatch;
import com.smart8.sportradar.firebase.FbSoccerMatch;
import com.smart8.sportradar.handlers.CancelledMatchHandler;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.services.StatusCode;
import com.smart8.sportradar.statisticsmodels.*;
import com.smart8.sportradar.utils.ConverterHelpers;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@Slf4j
@RunWith(Enclosed.class)
public class StatsMatchConverterTest {

    private final static String MATCH_DETAIL_FEED = "sportradar/statfeeds/matchdetail-soccer-feed.xml";
    private final static String MATCH_DETAIL_FEED_NO_PLAYERS = "sportradar/statfeeds/match-detail-with-no-goal-info.xml";
    private final static String MATCH_DETAIL_FEED_NO_PLAYER_INFO = "sportradar/statfeeds/matchdetail-soccer-no-playerinfo.xml";
    private final static String MATCH_DETAIL_WITH_INJURY_TIME = "sportradar/statfeeds/matchdetail-with-added-times.xml";
    private final static String MATCH_DETAIL_POSPONED = "sportradar/statfeeds/matchdetail-soccer-postponed.xml";
    private final static String PREMATCH_FEED = "sportradar/statfeeds/prematch-soccer-feed.xml";
    private final static String PREMATCH_FEED_2 = "sportradar/statfeeds/prematch-soccer-feed2.xml";
    private final static String PREMATCH_FEED_NO_MATCHES = "sportradar/statfeeds/prematch-soccer-feed-no-lastmatches.xml";
    private final static String DARTS_PREMATCH_FEED = "sportradar/statfeeds/prematch-darts-feed.xml";
    private final static String MATCHDETAIL_SOCCER_WITH_OVERTIME = "sportradar/statfeeds/matchdetail-soccer-with-overtime.xml";



    @RunWith(Parameterized.class)
    public static class TheParameterizedPart {

        private String testFeedName;

        @InjectMocks
        private SoccerStatMatchConverter soccerConverter;

        @InjectMocks
        private DartsStatMatchConverter dartsConverter;

        @Before
        public void setUp(){
            MockitoAnnotations.initMocks(this);
        }

        public TheParameterizedPart(String testFeedName) {
            this.testFeedName = testFeedName;
        }

        @Parameterized.Parameters
        public static Collection testFeedNames() {
            return Arrays.asList(
                PREMATCH_FEED,
                PREMATCH_FEED_2,
                DARTS_PREMATCH_FEED
            );
        }

        @Test
        public void testFbMatchConvertion_PrematchFeed_Soccer() throws IOException {
            final SportradarData data = TestHelper.getSportRadarDataFromXml(this.testFeedName);
            final Sport sport = data.getSport().get(0);
            final Tournament tournament = sport.getCategory().get(0).getTournament().get(0);

            assertEquals("Size should be 1", 1, data.getSport().size());
            assertEquals("Size should be 1", 1, sport.getCategory().size());
            assertEquals("Size should be 1", 1, sport.getCategory().get(0).getTournament().size());

            for (Match match : tournament.getMatches().getMatch()) {
                log.info("Testing {} match id: ", sport.getName(), match.getId());

                FbMatch fbMatch;
                if (sport.getId() == 22) {
                    fbMatch = dartsConverter.convert(tournament, match);
                }else{
                    fbMatch = soccerConverter.convert(tournament, match);
                }

                assertEquals(match.getId().toString(), fbMatch.getId());
                assertFalse(fbMatch.isCanceled());
                assertFalse(fbMatch.getPostponed());
                assertEquals(2, fbMatch.getContestants().size());

                for (int i = 0; i < fbMatch.getContestants().size(); i++) {
                    Team radarTeam = match.getTeams().getTeam().get(i);
                    assertEquals(radarTeam.getId().longValue(), fbMatch.getContestants().get(i).getId());
                    assertEquals(radarTeam.getName(), fbMatch.getContestants().get(i).getName());
                    assertEquals(ConverterHelpers.createTeamCode(radarTeam.getName()), fbMatch.getContestants().get(i).getCode());
                    assertNotNull(fbMatch.getContestants().get(i).getLastFive());
                    assertNotNull(fbMatch.getContestants().get(i).getLastFiveId());

                    if (tournament.getTeams().getTeam().get(i).getLastMatches().getTournament().size() >= 5) {
                        assertTrue(fbMatch.getContestants().get(i).getLastFiveId().size() >= 5);
                        assertTrue(fbMatch.getContestants().get(i).getLastFive().length() >= 5);
                    }
                    if (!sport.getName().equalsIgnoreCase("soccer")) {
                        assertEquals("Should be a number", String.valueOf(i + 1), fbMatch.getContestants().get(i).getPosition());
                    }
                }

                if (sport.getName().equalsIgnoreCase("soccer")) {
                    assertEquals("home", fbMatch.getContestants().get(0).getPosition());
                    assertEquals("away", fbMatch.getContestants().get(1).getPosition());
                }


                assertEquals(ConverterHelpers.parseXmlGCDateToStrUTC(match.getDateOfMatch()), fbMatch.getDateTime());
                assertEquals(Long.valueOf(ConverterHelpers.parseXmlGCDateToLong(match.getDateOfMatch())), fbMatch.getDateTimeInt());
                assertNull(fbMatch.getGameEndedInt());

                if (match.getReferee() == null) {
                    assertNull(fbMatch.getReferee());
                } else {
                    assertEquals(match.getReferee().getName(), fbMatch.getReferee().get(0).getName());
                }

                assertEquals(tournament.getUniqueTournamentId().longValue(), fbMatch.getTournamentId().longValue());
                assertEquals(tournament.getName(), fbMatch.getTournamentName());

                assertNull(fbMatch.getVenueName());
                assertNull(fbMatch.getVenueId());

                assertEquals(StatusCode.NotStarted.toString(), fbMatch.getStatus());
                assertEquals(0, fbMatch.getStatusCode());

            }
        }
    }

    public static class NotParameterizedPart {

        @Mock
        CancelledMatchHandler cancelledMatchHandler;

        @InjectMocks
        private SoccerStatMatchConverter soccerConverter;

        @Before
        public void setUp(){
            MockitoAnnotations.initMocks(this);
        }

        @Test
        public void testFbMatchConvertion_MatchdetailsFeed() throws IOException {
            SportradarData data = TestHelper.getSportRadarDataFromXml(MATCH_DETAIL_FEED);
            final Sport sport = data.getSport().get(0);
            final Tournament tournament = sport.getCategory().get(0).getTournament().get(0);

            assertEquals("Size should be 1", 1, data.getSport().size());
            assertEquals("Size should be 1", 1, sport.getCategory().size());
            assertEquals("Size should be 1", 1, sport.getCategory().get(0).getTournament().size());

            for (Match match : tournament.getMatches().getMatch()) {
                log.info("Testing match id: " + match.getId());

                FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
                assertEquals(match.getId().toString(), fbMatch.getId());
                assertEquals(match.getResult().isCanceled(), fbMatch.isCanceled());
                assertEquals(match.getResult().isPostponed(), fbMatch.isCanceled());

                assertEquals(2, fbMatch.getContestants().size());

                if (match.getReferee() != null) {
                    assertEquals(match.getReferee().getName(), fbMatch.getReferee().get(0).getName());
                } else {
                    assertNull(fbMatch.getReferee());
                }

                for (int i = 0; i < fbMatch.getContestants().size(); i++) {
                    assertEquals(match.getTeams().getTeam().get(i).getId().longValue(), fbMatch.getContestants().get(i).getId());
                    assertEquals(match.getTeams().getTeam().get(i).getName(), fbMatch.getContestants().get(i).getName());
                    assertEquals(ConverterHelpers.createTeamCode(match.getTeams().getTeam().get(i).getName()), fbMatch.getContestants().get(i).getCode());
                    assertNull(fbMatch.getContestants().get(i).getLastFive());
                    assertNull(fbMatch.getContestants().get(i).getLastFiveId());
                }
                assertEquals("home", fbMatch.getContestants().get(0).getPosition());
                assertEquals("away", fbMatch.getContestants().get(1).getPosition());

                assertEquals(ConverterHelpers.parseXmlGCDateToStrUTC(match.getDateOfMatch()), fbMatch.getDateTime());
                assertEquals(Long.valueOf(ConverterHelpers.parseXmlGCDateToLong(match.getDateOfMatch())), fbMatch.getDateTimeInt());
                assertNull(fbMatch.getGameEndedInt());

                assertEquals(match.getReferee().getName(), fbMatch.getReferee().get(0).getName());

                assertEquals(tournament.getUniqueTournamentId().longValue(), fbMatch.getTournamentId().longValue());
                assertEquals(tournament.getName(), fbMatch.getTournamentName());

                if (match.getStadium() != null) {
                    assertEquals(match.getStadium().getId().longValue(), fbMatch.getVenueId().longValue());
                    assertEquals(match.getStadium().getName(), fbMatch.getVenueName());
                } else {
                    assertNull(fbMatch.getVenueName());
                    assertNull(fbMatch.getVenueId());
                }

                assertEquals(StatusCode.Ended.toString(), fbMatch.getStatus());
                assertEquals(100, fbMatch.getStatusCode());

                assertNotNull(fbMatch.getLiveData());
                assertEquals(90, fbMatch.getLiveData().getMinutes());

                log.info(" - Success!");
            }
        }


        @Test
        public void testFbMatchGoalsConvertion_ForMatchdetailsFeed() throws IOException {
            FbSoccerMatch.SoccerLiveData liveData = getSingleSoccerMatchLiveData(MATCH_DETAIL_FEED);

            assertEquals(1, liveData.getScore().get("2825").intValue());
            assertEquals(2, liveData.getScore().get("2836").intValue());

            assertEquals(3, liveData.getGoals().size());
            FbSoccerMatch.Goal goal1 = FbSoccerMatch.Goal.builder()
                .contestantId(2836)
                .periodId(2)
                .timeMin(10)
                .type(null)
                .scorer("Angel Correa")
                .scorerId(316152L)
                .assistId(84539L)
                .assist("Koke")
                .build();
            assertEquals(goal1, liveData.getGoals().get(0));
            assertNotEquals(goal1, liveData.getGoals().get(1));

            FbSoccerMatch.Goal goal3 = FbSoccerMatch.Goal.builder()
                    .contestantId(2825)
                    .periodId(2)
                    .timeMin(92)
                    .scorer("Raul Garcia")
                    .assistId(41963L)
                    .build();
            assertNotEquals(goal3, liveData.getGoals().get(2));
        }


        @Test
        public void testFbMatchCardsConvertion_ForMatchdetailsFeed() throws IOException {
            FbSoccerMatch.SoccerLiveData liveData = getSingleSoccerMatchLiveData(MATCH_DETAIL_FEED);
            assertEquals(3, liveData.getGoals().size());

            FbSoccerMatch.Card card1 = FbSoccerMatch.Card.builder()
                .contestantId(2836)
                .periodId(1)
                .timeMin(25)
                .type("YC")
                .player("Thomas")
                .playerId(316148L)
                .build();
            assertEquals(card1, liveData.getCards().get(0));
            assertNotEquals(card1, liveData.getCards().get(1));
        }


        @Test
        public void testFbMatchSubstitutionConvertion_ForMatchdetailsFeed() throws IOException {
            FbSoccerMatch.SoccerLiveData liveData = getSingleSoccerMatchLiveData(MATCH_DETAIL_FEED);

            assertEquals(6, liveData.getSubs().size());
            FbSoccerMatch.Substitute sub1 = FbSoccerMatch.Substitute.builder()
                .contestantId(2825)
                .periodId(2)
                .timeMin(11)
                .playerOn("Benat Etxebarria")
                .playerOnId(18987L)
                .playerOff("Ander Iturraspe")
                .playerOffId(40981L)
                .build();
            assertEquals(sub1, liveData.getSubs().get(0));
            assertNotEquals(sub1, liveData.getSubs().get(1));
        }


        @Test
        public void testFbMatchStatisticConvertion_ForMatchdetailsFeed() throws IOException {
            FbSoccerMatch.SoccerLiveData liveData = getSingleSoccerMatchLiveData(MATCH_DETAIL_FEED);

            assertEquals(2, liveData.getStats().size());
            FbSoccerMatch.Statistic stat1 = FbSoccerMatch.Statistic.builder()
                .contestantId(2825)
                .ballPossession(55)
                .cornerKicks(6)
                .fouls(12)
                .freeKicks(14)
                .offsides(1)
                .shots(3)
                .shotsTarget(6)
                .build();
            assertEquals(stat1, liveData.getStats().get(0));

            FbSoccerMatch.Statistic stat2 = FbSoccerMatch.Statistic.builder()
                .contestantId(2836)
                .ballPossession(45)
                .fouls(12)
                .freeKicks(13)
                .offsides(2)
                .shots(4)
                .shotsTarget(9)
                .build();
            assertEquals(stat2, liveData.getStats().get(1));
        }

        @Test
        public void testFbMatchGoalsConvertion_ForMatchdetailsFeed_WithNoScorerInfo() throws IOException {
            FbSoccerMatch.SoccerLiveData liveData = getSingleSoccerMatchLiveData(MATCH_DETAIL_FEED_NO_PLAYERS);
            assertEquals(2, liveData.getScore().get("2022").intValue());
            assertEquals(0, liveData.getScore().get("1960").intValue());


            assertEquals(2, liveData.getGoals().size());
            FbSoccerMatch.Goal goal1 = FbSoccerMatch.Goal.builder()
                .contestantId(2022)
                .periodId(2)
                .timeMin(8)
                .type(null)
                .build();
            assertEquals(goal1, liveData.getGoals().get(0));
            assertNotEquals(goal1, liveData.getGoals().get(1));
        }

        @Test
        public void testFbMatchLineupsConvertion_ForMatchdetailsFeed() throws IOException {
            FbSoccerMatch.SoccerLiveData liveData = getSingleSoccerMatchLiveData(MATCH_DETAIL_FEED);

            assertEquals(2, liveData.getLineup().size());
            //Lineup 1
            FbSoccerMatch.Lineup lineup1 = liveData.getLineup().get(0);
            assertEquals(2825, lineup1.getContestantId());
            assertEquals("4231", lineup1.getFormation());
            assertEquals(18, lineup1.getPlayers().size());
            FbSoccerMatch.LineupPlayer player1 = FbSoccerMatch.LineupPlayer.builder()
                .id(149734)
                .firstName("Aymeric")
                .lastName("Laporte")
                .number(4)
                .position(4)
                .build();
            assertEquals(player1, lineup1.getPlayers().get(1));

            //Lineup2
            FbSoccerMatch.Lineup lineup2 = liveData.getLineup().get(1);
            assertEquals(2836, lineup2.getContestantId());
            assertEquals("442", lineup2.getFormation());
            assertEquals(18, lineup2.getPlayers().size());
            FbSoccerMatch.LineupPlayer player2 = FbSoccerMatch.LineupPlayer.builder()
                .id(14933)
                .firstName("Diego")
                .lastName("Godin")
                .number(2)
                .position(4)
                .build();
            assertEquals(player2, lineup2.getPlayers().get(0));
        }

        @Test
        public void testFbMatchLineupsConvertion_ForMatchdetailsFeed_WithNoPlayerInfo() throws IOException {
            FbSoccerMatch.SoccerLiveData liveData = getSingleSoccerMatchLiveData(MATCH_DETAIL_FEED_NO_PLAYER_INFO);

            assertEquals(2, liveData.getLineup().size());
            //Lineup 1
            FbSoccerMatch.Lineup lineup1 = liveData.getLineup().get(0);
            assertEquals(43686, lineup1.getContestantId());
            assertNull(lineup1.getFormation());
            assertEquals(22, lineup1.getPlayers().size());
            FbSoccerMatch.LineupPlayer player1 = FbSoccerMatch.LineupPlayer.builder()
                .id(132357)
                .firstName("Luca")
                .lastName("Pagliarulo")
                .number(4)
                .build();
            assertEquals(player1, lineup1.getPlayers().get(1));

            //Lineup2
            FbSoccerMatch.Lineup lineup2 = liveData.getLineup().get(1);
            assertEquals(5320, lineup2.getContestantId());
            assertEquals(23, lineup2.getPlayers().size());

        }

        @Test
        public void testConvertionOf_LastFiveMatchesResults() throws IOException {
            SportradarData data = TestHelper.getSportRadarDataFromXml(PREMATCH_FEED_2);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            final Match match = tournament.getMatches().getMatch().get(0);

            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);

            Contestant contest1 = fbMatch.getContestants().get(0);
            Contestant contest2 = fbMatch.getContestants().get(1);

            assertEquals("Athletic Bilbao", contest1.getName());
            assertEquals("WWWDL", contest1.getLastFive());
            assertEquals(Arrays.asList(12152780L, 12055100L, 12055120L, 12339650L, 12055142L), contest1.getLastFiveId());

            assertEquals("Atletico Madrid", contest2.getName());
            assertEquals("DWDDW", contest2.getLastFive());
            assertEquals(Arrays.asList(12054740L, 12055104L, 12055124L, 12330404L, 12055144L), contest2.getLastFiveId());
        }

        @Test
        public void testConvertionOf_LastFiveMatchesResults_WithNoLastMatches() throws IOException {
            SportradarData data = TestHelper.getSportRadarDataFromXml(PREMATCH_FEED_NO_MATCHES);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            final Match match = tournament.getMatches().getMatch().get(0);

            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);

            Contestant contest1 = fbMatch.getContestants().get(0);
            Contestant contest2 = fbMatch.getContestants().get(1);

            assertEquals("As Cagnes Le Cros Football", contest1.getName());
            assertNull(contest1.getLastFive());

            assertNotNull(contest2.getLastFive());
        }

        @Test
        public void testInjuryTime_AddedToEvent() throws IOException {
            SportradarData data = TestHelper.getSportRadarDataFromXml(MATCH_DETAIL_WITH_INJURY_TIME);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            final Match match = tournament.getMatches().getMatch().get(0);

            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);
            FbSoccerMatch.SoccerLiveData liveData = fbMatch.getLiveData();

            FbSoccerMatch.Goal goal  = liveData.getGoals().get(0);
            assertEquals("Iago Falque", goal.getScorer());
            assertEquals(2, goal.getPeriodId());
            assertEquals(48, goal.getTimeMin());


            FbSoccerMatch.Card card  = liveData.getCards().get(1);
            assertEquals("Danilo Cataldi", card.getPlayer());
            assertEquals(2, goal.getPeriodId());
            assertEquals("InjuryTime shoud with added to card", 46, card.getTimeMin());

            FbSoccerMatch.Substitute sub  = liveData.getSubs().get(5);
            assertEquals("Lorenzo Del Pinto", sub.getPlayerOn());
            assertEquals(2, goal.getPeriodId());
            assertEquals("InjuryTime shoud with added to subs", 48, sub.getTimeMin());
        }

        @Test
        public void testPostponedMatchDetail() throws IOException {
            SportradarData data = TestHelper.getSportRadarDataFromXml(MATCH_DETAIL_POSPONED);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            final Match match = tournament.getMatches().getMatch().get(0);

            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);

            Mockito.verify(cancelledMatchHandler).sendMatchCancelledMessage(SportType.Soccer, match.getId().toString());

            assertEquals(60, fbMatch.getStatusCode());
        }

        @Test
        public void testSoccerMatch_WithOvertime() throws IOException {
            SportradarData data = TestHelper.getSportRadarDataFromXml(MATCHDETAIL_SOCCER_WITH_OVERTIME);
            Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
            final Match match = tournament.getMatches().getMatch().get(0);

            FbSoccerMatch fbMatch = (FbSoccerMatch)soccerConverter.convert(tournament, match);

            assertEquals(0, fbMatch.getLiveData().getScore().get("2523").intValue());
            assertEquals(2, fbMatch.getLiveData().getScore().get("2524").intValue());
        }
    }

    private static FbSoccerMatch.SoccerLiveData getSingleSoccerMatchLiveData(String fileName) throws IOException {
        SportradarData data = TestHelper.getSportRadarDataFromXml(fileName);

        SoccerStatMatchConverter converter = new SoccerStatMatchConverter();
        Tournament tournament = data.getSport().get(0).getCategory().get(0).getTournament().get(0);
        final Match match = tournament.getMatches().getMatch().get(0);
        FbSoccerMatch fbMatch = (FbSoccerMatch) converter.convert(tournament, match);
        return fbMatch.getLiveData();
    }

}

