package com.smart8.sportradar.converters.stats;

import com.fasterxml.jackson.core.type.TypeReference;
import com.smart8.sportradar.firebase.FbTournament;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.mappers.ResourceMapper;
import com.smart8.sportradar.statisticsmodels.Category;
import com.smart8.sportradar.statisticsmodels.Sport;
import com.smart8.sportradar.statisticsmodels.SportradarData;
import com.smart8.sportradar.statisticsmodels.Tournament;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TournamentToFbTournamentConverterTest {

    private final static String TOURNAMENTS = "sportradar/statfeeds/tournaments.xml";

    private TournamentToFbTournamentConverter converter;

    private ResourceMapper resourceMapper;

    @Before
    public void setUp() {
        resourceMapper = mock(ResourceMapper.class);
        Map<String, Integer> priority = new HashMap<>();
        priority.put("8", 1000);
        priority.put("17", 999);
        priority.put("35", 998);
        priority.put("238", 997);
        priority.put("34", 996);
        priority.put("37", 995);
        priority.put("7", 994);
        priority.put("38", 993);
        priority.put("679", 992);
        when( resourceMapper.readDataToMap(anyString(), any(TypeReference.class))).thenReturn(priority);

        converter = new TournamentToFbTournamentConverter(resourceMapper);


    }

    @Test
    public void testTournamentConvertion() throws IOException {

        final SportradarData data = TestHelper.getSportRadarDataFromXml(TOURNAMENTS);
        final Sport sport = data.getSport().get(0);

        Category category = sport.getCategory().get(1);
        Tournament tournament = category.getTournament().get(0);

        FbTournament fbTournament = converter.convertTournamentToFbTournament(category, tournament);
        assertEquals(999, fbTournament.getPriority());
        assertEquals("Premier League", fbTournament.getName());
    }

    @Test
    public void testTournament_WithNoPriority() throws IOException {
        final SportradarData data = TestHelper.getSportRadarDataFromXml(TOURNAMENTS);
        final Sport sport = data.getSport().get(0);

        Category category = sport.getCategory().get(0);
        Tournament tournament = category.getTournament().get(0);


        FbTournament fbTournament = converter.convertTournamentToFbTournament(category, tournament);
        assertEquals(0, fbTournament.getPriority());
    }


    @Test
    public void testChampionsLeageConverting() throws IOException {
        final SportradarData data = TestHelper.getSportRadarDataFromXml(TOURNAMENTS);
        final String TOURNAMENT_NAME = "UEFA Champions League";
        final Sport sport = data.getSport().get(0);
        Category championLeague = null;

        for (Category cat : sport.getCategory()) {
            if (cat.getId() == 393) {
                championLeague = cat;
                break;
            }
        }

        FbTournament fbTournament = converter.convertTournamentToFbTournament(championLeague, championLeague.getTournament().get(0));

        assertEquals(TOURNAMENT_NAME, fbTournament.getName());
        assertEquals(994, fbTournament.getPriority());
    }
}
