package com.smart8.sportradar.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ConverterHelpersTest {

    @Test
    public void testCetTimeToUtcTimeConverter(){
        final String CET_DT = "2017-10-29T20:05:15 CET";
        final String UTC_DT = "2017-10-29T19:05:15Z";
        assertEquals(UTC_DT, ConverterHelpers.parseDateOfMatchString(CET_DT));
    }

    @Test
    public void testCetTimeToEpochTimeConversion(){
        final String CET_DT = "2017-10-29T20:05:15 CET";
        final long EPOCH_DT = 1509303915000L;
        assertEquals(EPOCH_DT, ConverterHelpers.parseMatchDateTimeToLong(CET_DT));
    }

    @Test
    public void testFullPlayerNameParser(){
        final String NAME_1 = "de Sales Cabral, Matheus";
        final String[] response1 = ConverterHelpers.splitPlayerFullName(NAME_1);
        assertEquals("de Sales Cabral", response1[0]);
        assertEquals("Matheus", response1[1]);

        final String NAME_2 = "Junior, Marcos";
        final String[] response2 = ConverterHelpers.splitPlayerFullName(NAME_2);
        assertEquals("First name incorect", "Marcos", response2[1]);
        assertEquals("Last name incorrect", "Junior", response2[0]);

        final String NAME_3 = "Marlon";
        final String[] response3 = ConverterHelpers.splitPlayerFullName(NAME_3);
        assertEquals("Marlon", response3[0]);
        assertEquals("", response3[1]);

        final String NAME_4 = "Mateus Norton";
        final String[] response4 = ConverterHelpers.splitPlayerFullName(NAME_4);
        assertEquals("Last name incorrect", "Norton", response4[0]);
        assertEquals("First name incorect", "Mateus", response4[1]);

        final String NAME_5 = "Rodrigo De Paul";
        final String[] response5 = ConverterHelpers.splitPlayerFullName(NAME_5);
        assertEquals("Last name incorrect", "De Paul", response5[0]);
        assertEquals("First name incorect", "Rodrigo", response5[1]);
    }

    @Test
    public void testMapFlattering() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        File file = new File(Resources.getResource("sportradar/utils/map-to-flatter.json").getFile());
        Map map = mapper.readValue(file, Map.class);

        Map flattenMap = ConverterHelpers.removeNullsAndFlattenMap(map, "");

        assertEquals( 9, flattenMap.size());
        assertEquals("somekeytext", flattenMap.get("levelkey1/levelkey2/levelkey3/levelkey4"));
        assertEquals("random second text", flattenMap.get("levelkey1/levelkey2/somekeytext"));
        assertEquals("world!!!", flattenMap.get("levelkey1/level2map/hello"));
        assertEquals("hello", flattenMap.get("levelkey1/list/0/keyoflist"));
        assertEquals("world", flattenMap.get("levelkey1/list/1/keyoflist"));
        assertEquals(1, flattenMap.get("levelkey1/listofNums/0"));
        assertEquals(2, flattenMap.get("levelkey1/listofNums/1"));
        assertEquals(3, flattenMap.get("levelkey1/listofNums/2"));

    }

    @Test
    public void testgetMinuteDiff(){
        final long START_TIME = 1510666800000L; //November 14, 2017 1:40:40 PM
        final long END_TIME = 1510668840000L; //November 14, 2017 2:14:00 PM
        final int DIFF = 34;

        assertEquals(DIFF, ConverterHelpers.getMinutesDiff(START_TIME, END_TIME));
    }

    @Test
    public void test(){
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        list.add("ddd");
        list.add("eee");
        list.add("fff");
        list.add("ggg");
        list.add("h");
        list.add("i");

       int count = 0;
       int max = 2;
       //    int done = false;
        List subList;// = new ArrayList();
        int left = list.size();
        while( left > 0 ){

            if ( left > max){
                subList = list.subList(count, count + max);
            }else{
                subList = list.subList(count, list.size());
            }
            count += max;
            left -=subList.size();
        }
    }
}
