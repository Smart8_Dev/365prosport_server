package com.smart8.sportradar.helpers;

import com.smart8.sportradar.firebase.FbSoccerMatch;

import java.util.ArrayList;
import java.util.List;

public  class MatchBuilderHelper {

    /**
     * @param values teamId:periodId set. Example 10001:1
     */
    public static List<FbSoccerMatch.Goal> createGoalsByTeamIdAndPeriod(String ... values) {

        List<FbSoccerMatch.Goal> goalList = new ArrayList<>();
        for (String value : values) {
            String[] goalStr = value.split(":");
            FbSoccerMatch.Goal goal = FbSoccerMatch.Goal.builder()
                .contestantId(Long.valueOf(goalStr[0]))
                .periodId(Integer.valueOf(goalStr[1]))
                .build();
            goalList.add(goal);
        }
        return  goalList;
    }

//    private List<Map<String, Integer>> createSets(String ... values) {
//        List<Map<String, Integer>> setScoreList = new ArrayList<>();
//        for (String value : values) {
//            Map<String, Integer> setScore = new HashMap<>();
//            String[] scoreStr = value.split(":");
//            setScore.put(PLAYER_1, Integer.parseInt(scoreStr[0]));
//            setScore.put(PLAYER_2, Integer.parseInt(scoreStr[1]));
//            setScoreList.add(setScore);
//        }
//        return setScoreList;
//    }
}
