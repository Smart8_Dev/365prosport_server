package com.smart8.sportradar.helpers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import com.smart8.sportradar.livemodels.BetradarLivescoreData;
import com.smart8.sportradar.models.unibet.UniLiveResult;
import com.smart8.sportradar.models.unibet.UniResult;
import com.smart8.sportradar.statisticsmodels.SportradarData;
import org.apache.commons.io.FileUtils;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.xml.transform.StringSource;

import javax.xml.bind.JAXBElement;
import java.io.File;
import java.io.IOException;

public class TestHelper {

    private static Jaxb2Marshaller marshaller;
    private static Jaxb2Marshaller marshallerLive;
    private static ObjectMapper mapper;

    static {
        marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("com.smart8.sportradar.statisticsmodels");
        marshallerLive = new Jaxb2Marshaller();
        marshallerLive.setPackagesToScan("com.smart8.sportradar.livemodels");
        mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public static SportradarData getSportRadarDataFromXml(String fileName) throws IOException {
        String data = FileUtils.readFileToString(new File(Resources.getResource(fileName).getFile()), "utf-8");
        final JAXBElement<?> unmarshal = (JAXBElement<?>) marshaller.unmarshal(new StringSource(data));
        return (SportradarData) unmarshal.getValue();
    }

    public static BetradarLivescoreData getSportRadarLiveDataFromXml(String fileName) throws IOException {
        String data = FileUtils.readFileToString(new File(Resources.getResource(fileName).getFile()), "utf-8");
        return (BetradarLivescoreData) marshallerLive.unmarshal(new StringSource(data));
    }

    public static UniResult getUniBetResults(String fileName) {
        UniResult result = null;
        try {
            String data = FileUtils.readFileToString(new File(Resources.getResource(fileName).getFile()), "utf-8");
            result = mapper.readValue(data, UniResult.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static UniLiveResult getUniLiveBetResults(String fileName) {
        UniLiveResult result = null;
        try {
            String data = FileUtils.readFileToString(new File(Resources.getResource(fileName).getFile()), "utf-8");
            result = mapper.readValue(data, UniLiveResult.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
