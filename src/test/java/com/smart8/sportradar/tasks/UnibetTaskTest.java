package com.smart8.sportradar.tasks;

import com.smart8.sportradar.enums.SportType;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.models.MiniMatch;
import com.smart8.sportradar.models.unibet.BetOffer;
import com.smart8.sportradar.models.unibet.UniLiveResult;
import com.smart8.sportradar.repositories.MatchRepository;
import com.smart8.sportradar.services.FirebaseService;
import com.smart8.sportradar.services.UnibetService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;


@RunWith(MockitoJUnitRunner.class)
public class UnibetTaskTest {

    private final static String MATCH_ID = "1001";
    private final static Long UNI_EVENT_ID = 1004058498L;
    private final static Long UNI_GROUP_ID = 1000094985L;
    private final static Long TOURNAMENT_ID = 202L;

    @Mock
    private MatchRepository repository;

    @Mock
    private FirebaseService fbService;

    @Mock
    private UnibetService unibetService;

    @InjectMocks
    private UnibetTask unibetTask;

    @Before
    public void setUp(){
        unibetTask = new UnibetTask();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLiveUniBetOffers_Soccer(){
        List<MiniMatch> matchList = Collections.singletonList(createStubMatch());
        Mockito.when(repository.getLiveMatches()).thenReturn(matchList);
        Mockito.when(repository.getUnibetMatchIdByRadarMatchId(matchList.get(0).getId())).thenReturn(UNI_EVENT_ID);
        Mockito.when(repository.getUniTournamentIdByRadarId(matchList.get(0).getTournamentId()+"")).thenReturn(UNI_GROUP_ID.toString());

        UniLiveResult result = TestHelper.getUniLiveBetResults("unibet/live-soccer-bets.json");

        Mockito.when(unibetService.fetchLiveBets(0, UNI_GROUP_ID)).thenReturn(result);

        ArgumentCaptor<Map> arg = ArgumentCaptor.forClass(Map.class);

        unibetTask.getLiveBets();

        Mockito.verify(fbService).updateOdds(arg.capture(), eq(SportType.Soccer.name()));

        Map<String, Set<BetOffer>> mapToStrore = arg.getValue();

        assertEquals(1, mapToStrore.size());
        assertEquals(5, mapToStrore.get(MATCH_ID).size());
    }

//    @Test
//    public void testLiveUniBetOffers_Darts(){
//        List<FbMatch> matchList = Collections.singletonList(createStubMatch());
//        Mockito.when(repository.getLiveMatches()).thenReturn(matchList);
//        Mockito.when(repository.getUnibetMatchIdByRadarMatchId(matchList.get(0).getId())).thenReturn(UNI_EVENT_ID);
//        Mockito.when(repository.getUniTournamentIdByRadarId(matchList.get(0).getTournamentId().toString())).thenReturn(UNI_GROUP_ID.toString());
//
//        UniResult result = Helper.getUniBetResults("unibet/darts-bets.json");
//
//        Mockito.when(unibetService.fetchLiveBets(0, UNI_GROUP_ID)).thenReturn(result);
//
//        ArgumentCaptor<Map> arg = ArgumentCaptor.forClass(Map.class);
//
//        unibetTask.getLiveBets();
//
//        Mockito.verify(fbService).updateOdds(arg.capture(), eq(SportType.Darts.name()));
//
//        Map<String, Set<BetOffer>> mapToStrore = arg.getValue();
//
//        assertEquals(1, mapToStrore.size());
//        assertEquals(1, mapToStrore.get(MATCH_ID).size());
//        mapToStrore.get(MATCH_ID).forEach(betOffer -> {
//            assertTrue(betOffer.getCriterionId() == CriterionType.MATCH_OUTCOME.id);
//        });
//    }

    private MiniMatch createStubMatch(){
        return MiniMatch.builder()
            .id(MATCH_ID)
            .tournamentId(TOURNAMENT_ID )
            .build();
    }
}
