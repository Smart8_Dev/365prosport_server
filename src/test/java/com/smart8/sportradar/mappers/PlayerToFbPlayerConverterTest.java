package com.smart8.sportradar.mappers;

import com.smart8.sportradar.converters.stats.PlayerToFbPlayerConverter;
import com.smart8.sportradar.firebase.FbPlayer;
import com.smart8.sportradar.helpers.TestHelper;
import com.smart8.sportradar.statisticsmodels.Player;
import com.smart8.sportradar.statisticsmodels.SportradarData;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlayerToFbPlayerConverterTest {

    private static List<Player> players;

    private PlayerToFbPlayerConverter converter;

    @BeforeClass
    public static void before() throws IOException {
        SportradarData data = TestHelper.getSportRadarDataFromXml("sportradar/statfeeds/feed-soccer-nurnberg-squad.xml");
        players = data.getSport().get(0).getTeams().getTeam().get(0).getPlayers().getPlayer();
    }

    @Before
    public void setUp(){
        converter = new PlayerToFbPlayerConverter();
    }


    @Test
    public void radarPlayerToFbPlayerConvertion_With2EmptyFields() throws IOException {
       Player player = players.get(0);

        FbPlayer fbPlayer = converter.convertRadarPlayerToFbPlayer(player);
        assertEquals(254003, fbPlayer.getId());
        assertEquals("Patrick", fbPlayer.getFirstName());
        assertEquals("Erras", fbPlayer.getLastName());
        assertNull( fbPlayer.getNumber());
        assertEquals("right", fbPlayer.getFoot());
        assertEquals("DEU", fbPlayer.getCountry());
        assertEquals("1995-01-21", fbPlayer.getBirth());
        assertEquals(85, fbPlayer.getWeight());
        assertEquals(195, fbPlayer.getHeight());
        assertEquals("M", fbPlayer.getPosition());
        assertNull(fbPlayer.getTeam());
        assertEquals("Number,Team", fbPlayer.emptyFields());
    }

    @Test
    public void radarPlayerToFbPlayerConvertion_WithEmptyFields() throws IOException {
        Player player = players.get(36);

        FbPlayer fbPlayer = converter.convertRadarPlayerToFbPlayer(player);
        assertEquals(1244492, fbPlayer.getId());
        assertEquals("Axel", fbPlayer.getFirstName());
        assertEquals("Hofmann", fbPlayer.getLastName());
        assertEquals( "31", fbPlayer.getNumber());
        assertNull("right", fbPlayer.getFoot());
        assertEquals("DEU", fbPlayer.getCountry());
        assertEquals("1997-10-09", fbPlayer.getBirth());
        assertEquals(0, fbPlayer.getWeight());
        assertEquals(0, fbPlayer.getHeight());
        assertEquals("G", fbPlayer.getPosition());
        assertNull(fbPlayer.getTeam());
        assertEquals("Preferred foot,Weight,Height,Team", fbPlayer.emptyFields());
    }



}
