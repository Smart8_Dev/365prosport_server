package com.smart8.sportradar.reports;

import com.smart8.sportradar.firebase.FbPlayer;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PlayerMappingReportTest {

    private PlayerMappingReport report;
    private final String FILE_NAME = "missing-players-item.txt";

    @Before
    public void setUp(){
        report = new PlayerMappingReport();
    }

    @Test
    public void testCorrectReportCreation() throws IOException {

        List<FbPlayer> players = new ArrayList<>();

        final FbPlayer player1 = FbPlayer.builder().id(101L).firstName("Cristiano").lastName("Ronaldo").build();
        final FbPlayer player2 = FbPlayer.builder().id(201L).firstName("Lionel").lastName("Messi").build();

        players.add(player1);
        players.add(player2);

        report.writeReport(players, FILE_NAME);

        List<String> lines = FileUtils.readLines(new File(FILE_NAME), "utf-8");

        assertEquals(lines.get(0), "player: " + player1.getId() + ", " + player1.getFirstName() + " " + player1.getLastName());
        assertEquals(lines.get(1), "    missingData: " + player1.emptyFields());
        assertEquals(lines.get(2), "player: " + player2.getId() + ", " + player2.getFirstName() + " " + player2.getLastName());
        assertEquals(lines.get(3), "    missingData: " + player2.emptyFields());

    }

    @After
    public void cleanUp(){
        FileUtils.deleteQuietly(new File(FILE_NAME));
    }



}
